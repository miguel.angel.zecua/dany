<!-- Page Sidebar Start-->
<header class="main-nav">

<div class="sidebar-user text-center mb-0" id="sd">
<img class="img-90 rounded-circle mt-0" src="<?php echo base_url('public/assets/images/dashboard/1.png');?>" alt="">
<div class="badge-bottom"><span class="badge badge-primary">New</span></div><a href="user-profile.html">
<h5 class="mt-3 f-14 f-w-800">ADMINISTRADOR</h5></a>
<p class="mb-0 font-roboto">IMEI</p>

</div>


<nav class="mt-0">
<div class="main-navbar mt-0">
<div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
<div id="mainnav">           
<ul class="nav-menu custom-scrollbar">
<li class="back-btn">
<div class="mobile-back text-end"><span>REGRESAR</span><i class="fa fa-angle-right ps-2" aria-hidden="true"></i></div>
</li>
<li class="sidebar-main-title mt-2">
<div>
<h6 class="text-center mt-0"><strong>GENERAL</strong></h6>
</div>
</li>

<!-- Menu 1-->
<li class="dropdown"><a class="nav-link menu-title" href="javascript:void(0)"><i data-feather="home"></i><span>Inicio</span></a>
<ul class="nav-submenu menu-content">
<li><a href="<?= base_url('Admin')?>">Inicio</a></li>
</ul>
</li>
<!-- Menu 1-->

<!-- Menu 2-->
<li class="dropdown"><a class="nav-link menu-title" href="javascript:void(0)"><i data-feather="database"></i><span>Calificaciones</span></a>
<ul class="nav-submenu menu-content">
<li><a href="<?= base_url('Admin/Calificaciones')?>">Calificaciones Actuales</a></li>
<li><a href="<?= base_url('Admin/Ciclos_escolares')?>">Ciclos escolares</a></li>
<li><a href="<?= base_url('Admin/Historiales')?>">Historiales</a></li>
</ul>
</li>
<!-- Menu 2-->

<!-- Menu 4-->
<li class="dropdown"><a class="nav-link menu-title" href="javascript:void(0)"><i data-feather="dollar-sign"></i><span>Pagos</span></a>
<ul class="nav-submenu menu-content">
<li><a href="<?= base_url('Admin/Pagos_Alumnos')?>">Pagos Alumnos</a></li>
<li class="d-none"><a href="<?= base_url('Admin/Pagos_Maestros')?>">Pagos Maestros</a></li>
<li><a href="<?= base_url('Admin/Busqueda_Pay_Alumnos')?>">Busqueda de Pagos Alumnos</a></li>

</ul>
</li>
<!-- Menu 4-->


<!-- Menu 3-->
<li class="dropdown"><a class="nav-link menu-title" href="javascript:void(0)"><i data-feather="clock"></i><span>Horarios</span></a>
<ul class="nav-submenu menu-content">
<li><a href="<?= base_url('Admin/Horario_alumnos')?>">Horarios Alumnos</a></li>
<li><a href="<?= base_url('Admin/Horario_maestros')?>">Horarios Maestros</a></li>
</ul>
</li>
<!-- Menu 3-->

<!-- Menu 4-->
<li class="dropdown d-none"><a class="nav-link menu-title" href="javascript:void(0)"><i data-feather="box"></i><span>Planeaciones</span></a>
<ul class="nav-submenu menu-content">
<li><a href="<?= base_url('Alumnos/Historial')?>">Historial Académico</a></li>
</ul>
</li>
<!-- Menu 4-->

<!-- Menu 4-->
<li class="dropdown"><a class="nav-link menu-title" href="javascript:void(0)"><i data-feather="check-square"></i><span>Notificaciones</span></a>
<ul class="nav-submenu menu-content">
<li><a href="<?= base_url('Admin/Notificaciones_alumnos')?>">Notificaciones Alumnos</a></li>
<li><a href="<?= base_url('Admin/Notificaciones_maestros')?>">Notificaciones Maestros</a></li>
<li><a href="<?= base_url('Admin/Permisos')?>">Permisos</a></li>
</ul>
</li>
<!-- Menu 4-->

<!-- Menu 8-->
<li class="dropdown"><a class="nav-link menu-title" href="javascript:void(0)"><i data-feather="layers"></i><span>Evaluación Continua</span></a>
<ul class="nav-submenu menu-content">
<li><a href="<?= base_url('Admin/Evaluacion_maestros')?>">Evaluación de Maestros</a></li>
<li class="d-none"><a href="chart-google.html">Evaluación de Institución</a></li>
</ul>
</li>
<!-- Menu 8-->

<!-- Menu 8-->
<li class="dropdown"><a class="nav-link menu-title" href="javascript:void(0)"><i data-feather="layers"></i><span>Listados</span></a>
<ul class="nav-submenu menu-content">
<li><a href="<?= base_url('Admin/Listados')?>">Listas por Salones</a></li>

</ul>
</li>
<!-- Menu 8-->

</ul>
</div>
<div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
</div>
</nav>
</header>
<!-- Page Sidebar Ends-->