
<html lang="en">
<head>
<title>Lista de Alumnos</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Integra-Educa">
<meta name="keywords" content="Integra-Educa, Integra">
<meta name="author" content="Mike">

<?=$this->load->view('Include/base_css','',TRUE);?>

<script type="text/javascript" src="<?=base_url('public/assets/js/html2/html2pdf.bundle.min.js')?>"></script>

<!-- CSS DATATABLES-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('especials/datatables/css/buttons.dataTables.min.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('especials/datatables/css/dataTables.bootstrap5.min.css') ?>">
<!-- CSS DATATABLES-->

</head>
<body>

<!-- Loader starts-->
<div class="loader-wrapper">
<div class="theme-loader">    
<div class="loader-p"></div>
</div>
</div>
<!-- Loader ends-->


<!-- page-wrapper Start-->
<div class="page-wrapper" id="pageWrapper">

<!-- Page Header Start-->
<?=$this->load->view('Admin/Header','',TRUE);?>
<!-- Page Header Start-->


<!-- Page Body Start-->
<div class="page-body-wrapper horizontal-menu">
 
<!-- Page Sidebar Inicio-->
<?=$this->load->view('Admin/Side_bar','',TRUE);?>
<!-- Page Sidebar Fin-->

<!-- TITULOS PAGINA-->
<div class="page-body">
<div class="container-fluid">
<div class="page-header">
<div class="row ">
<div class="col-sm-6 ">
<h3>Listado de Alumnos</h3>
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="index.html">Inicio</a></li>
<li class="breadcrumb-item">Home</li>
</ol>
</div>
</div>
</div>
</div>
<!-- TITULOS PAGINA-->




<div class="row justify-content-center mr-2 ml-2">
<div class="col-sm-12 col-xl-12 col-lg-12">
</div>
<span class="badge badge-warning text-dark" style="font-size: 22px; border-radius: 25px;">-Sistema Integral de Información-</span>
</div>

<br>


<!-- Container-fluid starts-->
<div class="container-fluid">

<!-- BODY-->
<div class="row">
    
<div class="col-sm-12 col-xl-12 col-lg-12" style="background-color: #D0ECE7 !important; border-radius: 25px;">
<div id="Impresion_2">
<!-- Tabla de excel-->
<table border="0" cellpadding="0" cellspacing="0" width="1346" style="border-collapse:
    collapse;table-layout:fixed;width:1006pt">
    <colgroup><col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="64" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    <col width="19" span="191" style="width:14pt">
    <col width="23" style="mso-width-source:userset;mso-width-alt:841;width:17pt">
    <col width="19" span="7" style="mso-width-source:userset;mso-width-alt:694;
    width:14pt">
    <col width="71" style="mso-width-source:userset;mso-width-alt:2596;width:53pt">
    <col width="16" span="56" style="mso-width-source:userset;mso-width-alt:585;
    width:12pt">
    </colgroup><tbody><tr height="20" style="height:15.0pt">
     <td height="20" width="23" style="height:15.0pt;width:17pt"></td>
     <td width="19" style="width:14pt"></td>
     <td width="19" style="width:14pt"></td>
     <td width="19" style="width:14pt"></td>
     <td width="19" style="width:14pt"></td>
     <td width="19" style="width:14pt"></td>
     <td width="19" style="width:14pt"></td>
     <td width="19" style="width:14pt"></td>
     <td width="71" style="width:53pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="16" style="width:12pt"></td>
     <td width="19" style="width:14pt"></td>
     <td width="19" style="width:14pt"></td>
     <td width="19" style="width:14pt"></td>
     <td width="19" style="width:14pt"></td>
     <td width="19" style="width:14pt"></td>
    </tr>
    <tr height="20" style="height:15.0pt">
     <td colspan="74" height="20" width="1270" style="height:15.0pt;width:950pt" align="left" valign="top">
      
     <span style="mso-ignore:vglayout;
     position:absolute;z-index:1;margin-left:21px;margin-top:16px;width:117px;
     height:139px"><img width="117" height="139" src="<?php echo base_url('public/assets/images/dashboard/image002.png');?>" alt="escudo imei,escudo imei" v:shapes="Picture_x0020_1 _x0000_s1026"></span><!--[endif]--><span style="mso-ignore:vglayout2">
     <table cellpadding="0" cellspacing="0">
      <tbody><tr>
       <td colspan="74" height="20" class="xl122" width="1270" style="height:15.0pt;
       width:950pt"></td>
      </tr>
     </tbody></table>
     </span></td>
     <td></td>
     <td></td>
     <td></td>
     <td></td>
    </tr>
    <tr height="20" style="height:15.0pt">
     <td colspan="74" height="20" class="xl123" style="height:15.0pt"></td>
     <td></td>
     <td></td>
     <td></td>
     <td></td>
    </tr>
    <tr height="20" style="height:15.0pt">
     <td colspan="74" height="20" class="xl122" style="height:15.0pt"></td>
     <td></td>
     <td></td>
     <td></td>
     <td></td>
    </tr>
    <tr height="20" style="height:15.0pt">
     <td colspan="74" height="20" class="xl122" style="height:15.0pt"></td>
     <td></td>
     <td></td>
     <td></td>
     <td></td>
    </tr>
    <tr height="20" style="height:15.0pt">
     <td colspan="74" height="20" class="xl124" style="height:15.0pt">Lista de
     Asistencia y Medición Integral</td>
     <td></td>
     <td></td>
     <td></td>
     <td></td>
    </tr>
    <?php foreach($mat_datos->result() as $mat_datos){ ?>
    <tr class="xl65" height="17" style="mso-height-source:userset;height:12.75pt">
     <td height="17" class="xl65" style="height:12.75pt"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl90"></td>
     <td class="xl66" colspan="2" style="mso-ignore:colspan">Asignatura:</td>
     <td colspan="23" class="xl121"><?= $mat_datos->Nombre ?></td>
     
     <td class="xl90"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl66" colspan="5" style="mso-ignore:colspan">Licenciatura:</td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td colspan="11" class="xl121"><?= $mat_datos->Carrera ?></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
    </tr>
    <tr class="xl65" height="17" style="mso-height-source:userset;height:12.75pt">
     <td height="17" class="xl65" style="height:12.75pt"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl66" colspan="2" style="mso-ignore:colspan">Grupo:</td>
     <td colspan="23" class="xl125"></td>

     <td class="xl90"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl66" colspan="5" style="mso-ignore:colspan">Cuatrimestre:</td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td colspan="11" class="xl125"><?= $mat_datos->Cuatrimestre ?></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
    </tr>
    <tr class="xl65" height="17" style="mso-height-source:userset;height:12.75pt">
     <td height="17" class="xl65" style="height:12.75pt"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl66" colspan="2" style="mso-ignore:colspan">Turno:</td>
     <td colspan="23" class="xl126"><?= $mat_datos->Turno ?></td>
     <td class="xl90"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl66" colspan="5" style="mso-ignore:colspan">Generación:</td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl66"></td>
     <td colspan="11" class="xl126"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
    </tr>
    <tr class="xl65" height="16" style="height:12.0pt">
     <td height="16" class="xl65" style="height:12.0pt"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl66" colspan="2" style="mso-ignore:colspan">Docente:</td>
     <td colspan="23" class="xl126"> 
     <?= $mat_datos->Prof ?>
     </td>
    
     <td class="xl90"></td>
     <td class="xl66"></td>
     <td class="xl66"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl66" colspan="6" style="mso-ignore:colspan">Ciclo Escolar:</td>
     <td class="xl65"></td>
     <td class="xl66"></td>
     <td colspan="11" class="xl126">2022-2</td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
    </tr>

    <?php  }?>

    <tr height="21" style="height:15.75pt">
     <td height="21" colspan="78" style="height:15.75pt;mso-ignore:colspan"></td>
    </tr>
    <tr class="xl65" height="17" style="mso-height-source:userset;height:12.75pt">
     <td rowspan="5" height="174" class="xl128" style="height:131.1pt; writing-mode: vertical-lr;transform: rotate(180deg);text-align:left;background-color: rgb(255,255,255);">No. Progresivo</td>
     <td colspan="8" rowspan="5" class="xl130" style="background-color: rgb(255,255,255);" >Nombre del Alumno</td>
     <td colspan="56" class="xl132" style="border-right:1.0pt solid black;background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl67" style="border-left:none;background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl68" style="background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl68" style="background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl69" style="background-color: rgb(255,255,255);">&nbsp;</td>
     <td colspan="3" class="xl134" style="border-left:none;background-color:rgb(255,255,255);">&nbsp;</td>
     <td class="xl69" style="background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl65" ></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
    </tr>
    <tr class="xl65" height="17" style="mso-height-source:userset;height:12.75pt">
     <td colspan="56" height="17" class="xl137" style="border-right:1.0pt solid black;background-color: rgb(255,255,255);
     height:12.75pt">&nbsp;</td>
     <td class="xl70" style="border-left:none;background-color:rgb(255, 255, 255);">&nbsp;</td>
     <td class="xl65" style="background-color: rgb(255,255,255);"></td>
     <td class="xl65" style="background-color: rgb(255,255,255);"></td>
     <td class="xl71" style="background-color: rgb(255,255,255);">&nbsp;</td>
     <td colspan="3" class="xl139" style="border-left:none;background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl71" style="background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
    </tr>
    <tr class="xl65" height="17" style="mso-height-source:userset;height:12.75pt">
     <td colspan="13" height="17" class="xl142" style="height:12.75pt;border-left:none;background-color: rgb(255,255,255);">MAYO</td>
     <td colspan="13" class="xl144" style="background-color: rgb(255,255,255);" >JUNIO</td>
     <td colspan="13" class="xl144" style="background-color: rgb(255,255,255);">JULIO</td>
     <td colspan="9" class="xl144" style="background-color: rgb(255,255,255);">AGOSTO</td>
     <td colspan="8" class="xl146" style="border-right:1.0pt solid black;background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl72" style="border-left:none;background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl73" style="background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl73" style="background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl74" style="background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl75" style="border-left:none;background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl76" style="background-color: rgb(255,255,255);"></td>
     <td class="xl76" style="background-color: rgb(255,255,255);"></td>
     <td class="xl71" style="background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl65" ></td>
     <td class="xl65" ></td>
     <td class="xl65" ></td>
     <td class="xl65" ></td>
     <td class="xl65" ></td>
    </tr>
    <tr class="xl88" height="17" style="mso-height-source:userset;height:12.75pt">
     <td height="17" class="xl77" style="height:12.75pt;border-left:none;background-color: rgb(255,255,255);">2</td>
     <td class="xl77" style="border-left:none;background-color: rgb(255,255,255);">4</td>
     <td class="xl77" style="border-left:none;background-color: rgb(255,255,255);">6</td>
     <td class="xl77" style="border-left:none;background-color: rgb(255,255,255);">9</td>
     <td class="xl77" style="border-left:none;background-color: rgb(255,255,255);">11</td>
     <td class="xl77" style="border-left:none;background-color: rgb(255,255,255);">13</td>
     <td class="xl77" style="border-left:none;background-color: rgb(255,255,255);">16</td>
     <td class="xl77" style="border-left:none;background-color: rgb(255,255,255);">18</td>
     <td class="xl77" style="border-left:none;background-color: rgb(255,255,255);">20</td>
     <td class="xl77" style="border-left:none;background-color: rgb(255,255,255);">23</td>
     <td class="xl77" style="border-left:none;background-color: rgb(255,255,255);">25</td>
     <td class="xl77" style="border-left:none;background-color: rgb(255,255,255);">27</td>
     <td class="xl78" style="border-left:none">30</td>
     <td class="xl79" style="border-top:none;border-left:none">1</td>
     <td class="xl79" style="border-top:none">3</td>
     <td class="xl80" style="border-top:none">6</td>
     <td class="xl80" style="border-top:none">8</td>
     <td class="xl80" style="border-top:none">10</td>
     <td class="xl80" style="border-top:none">13</td>
     <td class="xl81" style="border-top:none">15</td>
     <td class="xl82" style="border-left:none">17</td>
     <td class="xl83" style="border-top:none;border-left:none">20</td>
     <td class="xl83" style="border-top:none">22</td>
     <td class="xl83" style="border-top:none">24</td>
     <td class="xl83" style="border-top:none">27</td>
     <td class="xl83" style="border-top:none">29</td>
     <td class="xl83" style="border-top:none">1</td>
     <td class="xl83" style="border-top:none">4</td>
     <td class="xl83" style="border-top:none">6</td>
     <td class="xl83" style="border-top:none">8</td>
     <td class="xl84" style="border-top:none">11</td>
     <td class="xl84" style="border-top:none">13</td>
     <td class="xl84" style="border-top:none">15</td>
     <td class="xl83" style="border-top:none">18</td>
     <td class="xl83" style="border-top:none">20</td>
     <td class="xl81" style="border-top:none">22</td>
     <td class="xl85" style="border-top:none;border-left:none;background-color: rgb(255,255,255);">25</td>
     <td class="xl85" style="border-top:none;border-left:none;background-color: rgb(255,255,255);">27</td>
     <td class="xl86" style="border-top:none;border-left:none;background-color: rgb(255,255,255);">29</td>
     <td class="xl86" style="border-top:none;border-left:none;background-color: rgb(255,255,255);">1</td>
     <td class="xl86" style="border-top:none;border-left:none;background-color: rgb(255,255,255);">3</td>
     <td class="xl86" style="border-top:none;border-left:none;background-color: rgb(255,255,255);">5</td>
     <td class="xl86" style="border-top:none;border-left:none;background-color: rgb(255,255,255);">8</td>
     <td class="xl81" style="border-top:none;border-left:none">10</td>
     <td class="xl81" style="border-top:none;border-left:none">12</td>
     <td class="xl87" style="border-top:none;border-left:none">15</td>
     <td class="xl87" style="border-top:none;border-left:none">17</td>
     <td class="xl87" style="border-top:none;border-left:none">19</td>
     <td colspan="4" class="xl149" style="border-right:1.0pt solid black;border-left:
     none;;background-color:rgb(255, 255, 255);">1 PARCIAL</td>
     <td colspan="4" class="xl149" style="border-right:1.0pt solid black;border-left:
     none;;background-color:rgb(255, 255, 255);">2 PARCIAL</td>
     <td colspan="8" class="xl149" style="border-right:1.0pt solid black;border-left:
     none;background-color:rgb(255, 255, 255);">FINAL</td>
     <td class="xl88"></td>
     <td class="xl88"></td>
     <td class="xl88"></td>
     <td class="xl88"></td>
     <td class="xl88"></td>
    </tr>
    <tr class="xl65" height="106" style="mso-height-source:userset;height:80.1pt">
     <td height="106" class="xl92" style="height:80.1pt;border-top:none;background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl92" style="border-top:none;background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl92" style="border-top:none;background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl92" style="border-top:none;background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl92" style="border-top:none;background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl92" style="border-top:none;background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl93" style="border-top:none;border-left:none;background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl93" style="border-top:none;border-left:none;background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl93" align="right" style="border-top:none;border-left:none;background-color: rgb(255,255,255);"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
     <td class="xl93" style="border-top:none;border-left:none;background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl93" style="border-top:none;border-left:none;background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl94" style="border-top:none;border-left:none;background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl95" style="border-top:none">&nbsp;</td>
     <td class="xl96">&nbsp;</td>
     <td class="xl96">&nbsp;</td>
     <td class="xl97">&nbsp;</td>
     <td class="xl97">&nbsp;</td>
     <td class="xl97">&nbsp;</td>
     <td class="xl98">&nbsp;</td>
     <td class="xl99" style="border-top:none;border-left:none">&nbsp;</td>
     <td class="xl100" style="border-left:none">&nbsp;</td>
     <td class="xl100" style="border-left:none">&nbsp;</td>
     <td class="xl97" style="border-top:none">&nbsp;</td>
     <td class="xl101" style="border-top:none;border-left:none">&nbsp;</td>
     <td class="xl102" style="border-top:none">&nbsp;</td>
     <td class="xl103" style="border-top:none">&nbsp;</td>
     <td class="xl97" style="border-top:none">&nbsp;</td>
     <td class="xl97" style="border-top:none">&nbsp;</td>
     <td class="xl98" style="border-top:none">&nbsp;</td>
     <td class="xl98" style="border-top:none">&nbsp;</td>
     <td class="xl104" width="16" style="border-top:none;border-left:none;width:12pt">&nbsp;</td>
     <td class="xl104" width="16" style="border-top:none;width:12pt">&nbsp;</td>
     <td class="xl104" width="16" style="border-top:none;width:12pt">&nbsp;</td>
     <td class="xl105" width="16" style="border-top:none;width:12pt">&nbsp;</td>
     <td class="xl105" width="16" style="border-top:none;width:12pt">&nbsp;</td>
     <td class="xl105" width="16" style="border-top:none;width:12pt">&nbsp;</td>
     <td class="xl105" width="16" style="width:12pt">&nbsp;</td>
     <td class="xl105" width="16" style="width:12pt">&nbsp;</td>
     <td class="xl106" style="border-top:none">&nbsp;</td>
     <td class="xl107">&nbsp;</td>
     <td class="xl100" style="border-left:none">&nbsp;</td>
     <td class="xl100" style="border-left:none">&nbsp;</td>
     <td class="xl100" style="border-left:none">&nbsp;</td>
     <td class="xl100" style="border-left:none">&nbsp;</td>
     <td class="xl107">&nbsp;</td>
     <td class="xl108">&nbsp;</td>
     <td class="xl108">&nbsp;</td>
     <td class="xl108">&nbsp;</td>
     <td class="xl109" align="right" style="border-top:none;writing-mode: vertical-lr;transform: rotate(180deg);text-align:left;background-color: rgb(255, 255, 255);">Suma</td>
     <td class="xl110" align="right" style="border-top:none;border-left:none;writing-mode: vertical-lr;transform: rotate(180deg);text-align:left;background-color: rgb(255, 255, 255);">Promedio</td>
     <td class="xl110" align="right" style="border-top:none;border-left:none;writing-mode: vertical-lr;transform: rotate(180deg);text-align:left;background-color: rgb(255, 255, 255);">Criterio
     de Integración</td>
     <td class="xl111" align="right" style="border-top:none;border-left:none;writing-mode: vertical-lr;transform: rotate(180deg);text-align:left;background-color: rgb(255, 255, 255);">Resultado</td>
     <td class="xl112" align="right" style="border-top:none;writing-mode: vertical-lr;transform: rotate(180deg);text-align:left;background-color: rgb(255, 255, 255);">Suma</td>
     <td class="xl110" align="right" style="border-top:none;border-left:none;writing-mode: vertical-lr;transform: rotate(180deg);text-align:left;background-color: rgb(255, 255, 255);">Promedio</td>
     <td class="xl110" align="right" style="border-top:none;border-left:none;writing-mode: vertical-lr;transform: rotate(180deg);text-align:left;background-color: rgb(255, 255, 255);">Criterio
     de Integración</td>
     <td class="xl113" align="right" style="border-top:none;border-left:none;writing-mode: vertical-lr;transform: rotate(180deg);text-align:left;background-color: rgb(255, 255, 255);">Resultado</td>
     <td class="xl109" align="right" style="border-top:none;writing-mode: vertical-lr;transform: rotate(180deg);text-align:left;background-color: rgb(255, 255, 255);">Suma</td>
     <td class="xl110" align="right" style="border-top:none;border-left:none;writing-mode: vertical-lr;transform: rotate(180deg);text-align:left;background-color: rgb(255, 255, 255);">Promedio</td>
     <td class="xl110" align="right" style="border-top:none;border-left:none;writing-mode: vertical-lr;transform: rotate(180deg);text-align:left;background-color: rgb(255, 255, 255);">Criterio
     de Integración</td>
     <td class="xl111" align="right" style="border-top:none;border-left:none;writing-mode: vertical-lr;transform: rotate(180deg);text-align:left;background-color: rgb(255, 255, 255);">Resultado</td>
     <td class="xl91" align="right" style="border-left:none;writing-mode: vertical-lr;transform: rotate(180deg);text-align:left;">Calificación</td>
     <td class="xl114" align="right" style="border-left:none;writing-mode: vertical-lr;transform: rotate(180deg);text-align:left;">Asistencia</td>
     <td class="xl114" align="right" style="border-left:none;writing-mode: vertical-lr;transform: rotate(180deg);text-align:left;">% Asistencia</td>
     <td class="xl115" align="right" style="border-left:none;writing-mode: vertical-lr;transform: rotate(180deg);text-align:left;">Faltas</td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl89"></td>
     <td class="xl65"></td>
    </tr>

<?php foreach ($alumnos->result() as $alumnos){?>
<!--Fila del alumno-->
    <tr class="xl65" height="20" style="mso-height-source:userset;height:15.0pt">
     <td height="20" class="xl116" style="height:15.0pt"></td>
     <td colspan="8" class="xl127" style="border-left:none;background-color: rgb(255,255,255);"><strong><?= $alumnos->Alumno ?></strong></td>
     <td class="xl117" style="border-left:none;background-color:rgb(255, 255, 255);">&nbsp;</td>
     <td class="xl117" style="border-left:none;background-color:rgb(255, 255, 255);">&nbsp;</td>
     <td class="xl117" style="border-left:none;background-color:rgb(255, 255, 255);">&nbsp;</td>
     <td class="xl117" style="border-left:none;background-color:rgb(255, 255, 255);">&nbsp;</td>
     <td class="xl117" style="border-left:none;background-color:rgb(255, 255, 255);">&nbsp;</td>
     <td class="xl117" style="border-left:none;background-color:rgb(255, 255, 255);">&nbsp;</td>
     <td class="xl117" style="border-left:none;background-color:rgb(255, 255, 255);">&nbsp;</td>
     <td class="xl117" style="border-left:none;background-color:rgb(255, 255, 255);">&nbsp;</td>
     <td class="xl117" style="border-left:none;background-color:rgb(255, 255, 255);">&nbsp;</td>
     <td class="xl117" style="border-left:none;background-color:rgb(255, 255, 255);">&nbsp;</td>
     <td class="xl117" style="border-left:none;background-color:rgb(255, 255, 255);">&nbsp;</td>
     <td class="xl117" style="border-left:none;background-color:rgb(255, 255, 255);">&nbsp;</td>
     <td class="xl118" style="border-left:none">&nbsp;</td>
     <td class="xl118" style="border-left:none">&nbsp;</td>
     <td class="xl118" style="border-left:none">&nbsp;</td>
     <td class="xl119" style="border-left:none">&nbsp;</td>
     <td class="xl119" style="border-left:none">&nbsp;</td>
     <td class="xl119" style="border-left:none">&nbsp;</td>
     <td class="xl119" style="border-left:none">&nbsp;</td>
     <td class="xl119" style="border-left:none">&nbsp;</td>
     <td class="xl119" style="border-left:none">&nbsp;</td>
     <td class="xl119" style="border-left:none">&nbsp;</td>
     <td class="xl119" style="border-left:none">&nbsp;</td>
     <td class="xl119" style="border-left:none">&nbsp;</td>
     <td class="xl119" style="border-left:none">&nbsp;</td>
     <td class="xl119" style="border-left:none">&nbsp;</td>
     <td class="xl119" style="border-left:none">&nbsp;</td>
     <td class="xl119" style="border-left:none">&nbsp;</td>
     <td class="xl119" style="border-left:none">&nbsp;</td>
     <td class="xl119" style="border-left:none">&nbsp;</td>
     <td class="xl118" style="border-left:none">&nbsp;</td>
     <td class="xl118" style="border-left:none">&nbsp;</td>
     <td class="xl118" style="border-left:none">&nbsp;</td>
     <td class="xl119" style="border-left:none">&nbsp;</td>
     <td class="xl119" style="border-left:none">&nbsp;</td>
     <td class="xl119" style="border-left:none">&nbsp;</td>
     <td class="xl119" style="border-left:none">&nbsp;</td>
     <td class="xl119" style="border-left:none">&nbsp;</td>
     <td class="xl119" style="border-left:none">&nbsp;</td>
     <td class="xl117" style="border-left:none;background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl117" style="border-left:none;background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl117" style="border-left:none;background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl117" style="border-left:none;background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl119" style="border-left:none">&nbsp;</td>
     <td class="xl119" style="border-left:none">&nbsp;</td>
     <td class="xl118" style="border-left:none">&nbsp;</td>
     <td class="xl118" style="border-left:none">&nbsp;</td>
     <td class="xl118" style="border-left:none">&nbsp;</td>
     <td class="xl117" style="border-left:none;background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl117" style="border-left:none;background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl117" style="border-left:none;background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl117" style="border-left:none;background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl117" style="border-left:none;background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl117" style="border-left:none;background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl117" style="border-left:none;background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl117" style="border-left:none;background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl117" style="border-left:none;background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl117" style="border-left:none;background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl117" style="border-left:none;background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl117" style="border-left:none;background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl117" style="border-left:none;background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl117" style="border-left:none;background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl117" style="border-left:none;background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl120" style="border-left:none;background-color: rgb(255,255,255);">&nbsp;</td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
     <td class="xl65"></td>
    </tr>
    <!--Fila del alumno-->
<?php
}
?>
   
   
   </tbody>
</table>
<!-- Page de excel-->
<br>
</div>
<div class="row">
<div class="col-5"></div>
<div class="col-2">
<button type="button" class="btn btn-primary btn-lg border-15px " onclick="geneatePDF()">Imprimir</button>
</div>
<div class="col-5 mb-4"></div>
</div>

<!--Cuerpo de la lista y script de descarga-->
<!--Script de impresion-->
<script type="text/javascript">
  
  function geneatePDF(){

const $elementoParaConvertir = document.getElementById('Impresion_2'); // <-- Aquí puedes elegir cualquier elemento del DOM
console.log($elementoParaConvertir)
html2pdf()
    .set({
        margin: 1,
        filename: 'Lista_Alumnos.pdf',
        image: {
            type: 'jpeg',
            quality: 0.98
        },
        html2canvas: {
            scale: 8, // A mayor escala, mejores gráficos, pero más peso
            letterRendering: true,
        },
        jsPDF: {
            unit: "in",
            format: "a3",
            orientation: 'landscape' // landscape o portrait
        }
    })
    .from($elementoParaConvertir)
    .save()
    .catch(err => console.log(err));
  }

</script>
<!--script de impresion-->
<!--Cuerpo de la lista-->
</div>

</div>

</div>

<!-- BODY-->


</div>


</div>
</div>

<!-- Container-fluid Ends-->
</div>

<!-- footer start-->
<footer class="footer" style="margin-left: auto; margin-right: auto;">
<div class="row">
<div class="col-md-6 footer-copyright">
<p class="mb-0">Copyright 2022-23 © IMEI / CEP</p>
</div>
<div class="col-md-6">
<p class="pull-right mb-0">Made by "Hack the box" with <i class="fa fa-heart font-secondary"></i></p>
</div>
</div>

</footer>
</div>
</div>





<?=$this->load->view('Include/base_js','',TRUE);?>



</body>
</html>