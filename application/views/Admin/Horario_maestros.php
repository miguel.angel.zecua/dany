
<html lang="en">
<head>
<title>Horarios Maestros</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Integra-Educa">
<meta name="keywords" content="Integra-Educa, Integra">
<meta name="author" content="Mike">

<?=$this->load->view('Include/base_css','',TRUE);?>
<script type="text/javascript" src="<?=base_url('public/assets/js/html2/html2pdf.bundle.min.js')?>"></script>

<script type="text/javascript" src="<?=base_url('public/assets/js/html2/html2pdf.bundle.min.js')?>"></script>

<link rel="stylesheet" type="text/css" href="<?=base_url('public/assets/css/table.css')?>">


<link rel="stylesheet" type="text/css" href="<?=base_url('public/assets/css/feather-icon.css')?>">


<link rel="stylesheet" type="text/css" href="<?=base_url('public/assets/css/datatables.css')?>">

<script src="<?php echo base_url('public/assets/js/Jquery/jquery.js') ?>"></script>

</head>
<body>

<!-- Loader starts-->
<div class="loader-wrapper">
<div class="theme-loader">    
<div class="loader-p"></div>
</div>
</div>
<!-- Loader ends-->


<!-- page-wrapper Start-->
<div class="page-wrapper" id="pageWrapper">

<!-- Page Header Start-->
<?=$this->load->view('Admin/Header','',TRUE);?>
<!-- Page Header Start-->


<!-- Page Body Start-->
<div class="page-body-wrapper horizontal-menu">
 
<!-- Page Sidebar Inicio-->
<?=$this->load->view('Admin/Side_bar','',TRUE);?>
<!-- Page Sidebar Fin-->

<!-- TITULOS PAGINA-->
<div class="page-body">
<div class="container-fluid">
<div class="page-header">
<div class="row ">
<div class="col-sm-6 ">
<h3>Horarios Maestros</h3>
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="index.html">Inicio</a></li>
<li class="breadcrumb-item">Home</li>
</ol>
</div>
</div>
</div>
</div>
<!-- TITULOS PAGINA-->


<!-- Modal -->
<div class="modal fade" id="horario" tabindex="-1" aria-labelledby="myLargeModalLabel"  aria-modal="true" role="dialog">
<div class="modal-dialog modal-xl">
<div class="modal-content">
    <div id="impresion">
<div class="modal-header">
<h4 class="modal-title" id="exampleModalLabel" style="font-weight: bold; font-size: 20px;"></h4>
<button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close" data-bs-original-title="" title=""></button>
</div>

<div class="modal-body">

<div class="row">

<div class="col-sm-12 col-xl-12 col-lg-12">

<div class="table-responsive" style="border-radius: 25px !important; font-family: sans-serif;">

<table class="table table-hover table-bordered align-content-center table-secondary" >

<thead class="table-dark">
<tr class="mt-1">
<th scope="col" style="font-size: 16px; width: 25%" class=" d-none text-center text-white">Profesor</th>
<th scope="col" style="font-size: 16px; width: 25%" class="text-center text-white">Materia</th>
<th scope="col" style="font-size: 16px; width: 25%" class="text-center text-white">Cuatrimestre</th>
<th scope="col" style="font-size: 16px; width: 6%" class="text-center text-white">Dia</th>
<th scope="col" style="font-size: 16px; width: 6%" class="text-center text-white">Turno</th>
<th scope="col" style="font-size: 16px; width: 10%" class="text-center text-white">Hora Inicio</th>
<th scope="col" style="font-size: 16px; width: 10%" class="text-center text-white">Hora Fin</th>
</tr>
</thead>


<tbody class="mt-2" id="records_content">




</tbody>
</table>

<br>
</div>
<div class="row">
<div class="col-5"></div>
<div class="col-2 justify-content-center">
<button type="button" class="btn btn-primary btn-lg " id="boton" onclick="geneatePDF()">Descargar</button>
</div>
<div class="col-5"></div>
</div>
</div>
</div>

</div>
</div>
</div>
</div>
</div>
<!-- Modal -->


<div class="row justify-content-center mr-2 ml-2">
<div class="col-sm-12 col-xl-12 col-lg-12">
</div>
<span class="badge badge-warning text-dark" style="font-size: 22px; border-radius: 25px;">-Sistema Integral de Información-</span>
</div>

<br>



<!-- Container-fluid starts-->
<div class="container-fluid">

<!-- BODY-->

<div class="row justify-content-center">
  


<div class="col-sm-12">

<h2 class="text-center"><strong>Horarios Maestros</strong></h2>

<div class="card" style="background-color: #D0ECE7 ; border-radius: 50px;">

<div class="card-body">
<div class="table-responsive">
<div id="basic-1_wrapper" class="dataTables_wrapper no-footer">

<table class="display dataTable no-footer" id="basic-1" role="grid" aria-describedby="basic-1_info">
<thead>
<tr role="row">
<th class="sorting_asc text-center" tabindex="0" aria-controls="basic-1" rowspan="1" colspan="1" aria-sort="ascending" style="width: 123.188px;">Matricula</th>

<th class="sorting text-center" tabindex="0" aria-controls="basic-1" rowspan="1" colspan="1" style="width: 208.141px;">Nombre</th>


<th class="sorting text-center" tabindex="0" aria-controls="basic-1" rowspan="1" colspan="1" style="width: 34.6406px;">Ver Horario</th>


</thead>

<tbody>
                          
 <?php foreach ($lista->result() as $lista){?>
                          
<tr role="row" class="odd">
<td class="text-center" style="font-size: 16px;"><strong><?= $lista->Matricula_Profesor ?></strong></td>
<td class="text-center" style="font-size: 16px;"><strong><?= $lista->nombre ?></strong></td>

<td class="text-center" style="font-size: 16px;"><button onclick="GetUserDetails('<?= $lista->Matricula_Profesor ?>')" class="btn btn-warning"><i data-feather="eye" style="font-size: 28px; color: black; height: 18px;"></i></button></td>
</tr>

<?php
}
?>



</tbody>

</table>


</div>
</div>
</div>
</div>
</div>


</div>
<script type="text/javascript">

function GetUserDetails(id) 
{
$.ajax({
url:"./Time_profesor",
type:"POST",
cache:false,
data:{id:id},
success:function(data){
$("#records_content").html(data);
var nombre_prof = $('#nombre_prof').text();
$('#exampleModalLabel').text("HORARIO DEL DOCENTE: " + nombre_prof);
console.log (nombre_prof);
$("#horario").modal("show");
}
}
);

}

function geneatePDF(){

const $elementoParaConvertir = document.getElementById('impresion'); // <-- Aquí puedes elegir cualquier elemento del DOM
console.log($elementoParaConvertir)
html2pdf()
    .set({
        margin: 1,
        filename: 'Horario.pdf',
        image: {
            type: 'jpeg',
            quality: 0.98
        },
        html2canvas: {
            scale: 10, // A mayor escala, mejores gráficos, pero más peso
            letterRendering: true,
        },
        jsPDF: {
            unit: "in",
            format: "a4",
            orientation: 'landscape' // landscape o portrait
        }
    })
    .from($elementoParaConvertir)
    .save()
    .catch(err => console.log(err));
  //document.getElementById('boton').style.display='none';

  }

</script>

</div>

<!-- BODY-->

</div>


</div>
</div>

<!-- Container-fluid Ends-->
</div>

<!-- footer start-->
<footer class="footer" style="margin-left: auto; margin-right: auto;">
<div class="row">
<div class="col-md-6 footer-copyright">
<p class="mb-0">Copyright 2022-23 © IMEI / CEP</p>
</div>
<div class="col-md-6">
<p class="pull-right mb-0">Made by "Hack the box" with <i class="fa fa-heart font-secondary"></i></p>
</div>
</div>

</footer>
</div>
</div>
<?=$this->load->view('Include/base_js','',TRUE);?>



<script src="<?=base_url('public/assets/js/datatable/datatables/jquery.dataTables.min.js')?>"></script>

<script src="<?=base_url('public/assets/js/datatable/datatables/datatable.custom.js')?>"></script>


</body>
</html>