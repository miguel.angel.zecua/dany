
<html lang="en">
<head>
<title>Notificaciones Maestros</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Integra-Educa">
<meta name="keywords" content="Integra-Educa, Integra">
<meta name="author" content="Mike">

<?=$this->load->view('Include/base_css','',TRUE);?>

<link rel="stylesheet" type="text/css" href="<?=base_url('public/assets/css/datatables.css')?>">

</head>
<body>

<!-- Loader starts-->
<div class="loader-wrapper">
<div class="theme-loader">    
<div class="loader-p"></div>
</div>
</div>
<!-- Loader ends-->


<!-- page-wrapper Start-->
<div class="page-wrapper" id="pageWrapper">

<!-- Page Header Start-->
<?=$this->load->view('Admin/Header','',TRUE);?>
<!-- Page Header Start-->


<!-- Page Body Start-->
<div class="page-body-wrapper horizontal-menu">
 
<!-- Page Sidebar Inicio-->
<?=$this->load->view('Admin/Side_bar','',TRUE);?>
<!-- Page Sidebar Fin-->

<!-- TITULOS PAGINA-->
<div class="page-body">
<div class="container-fluid">
<div class="page-header">
<div class="row ">
<div class="col-sm-6 ">
<h3>Notificaciones Maestros</h3>
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="index.html">Inicio</a></li>
<li class="breadcrumb-item">Home</li>
</ol>
</div>
</div>
</div>
</div>
<!-- TITULOS PAGINA-->

<!--Modal para CRUD-->
<div class="modal fade" id="Edit" tabindex="-1" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title" id="exampleModalLabel">Editar Notificación </h5>
<button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close" data-bs-original-title="" title=""></button>
</div>
<div class="modal-body">
                              
<!--BODY MODAL-->
<form id="formData">

<div class="form-group row">
<div class="col-sm-6 col-xl-6 col-lg-6">
<label for="exampleInputEmail1">Usuario</label>
<input type="email" class="form-control" id="view_usuario" name="view_usuario" readonly>   
</div>

<div class="col-sm-6 col-xl-6 col-lg-6">
<label for="exampleInputEmail1">Id</label>
<input type="email" class="form-control" id="id_view" name="id_view" readonly>   
</div>
</div>


<div class="form-group">
<label for="exampleInputPassword1">Prioridad</label>
<select class="form-control" id="view_prioridad" name="view_prioridad">
<option value="1">Urgente</option>
<option value="2">Pendiente</option>
<option value="3">Opcional</option>
</select>
</div>

<div class="form-group">
<label for="exampleInputEmail1">Texto</label>
<textarea class="form-control" aria-label="With textarea" id="view_texto" name="view_texto" placeholder="Escriba el Texto"></textarea>
</div>



<!--BODY MODAL-->

</div>
<div class="modal-footer">
<button class="btn btn-danger" type="button" data-bs-dismiss="modal" >Cerrar</button>
<button class="btn btn-primary" type="submit">Guardar</button>
</div>
</form>
</div>
</div>
</div>
<!--Modal para CRUD-->


<!-- MODAL 2-->
<div class="modal fade" id="New_Noty" tabindex="-1" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title" id="exampleModalLabel">Nueva Notificación </h5>
<button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close" data-bs-original-title="" title=""></button>
</div>
<div class="modal-body">
<!--BODY MODAL-->

<form id="newData">

<div class="form-group row">
<div class="col-sm-12 col-xl-12 col-lg-12">
<label for="exampleInputEmail1">Usuario</label>
<input type="email" class="form-control" id="new_usuario" name="new_usuario" readonly>   
</div>

</div>


<div class="form-group">
<label for="exampleInputPassword1">Prioridad</label>
<select class="form-control" id="new_prioridad" name="new_prioridad">
<option value="1">Urgente</option>
<option value="2">Pendiente</option>
<option value="3">Opcional</option>
</select>
</div>

<div class="form-group">
<label for="exampleInputEmail1">Texto</label>
<textarea class="form-control" aria-label="With textarea" id="new_texto" name="new_texto" placeholder="Escriba el Texto"></textarea>
</div>


<!--BODY MODAL-->
</div>
<div class="modal-footer">
<button class="btn btn-danger text-dark" type="button" data-bs-dismiss="modal" data-bs-original-title="" title="">Cerrar</button>
<button class="btn btn-warning text-dark" type="submit" data-bs-original-title="" title="">Asignar</button>
</div>
</form>
</div>
</div>
</div>
<!-- MODAL 2-->


<div class="row justify-content-center mr-2 ml-2">
<div class="col-sm-12 col-xl-12 col-lg-12">
</div>
<span class="badge badge-warning text-dark" style="font-size: 22px; border-radius: 25px;">-Sistema Integral de Información-</span>
</div>

<br>




<!-- Container-fluid starts-->
<div class="container-fluid">


<div class="row">
<div class="col-lg-12">            
<button class="btn btn-primary text-center" type="button" data-bs-toggle="modal" data-original-title="test" data-bs-target="#New_Noty" data-bs-original-title="" title="Nueva Notificación" id="New_Dat">
<i data-feather="plus" class="justify-content-center text-center  mr-2 ml-2"></i>
</button>
</div>    
</div>  


<!-- BODY-->
<div class="row">
    
<div class="col-sm-12 col-xl-12 col-lg-12">

<div class="table-responsive mt-4" style="border-radius:25px;">
<table class="table table-borderless display nowrap table-danger text-center" id="tablaUsuarios" style="width:100%;">
<thead>
<tr>
<th>Id</th>
<th>Descripcion</th>
<th>Prioridad</th>

<th>Observaciones</th>

</tr>
</thead>
</table>
</div>

</div>

</div>

</div>

<!-- BODY-->


</div>


</div>
</div>

<!-- Container-fluid Ends-->
</div>




<!-- footer start-->
<footer class="footer" style="margin-left: auto; margin-right: auto;">
<div class="row">
<div class="col-md-6 footer-copyright">
<p class="mb-0">Copyright 2022-23 © IMEI / CEP</p>
</div>
<div class="col-md-6">
<p class="pull-right mb-0">Made by "Hack the box" with <i class="fa fa-heart font-secondary"></i></p>
</div>
</div>

</footer>
</div>
</div>





<?=$this->load->view('Include/base_js','',TRUE);?>


<!-- jquery y bootstrap -->
<script src="https://code.jquery.com/jquery-3.5.1.js"></script> 
<script src="<?=base_url('public/assets/js/datatable/datatables/jquery.dataTables.min.js')?>"></script>

<script src="<?=base_url('public/assets/js/datatable/datatables/datatable.custom.js')?>"></script>
<script src="<?=base_url('especials/sweet/sweetalert2@11.js')?>"></script>
<script type="text/javascript">

var tablaUsuarios;


document.addEventListener("DOMContentLoaded", function(event) {

//CARGAR TABLA
tablaUsuarios = $('#tablaUsuarios').DataTable({  
"ajax":{            
"url": "../Read_notify_maestro", 
"method": 'POST',
"data":{Id:sessionStorage.getItem('Id')},
"dataSrc":""
},
"columns":[
{"data": "Id"},
{"data": "descripcion"},
{"data": "prioridad"},


{"defaultContent": "<div class='text-center'><div class='btn-group'><button class='btn btn-primary btn-sm btnEditar'><i class='material-icons'>EDITAR</i></button><button class='btn btn-danger btn-sm btnBorrar'><i class='material-icons'>ELIMINAR</i></button></div></div>"}
]
}); 
//CARGAR TABLA  



//OPEN EDITAR
$(document).on("click", ".btnEditar", function()
{		        
fila = $(this).closest("tr");	       
$("#view_usuario").val(sessionStorage.getItem('Id'));
$("#id_view").val(fila.find('td:eq(0)').text());
$("#view_prioridad option[value="+ fila.find('td:eq(2)').text() +"]").attr("selected",true);
$("#view_texto").val(fila.find('td:eq(1)').text());
$('#Edit').modal('show');
		   
});
//OPEN EDITAR

//OPEN NEW
$(document).on("click", "#New_Dat", function()
{		        
$("#new_usuario").val(sessionStorage.getItem('Id'));
});
//OPEN NEW

//ENVIAR NUEVO
$('#newData').submit(function(e){                         
e.preventDefault();
new_usuario = $.trim($('#new_usuario').val());    
new_prioridad = $.trim($('#new_prioridad').val());    
new_texto = $.trim($('#new_texto').val());    

$.ajax({
url: "../New_notify_maestro",
type: "POST",
datatype:"json",    
data:  {new_usuario:new_usuario, new_prioridad:new_prioridad, new_texto:new_texto},    
success: function(data) 
{
Swal.fire('¡LISTO!');	
$('#New_Noty').modal('hide');	
tablaUsuarios.ajax.reload(null, false);		
}
});			        
										     			
});
//ENVIAR NUEVO

//ACTUALIZAR
$('#formData').submit(function(e){                         
e.preventDefault();
usuario = $.trim($('#view_usuario').val());    
id = $.trim($('#id_view').val());
prioridad = $.trim($('#view_prioridad').val());    
texto = $.trim($('#view_texto').val());    

$.ajax({
url: "../Edit_notify_maestro",
type: "POST",
datatype:"json",    
data:  {usuario:usuario, id:id, prioridad:prioridad, texto:texto},    
success: function(data) 
{
Swal.fire('¡Actualizado!')
tablaUsuarios.ajax.reload(null, false);
}
});			        
$('#Edit').modal('hide');											     			
});
//ACTUALIZAR

//ELIMINAR
$(document).on("click", ".btnBorrar", function(){
fila = $(this);           
Id_Noty = parseInt($(this).closest('tr').find('td:eq(0)').text()) ;		

Swal.fire({
title: '¿Desea Elimnar la notificacion? '+Id_Noty,
showDenyButton: true,
showCancelButton: true,
confirmButtonText: 'SI',
denyButtonText: 'NO',
}).then((result) => 
{
if (result.isConfirmed) 
{

$.ajax({
url: "../Delete_notify_maestro",
type: "POST",   
data:  {Id_Noty:Id_Noty},    
success: function(data) 
{
tablaUsuarios.row(fila.parents('tr')).remove().draw();                  
}
});	

Swal.fire('¡ELIMINADA!', '', 'success')
} else if (result.isDenied) {
Swal.fire('Los cambios no se guardaron', '', 'info')
}
})

});
//ELIMINAR


  
});




</script>



</body>
</html>