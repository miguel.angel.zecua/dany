
<html lang="en">
<head>
<title>Estadisticas</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Integra-Educa">
<meta name="keywords" content="Integra-Educa, Integra">
<meta name="author" content="Mike">

<?=$this->load->view('Include/base_css','',TRUE);?>
<script type="text/javascript" src="<?=base_url('public/assets/js/html2/html2pdf.bundle.min.js')?>"></script>

<link rel="stylesheet" type="text/css" href="<?=base_url('public/assets/css/table.css')?>">


<link rel="stylesheet" type="text/css" href="<?=base_url('public/assets/css/feather-icon.css')?>">


<link rel="stylesheet" type="text/css" href="<?=base_url('public/assets/css/datatables.css')?>">

<script src="<?php echo base_url('public/assets/js/Jquery/jquery.js') ?>"></script>

</head>
<body>

<!-- Loader starts-->
<div class="loader-wrapper">
<div class="theme-loader">    
<div class="loader-p"></div>
</div>
</div>
<!-- Loader ends-->


<!-- page-wrapper Start-->
<div class="page-wrapper" id="pageWrapper">

<!-- Page Header Start-->
<?=$this->load->view('Admin/Header','',TRUE);?>
<!-- Page Header Start-->


<!-- Page Body Start-->
<div class="page-body-wrapper horizontal-menu">
 
<!-- Page Sidebar Inicio-->
<?=$this->load->view('Admin/Side_bar','',TRUE);?>
<!-- Page Sidebar Fin-->

<!-- TITULOS PAGINA-->
<div class="page-body">
<div class="container-fluid">
<div class="page-header">
<div class="row ">
<div class="col-sm-6 ">
<h3>Estadisticas</h3>
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="index.html">Inicio</a></li>
<li class="breadcrumb-item">Home</li>
</ol>
</div>
</div>
</div>
</div>
<!-- TITULOS PAGINA-->



<div class="row justify-content-center mr-2 ml-2">
<div class="col-sm-12 col-xl-12 col-lg-12">
</div>
<span class="badge badge-warning text-dark" style="font-size: 22px; border-radius: 25px;">-Sistema Integral de Información-</span>
</div>

<br>



<!-- Container-fluid starts-->
<div class="container-fluid">

<!-- BODY-->

<div class="row justify-content-center">
  


<div class="col-sm-12 align-items-center">


<div class="card" style="background-color: #D0ECE7 ; border-radius: 50px;">

<div class="card-body">




<canvas id="myChart" width="400" height="100"></canvas>

</div>

</div>


</div>


</div>
</div>

<!-- BODY-->



</div>


</div>
</div>

<!-- Container-fluid Ends-->
</div>

<!-- footer start-->
<footer class="footer" style="margin-left: auto; margin-right: auto;">
<div class="row">
<div class="col-md-6 footer-copyright">
<p class="mb-0">Copyright 2022-23 © IMEI / CEP</p>
</div>
<div class="col-md-6">
<p class="pull-right mb-0">Made by "Hack the box" with <i class="fa fa-heart font-secondary"></i></p>
</div>
</div>

</footer>
</div>
</div>
<?=$this->load->view('Include/base_js','',TRUE);?>



<script src="<?=base_url('public/assets/js/datatable/datatables/jquery.dataTables.min.js')?>"></script>

<script src="<?=base_url('public/assets/js/datatable/datatables/datatable.custom.js')?>"></script>

<script src="<?=base_url('library/src/plugins/sweetalert2/sweetalert2.all.js')?>"></script>
<script src="<?=base_url('library/src/plugins/sweetalert2/sweet-alert.init.js')?>"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.1/chart.min.js"></script>



<script type="text/javascript">
    function miFunc() 
    {
$.ajax({

url:"<?=base_url('Admin/Datos')?>",
data:{
materia:'<?=$this->uri->segment(3)?>',
maestro:'<?=$this->uri->segment(4)?>'

},
type:'POST'
}).done(function(resp){
var dat = JSON.parse(resp);
var result = [];

for(var i in dat){
    result.push([i, dat [i]]);
}



//---------
const ctx = document.getElementById('myChart').getContext('2d');
const myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['preg 1', 'Preg 2', 'Preg 3', 'Preg 4', 'Preg 5', 'Preg 6','Preg 7','Preg 8','Preg 9','Preg 10','Preg 11','Preg 12','Preg 13','Preg 14','Preg 15','Preg 16','Preg 17','Preg 18','Preg 19','Preg 20','Preg 21','Preg 22','Preg 23','Preg 24'],
        datasets: [{
            label: '# of Votes_2',

            data: [dat['preg_1'], dat['preg_2'], dat['preg_3'], dat['preg_4'], dat['preg_5'], dat['preg_6'], dat['preg_7'], dat['preg_8'], dat['preg_9'], dat['preg_10'], dat['preg_11'], dat['preg_12'], dat['preg_13'], dat['preg_14'], dat['preg_15'], dat['preg_16'], dat['preg_17'], dat['preg_18'], dat['preg_19'], dat['preg_20'], dat['preg_21'], dat['preg_22'], dat['preg_23'], dat['preg_24']],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
      options: {
        plugins: {
            legend: {
                display: true,

               
                labels: {
                    color: 'rgb(25500, 99, 132)',
                     text: 'Chart Title'
                }
            }
        }
    }
});
//-----------

})
    }

    window.onload=miFunc;

</script>

</body>
</html>