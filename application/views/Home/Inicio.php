<!--Pagina de inicio de IMEI agregado al nuevo sistemas -->

<!DOCTYPE html>
<html>

<!-- HEAD -->
<head>
<meta charset="utf-8">
<title>SISTEMA DE INTEGRAL INFORMACIÓN INTEGRA-EDUCA</title>
<!-- CSS -->
<?=$this->load->view('Include/Head','',TRUE);?>
<!-- CSS -->
</head>
<!-- HEAD -->

<body>

<!-- PRE-CARGA -->
<?=$this->load->view('Include/PreCarga','',TRUE);?>
<!-- PRE-CARGA -->




<!-- CONTENEDOR-->	
<div class="main-container" style="padding-left: 0px; padding-top: 50px;">
<div class="xs-pd-20-10">

<div class="title text-center">
<h2 class="h3 mb-0 mt-0" style="letter-spacing: 0.18rem; font-family: Arial, Helvetica, sans-serif;"><span class="badge badge-info " style="border-radius: 1000px;">SISTEMA INTEGRAL DE INFORMACIÓN INTEGRA-EDUCA</span></h2>
</div>
<br>
<hr>




<div class="title pb-20 pt-20 text-center">
<h2 class="h3 mb-0"><span class="badge badge-success" style="border-radius: 50px;">INICIO DE SESIÓN</span></h2>
</div>

<!-- FILA DE CARDS-->	
<div class="row justify-content-center">

<!-- CARD 1-->	
<div class="col-md-3 mb-30 conten ml-2 mr-2" >
<div class="card-box pricing-card mt-30 mb-30 ">
<a href="<?=base_url('Profesores/Login_profesores')?>">
<div class="pricing-icon rounded-circle">
<img src="<?=base_url('library/vendors/images/Docente.png')?>" class="imagen">
</div>
<div class="pricing-price" style="font-size: 25px;"><strong class="text-dark">DOCENTES</strong></div>
<div class="cta">
<a class="btn btn-info btn-rounded btn-lg"></a>
</div>
</a>
</div>
</div>
<!-- CARD 1-->	

<!-- CARD 2-->	
<div class="col-md-3 mb-30 conten ml-2 mr-2" >
<div class="card-box pricing-card mt-30 mb-30">
<a href="<?=base_url('Alumnos/Login_Alumnos')?>">
<div class="pricing-icon rounded-circle">
<img src="<?=base_url('library/vendors/images/Alumno.png')?>" alt="">
</div>
<div class="pricing-price" style="font-size: 25px;"><strong class="text-dark">ALUMNOS</strong></div>
<div class="cta">
<a  class="btn btn-warning btn-rounded btn-lg"></a>
</div>
</a>
</div>
</div>
<!-- CARD 2-->	

</div>
<!-- FILA DE CARDS-->	

<!-- FOOTER-->	
<div class="footer-wrap pd-20 mb-20 card-box d-none">Power by Mike</div>
<!-- FOOTER-->	

</div>
</div>
<!-- CONTENEDOR-->


<style type="text/css">
	

.conten:hover {
-webkit-transform:scale(1.0);
transition: 0.5s;
transform:scale(1.2);}
.conten {overflow:hidden;}


</style>

<!-- JS -->
<?=$this->load->view('Include/Js','',TRUE);?>
<!-- JS -->

</body>
</html>