
<html lang="en">
<head>
<title>Pagos</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Integra-Educa">
<meta name="keywords" content="Integra-Educa, Integra">
<meta name="author" content="Mike">

<?=$this->load->view('Include/base_css','',TRUE);?>
<script type="text/javascript" src="<?=base_url('public/assets/js/html2/html2pdf.bundle.min.js')?>"></script>

<link rel="stylesheet" type="text/css" href="<?=base_url('public/assets/css/table.css')?>">

</head>
<body>

<!-- Loader starts-->
<div class="loader-wrapper">
<div class="theme-loader">    
<div class="loader-p"></div>
</div>
</div>
<!-- Loader ends-->


<!-- page-wrapper Start-->
<div class="page-wrapper" id="pageWrapper">

<!-- Page Header Start-->
<?=$this->load->view('Alumnos/Header','',TRUE);?>
<!-- Page Header Start-->


<!-- Page Body Start-->
<div class="page-body-wrapper horizontal-menu">
 
<!-- Page Sidebar Inicio-->
<?=$this->load->view('Alumnos/Side_bar','',TRUE);?>
<!-- Page Sidebar Fin-->

<!-- TITULOS PAGINA-->
<div class="page-body">
<div class="container-fluid">
<div class="page-header">
<div class="row ">
<div class="col-sm-6 ">
<h3>Pagos</h3>
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="<?= base_url('Alumnos')?>">Inicio</a></li>
<li class="breadcrumb-item">Home</li>
</ol>
</div>
</div>
</div>
</div>
<!-- TITULOS PAGINA-->

<div class="row justify-content-center mr-2 ml-2">
<div class="col-sm-12 col-xl-12 col-lg-12">
</div>
<span class="badge badge-warning text-dark" style="font-size: 22px; border-radius: 25px;">-Sistema Integral de Información-</span>
</div>

<br>



<!-- Container-fluid starts-->
<div class="container-fluid">

<!-- BODY-->



<!-- MODAL-->
<div class="modal fade bd-example-modal-lg" tabindex="-1" aria-labelledby="myLargeModalLabel"  aria-modal="true" role="dialog" id="modal">
<div class="modal-dialog modal-lg">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title" id="myLargeModalLabel">PAGOS</h4>
<button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close" data-bs-original-title="" title=""></button>
</div>
<div class="modal-body">
<!-- BODY MODAL-->  


<div class="row justify-content-center mt-0">
    
<div class="col-sm-12 col-xl-12">
    


<div class="table-responsive invoice-table" id="table" style="border-radius: 25px;">
<table class="table table-bordered table-striped table-success text-center">

<thead class="table-dark">
<tr class="mt-1">
<th scope="col" style="font-size: 15px; width: 14%" class="text-left align-self-center text-white">CONCEPTO</th>
<th scope="col" style="font-size: 15px; width: 14%" class="text-white">COSTO</th>
<th scope="col" style="font-size: 15px; width: 14%" class="text-white">FECHA</th>
<th scope="col" style="font-size: 14px; width: 14%" class="text-center text-white">ESTATUS</th>
<th scope="col" style="font-size: 14px; width: 14%" class="text-center text-white">PERIODO</th>

</tr>
</thead>

<tbody class="mt-2">


<!-- TR1-->  

<tr>

<th scope="row" style="padding: 0.8rem; font-size: 12px" class="rounded-pill text-center text-dark justify-content-center align-items-center"><strong>MES 1</strong></th>


<td  style="font-size: 15px; font-weight: 600; padding: 0.8rem; 
 " class="rounded-pill text-center text-dark justify-content-center align-items-center">
      <strong id="costo_1"></strong>
 </td>

<td  style="font-size: 15px; font-weight: 600; padding: 0.8rem; 
 " class="rounded-pill text-center text-dark justify-content-center align-items-center">
     <strong id="fecha_1"></strong>
 </td>

<td  style="font-size: 15px; font-weight: 600; padding: 0.8rem; 
 " class="rounded-pill text-center text-dark justify-content-center align-items-center">
     <strong>

<i data-feather="check" style="font-size: 28px; color: black; height: 18px;" id="status_1" class="d-none"></i>
<i data-feather="x" style="font-size: 28px; color: black; height: 18px;" id="status_1_1" ></i>


    </strong>
 </td>

<td  style="font-size: 15px; font-weight: 600; padding: 0.8rem; 
 " class="rounded-pill text-center text-dark justify-content-center align-items-center">
     <strong id="periodo_1" ></strong>
 </td>


</tr>

<!-- TR1-->  

<!-- TR1-->  

<tr>

<th scope="row" style="padding: 0.8rem; font-size: 12px" class="rounded-pill text-center text-dark justify-content-center align-items-center"><strong>MES 2</strong></th>


<td  style="font-size: 15px; font-weight: 600; padding: 0.8rem; 
 " class="rounded-pill text-center text-dark justify-content-center align-items-center">
      <strong id="costo_2"></strong>
 </td>

<td  style="font-size: 15px; font-weight: 600; padding: 0.8rem; 
 " class="rounded-pill text-center text-dark justify-content-center align-items-center">
     <strong id="fecha_2"></strong>
 </td>

<td  style="font-size: 15px; font-weight: 600; padding: 0.8rem; 
 " class="rounded-pill text-center text-dark justify-content-center align-items-center">
     <strong>
       <i data-feather="check" style="font-size: 28px; color: black; height: 18px;" id="status_2" class="d-none"></i>
<i data-feather="x" style="font-size: 28px; color: black; height: 18px;" id="status_2_2" ></i>

    </strong>
 </td>

<td  style="font-size: 15px; font-weight: 600; padding: 0.8rem; 
 " class="rounded-pill text-center text-dark justify-content-center align-items-center">
     <strong id="periodo_2" ></strong>
 </td>



</tr>

<!-- TR1-->  

<!-- TR1-->  

<tr>

<th scope="row" style="padding: 0.8rem; font-size: 12px" class="rounded-pill text-center text-dark justify-content-center align-items-center"><strong>MES 3</strong></th>


<td  style="font-size: 15px; font-weight: 600; padding: 0.8rem; 
 " class="rounded-pill text-center text-dark justify-content-center align-items-center">
      <strong id="costo_3"></strong>
 </td>

<td  style="font-size: 15px; font-weight: 600; padding: 0.8rem; 
 " class="rounded-pill text-center text-dark justify-content-center align-items-center">
     <strong id="fecha_3"></strong>
 </td>

<td  style="font-size: 15px; font-weight: 600; padding: 0.8rem; 
 " class="rounded-pill text-center text-dark justify-content-center align-items-center">
     <strong>
<i data-feather="check" style="font-size: 28px; color: black; height: 18px;" id="status_3" class="d-none"></i>
<i data-feather="x" style="font-size: 28px; color: black; height: 18px;" id="status_3_3" ></i>

    </strong>
 </td>

<td  style="font-size: 15px; font-weight: 600; padding: 0.8rem; 
 " class="rounded-pill text-center text-dark justify-content-center align-items-center">
     <strong id="periodo_3" ></strong>
 </td>


</tr>

<!-- TR1-->  

<!-- TR1-->  

<tr>

<th scope="row" style="padding: 0.8rem; font-size: 12px" class="rounded-pill text-center text-dark justify-content-center align-items-center"><strong>MES 4</strong></th>


<td  style="font-size: 15px; font-weight: 600; padding: 0.8rem; 
 " class="rounded-pill text-center text-dark justify-content-center align-items-center">
      <strong id="costo_4"></strong>
 </td>

<td  style="font-size: 15px; font-weight: 600; padding: 0.8rem; 
 " class="rounded-pill text-center text-dark justify-content-center align-items-center">
     <strong id="fecha_4"></strong>
 </td>

<td  style="font-size: 15px; font-weight: 600; padding: 0.8rem; 
 " class="rounded-pill text-center text-dark justify-content-center align-items-center">
     <strong>
<i data-feather="check" style="font-size: 28px; color: black; height: 18px;" id="status_4" class="d-none"></i>
<i data-feather="x" style="font-size: 28px; color: black; height: 18px;" id="status_4_4" ></i>

    </strong>
 </td>

<td  style="font-size: 15px; font-weight: 600; padding: 0.8rem; 
 " class="rounded-pill text-center text-dark justify-content-center align-items-center">
     <strong id="periodo_4" ></strong>
 </td>


</tr>

<!-- TR1-->  

<!-- TR1-->  

<tr>

<th scope="row" style="padding: 0.8rem; font-size: 12px" class="rounded-pill text-center text-dark justify-content-center align-items-center"><strong>REINSCRIPCIÓN</strong></th>


<td  style="font-size: 15px; font-weight: 600; padding: 0.8rem; 
 " class="rounded-pill text-center text-dark justify-content-center align-items-center">
 <strong id="costo_5" ></strong>
 </td>

<td  style="font-size: 15px; font-weight: 600; padding: 0.8rem; 
 " class="rounded-pill text-center text-dark justify-content-center align-items-center">
     <strong id="fecha_5"></strong>
 </td>

<td  style="font-size: 15px; font-weight: 600; padding: 0.8rem; 
 " class="rounded-pill text-center text-dark justify-content-center align-items-center">
     <strong>
<i data-feather="check" style="font-size: 28px; color: black; height: 18px;" id="status_5" class="d-none"></i>
<i data-feather="x" style="font-size: 28px; color: black; height: 18px;" id="status_5_5" ></i>
    </strong>
 </td>

<td  style="font-size: 15px; font-weight: 600; padding: 0.8rem; 
 " class="rounded-pill text-center text-dark justify-content-center align-items-center">
     <strong id="periodo_5" ></strong>
 </td>



</tr>

<!-- TR1-->  


</tbody>
</table>
</div>


</div>

</div>
  

<!-- BODY MODAL--> 
</div>
</div>
</div>
</div>
<!-- MODAL-->






<div class="row justify-content-center mt-4">
    
<div class="col-sm-12 col-xl-12">
    


<div class="table-responsive " id="table" style="border-radius: 25px;">
<table class="table table-bordered table-striped table-success text-center">

<thead class="table-dark">
<tr class="mt-1">
<th scope="col" style="font-size: 15px; width: 14%" class="text-left align-self-center text-white">Mes 1</th>
<th scope="col" style="font-size: 15px; width: 14%" class="text-white">Mes 2</th>
<th scope="col" style="font-size: 15px; width: 14%" class="text-white">Mes 3</th>
<th scope="col" style="font-size: 14px; width: 14%" class="text-center text-white">Mes 4</th>
<th scope="col" style="font-size: 14px; width: 14%" class="text-center text-white">Reinscripción</th>

<th scope="col" style="font-size: 14px; width: 14%" class="text-center text-white">Periodo</th>
<th scope="col" style="font-size: 14px; width: 14%" class="text-center text-white">Ver más..</th>


</tr>
</thead>

<tbody class="mt-2">
<!-- CUERPO TABLA-->
<?php 

foreach ($pagos->result() as $pago){?>

<tr>

<td style="padding: 0.8rem; font-size: 15px; font-weight: bold;
" class="text-left">
<strong>$<?= $pago->mes_1 ?></strong>
</td>

<td style="padding: 0.8rem; font-size: 15px; font-weight: bold;
" class="text-left">
<strong>$<?= $pago->mes_2 ?></strong>
</td>

<td style="padding: 0.8rem; font-size: 15px" class="text-center">
<strong>$<?= $pago->mes_3 ?></strong>
</td>

<td  style="font-size: 15px; font-weight: 600; padding: 0.8rem; 
 " class="text-center text-dark justify-content-center align-items-center">
 <strong>$<?= $pago->mes_4 ?></strong>
</td>

<td  style="font-size: 15px; font-weight: 600; padding: 0.8rem; 
" class="text-center text-dark  justify-content-center align-items-center">
<strong>$<?= $pago->reinscripcion ?></strong>
</td>

<td style="font-size: 15px; font-weight: 600; padding: 0.8rem; 
" class="text-center text-dark  justify-content-center align-items-center">
<strong><?= $pago->periodo ?></strong>
</td>


<td class="text-center" style="font-weight: bold;">
<button onclick="Pay_2('<?= $pago->Matricula_Alumno ?>')" class="btn btn-warning"><i data-feather="eye" style="font-size: 28px; color: black; height: 18px;"></i></button>
</td>


</tr>

<?php
}
?>
<!-- CUERPO TABLA-->
</tbody>

</table>
</div>


</div>

</div>
  

</div>
<!-- TABLA-->





<!-- FORMULARIO-->



</div>


<!-- BODY-->




<script type="text/javascript">

function Pay_2(id) {

$.post("GetPagos", {
id: id
},
function (data) {
var myArray = JSON.parse(data);

$('#costo_1').text(myArray.datos.mes_1);
$('#costo_2').text(myArray.datos.mes_2);
$('#costo_3').text(myArray.datos.mes_3);
$('#costo_4').text(myArray.datos.mes_4);
$('#costo_5').text(myArray.datos.reinscripcion);

$('#fecha_1').text(myArray.datos.fecha_1);
$('#fecha_2').text(myArray.datos.fecha_2);
$('#fecha_3').text(myArray.datos.fecha_3);
$('#fecha_4').text(myArray.datos.fecha_4);
$('#fecha_5').text(myArray.datos.fecha_5);

if(myArray.datos.status_1==1){
$("#status_1").removeClass("d-none");
$("#status_1_1").addClass("d-none");}

if(myArray.datos.status_2==1){
$("#status_2").removeClass("d-none");
$("#status_2_2").addClass("d-none");}

if(myArray.datos.status_3==1){
$("#status_3").removeClass("d-none");
$("#status_3_3").addClass("d-none");}

if(myArray.datos.status_4==1){
$("#status_4").removeClass("d-none");
$("#status_4_4").addClass("d-none");}

if(myArray.datos.status_5==1){
$("#status_5").removeClass("d-none");
$("#status_5_5").addClass("d-none");}

$('#periodo_1').text(myArray.datos.periodo);
$('#periodo_2').text(myArray.datos.periodo);
$('#periodo_3').text(myArray.datos.periodo);
$('#periodo_4').text(myArray.datos.periodo);
$('#periodo_5').text(myArray.datos.periodo);

$('#observacion_5').text(myArray.datos.observacion);

$('#modal').modal('show');


}
);



}

</script>


</div>


</div>
</div>

<!-- Container-fluid Ends-->
</div>

<!-- footer start-->
<footer class="footer" style="margin-left: auto; margin-right: auto;">
<div class="row">
<div class="col-md-6 footer-copyright">
<p class="mb-0">Copyright 2022-23 © IMEI / CEP</p>
</div>
<div class="col-md-6">
<p class="pull-right mb-0">Made by "Hack the box" with <i class="fa fa-heart font-secondary"></i></p>
</div>
</div>

</footer>
</div>
</div>
<?=$this->load->view('Include/base_js','',TRUE);?>

</body>
</html>