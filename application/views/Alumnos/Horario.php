
<html lang="en">
<head>
<title>Horario</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Integra-Educa">
<meta name="keywords" content="Integra-Educa, Integra">
<meta name="author" content="Mike">

<?=$this->load->view('Include/base_css','',TRUE);?>
<script type="text/javascript" src="<?=base_url('public/assets/js/html2/html2pdf.bundle.min.js')?>"></script>

<link rel="stylesheet" type="text/css" href="<?=base_url('public/assets/css/table.css')?>">

</head>
<body>

<!-- Loader starts-->
<div class="loader-wrapper">
<div class="theme-loader">    
<div class="loader-p"></div>
</div>
</div>
<!-- Loader ends-->


<!-- page-wrapper Start-->
<div class="page-wrapper" id="pageWrapper">

<!-- Page Header Start-->
<?=$this->load->view('Alumnos/Header','',TRUE);?>
<!-- Page Header Start-->


<!-- Page Body Start-->
<div class="page-body-wrapper horizontal-menu">
 
<!-- Page Sidebar Inicio-->
<?=$this->load->view('Alumnos/Side_bar','',TRUE);?>
<!-- Page Sidebar Fin-->

<!-- TITULOS PAGINA-->
<div class="page-body">
<div class="container-fluid">
<div class="page-header">
<div class="row ">
<div class="col-sm-6 ">
<h3>Horario</h3>
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="<?= base_url('Alumnos')?>">Inicio</a></li>
<li class="breadcrumb-item">Home</li>
</ol>
</div>
</div>
</div>
</div>
<!-- TITULOS PAGINA-->

<div class="row justify-content-center mr-2 ml-2">
<div class="col-sm-12 col-xl-12 col-lg-12">
</div>
<span class="badge badge-warning text-dark" style="font-size: 22px; border-radius: 25px;">-Sistema Integral de Información-</span>
</div>

<br>



<!-- Container-fluid starts-->
<div class="container-fluid">

<!-- BODY-->


<div class="card-block row">
  


<hr>

<?php 

$dia= $row->result();


if($dia==null)
{
echo '<span class="badge badge-info text-dark text-center mr-2 ml-2" style="font-size: 22px; border-radius: 25px; background-color: #ffb1b1;">No hay Horario Asignado</span>';
}
else
{

switch ($dia[0]->turno) {

//CASO 1
case 'SABATINO':
echo '<span class="badge badge-info text-dark text-center mr-2 ml-2" style="font-size: 22px; border-radius: 25px; background-color: #ffb1b1;">Horario Sabatino</span>
<br>
<div class="col-sm-12 col-lg-12 col-xl-12 mt-3 mb-3">
<!-- TABLA-->
<div class="table-responsive" style="border-radius: 25px;">

<table class="table table-striped table-bordered table-primary">

<thead class="bg-primary">

<tr>
<th class="text-left text-dark" style="width: 27%; font-size: 16px; font-weight: 600;">Hora</th>
<th class="text-left text-dark" style="width: 27%; font-size: 16px; font-weight: 600;">Materia</th>
<th class="text-left text-dark" style="width: 27%; font-size: 16px; font-weight: 600;">Docente</th>
</tr>

</thead>';

foreach ($row->result() as $row)
{
echo
'<tr>
<td style="padding: 0.6rem; font-size: 15px; font-weight: 600;" class="text-dark">'.$row->hora_inicio .' - '.$row->hora_fin.'</td>
<td style="padding: 0.6rem; font-size: 15px; font-weight: 600;" class="text-dark">'.$row->nombre.'</td>
<td style="padding: 0.6rem; font-size: 15px; font-weight: 600;" class="text-dark">'.$row->profesor.'</td>
</tr>';

}

echo
'</table>';
break;
//CASO 1

//CASO 2
case 'MATUTINO':
echo 
'<span class="badge badge-info text-dark text-center mr-2 ml-2" style="font-size: 22px; border-radius: 25px; background-color: #ffb1b1;">Horario Matutino</span>

<br>
<div class="col-sm-12 col-lg-12 col-xl-12 mt-3 mb-3">
<!-- TABLA-->
<div class="table-responsive" style="border-radius: 25px;">

<table class="table table-striped table-bordered  table-primary">

<thead class="bg-primary">

<tr>
<th class="text-left text-dark" style="width: 17%; font-size: 16px; font-weight: 600;">Dia</th>
<th class="text-left text-dark" style="width: 27%; font-size: 16px; font-weight: 600;">Hora</th>
<th class="text-left text-dark" style="width: 27%; font-size: 16px; font-weight: 600;">Materia</th>
<th class="text-left text-dark" style="width: 27%; font-size: 16px; font-weight: 600;">Docente</th>
</tr>

</thead>';

foreach ($row->result() as $row)
{

if($row->dia=='LUNES')
{
echo '
<tr class="table-warning">
<td style="padding: 0.6rem; font-size: 15px; font-weight: 600;" class="text-dark">'.$row->dia.'</td>
<td style="padding: 0.6rem; font-size: 15px; font-weight: 600;" class="text-dark">'.$row->hora_inicio .' - '.$row->hora_fin.'</td>
<td style="padding: 0.6rem; font-size: 15px; font-weight: 600;" class="text-dark">'.$row->nombre.'</td>
<td style="padding: 0.6rem; font-size: 15px; font-weight: 600;" class="text-dark">'.$row->profesor.'</td>
</tr>
<tr>
<td style="padding: 0.6rem"></td>
<td style="padding: 0.6rem"></td>
<td style="padding: 0.6rem"></td>
<td style="padding: 0.6rem"></td>
</tr>
';
}


if($row->dia=='MIERCOLES')
{
echo '
<tr class="table-success">
<td style="padding: 0.6rem; font-size: 15px; font-weight: 600;" class="text-dark">'.$row->dia.'</td>
<td style="padding: 0.6rem; font-size: 15px; font-weight: 600;" class="text-dark">'.$row->hora_inicio .' - '.$row->hora_fin.'</td>
<td style="padding: 0.6rem; font-size: 15px; font-weight: 600;" class="text-dark">'.$row->nombre.'</td>
<td style="padding: 0.6rem; font-size: 15px; font-weight: 600;" class="text-dark">'.$row->profesor.'</td>
</tr>
<tr>
<td style="padding: 0.6rem"></td>
<td style="padding: 0.6rem"></td>
<td style="padding: 0.6rem"></td>
<td style="padding: 0.6rem"></td>
</tr>
';
}


if($row->dia=='VIERNES')
{
echo '
<tr class="table-danger">
<td style="padding: 0.6rem; font-size: 15px; font-weight: 600;" class="text-dark">'.$row->dia.'</td>
<td style="padding: 0.6rem; font-size: 15px; font-weight: 600;" class="text-dark">'.$row->hora_inicio .' - '.$row->hora_fin.'</td>
<td style="padding: 0.6rem; font-size: 15px; font-weight: 600;" class="text-dark">'.$row->nombre.'</td>
<td style="padding: 0.6rem; font-size: 15px; font-weight: 600;" class="text-dark">'.$row->profesor.'</td>
</tr>
<tr>
<td style="padding: 0.6rem"></td>
<td style="padding: 0.6rem"></td>
<td style="padding: 0.6rem"></td>
<td style="padding: 0.6rem"></td>
</tr>
';
}



}

echo '</table>';


break;
//CASO 2

//CASO 3
case 'VESPERTINO':
echo 
'<span class="badge badge-info text-dark text-center mr-2 ml-2" style="font-size: 22px; border-radius: 25px; background-color: #ffb1b1;">Horario Vespertino</span>
<hr>


<div class="table-responsive" style="border-radius: 25px;">

<table class="table table-striped table-bordered  table-primary">

<thead class="bg-primary">
        
<tr>
<th class="text-left text-dark" style="width: 7%; font-size: 16px; font-weight: 600;">Dia</th>
<th class="text-left text-dark" style="width: 20%; font-size: 16px; font-weight: 600;">Hora</th>
<th class="text-left text-dark" style="font-size: 16px; font-weight: 600;">Materia</th>
<th class="text-left text-dark" style="font-size: 16px; font-weight: 600;">Docente</th>
</tr>

</thead>';

foreach ($row->result() as $row)
{

if($row->dia=='MARTES')
{
echo '
<tr class="table-warning">
<td style="padding: 0.6rem; font-size: 15px; font-weight: 600;" class="text-dark">'.$row->dia.'</td>
<td style="padding: 0.6rem; font-size: 15px; font-weight: 600;" class="text-dark">'.$row->hora_inicio .' - '.$row->hora_fin.'</td>
<td style="padding: 0.6rem; font-size: 15px; font-weight: 600;" class="text-dark">'.$row->nombre.'</td>
<td style="padding: 0.6rem; font-size: 15px; font-weight: 600;" class="text-dark">'.$row->profesor.'</td>
</tr>
<tr>
<td style="padding: 0.6rem"></td>
<td style="padding: 0.6rem"></td>
<td style="padding: 0.6rem"></td>
<td style="padding: 0.6rem"></td>
</tr>
';
}


if($row->dia=='JUEVES')
{
echo '
<tr class="table-danger">
<td style="padding: 0.6rem; font-size: 15px; font-weight: 600;" class="text-dark">'.$row->dia.'</td>
<td style="padding: 0.6rem; font-size: 15px; font-weight: 600;" class="text-dark">'.$row->hora_inicio .' - '.$row->hora_fin.'</td>
<td style="padding: 0.6rem; font-size: 15px; font-weight: 600;" class="text-dark">'.$row->nombre.'</td>
<td style="padding: 0.6rem; font-size: 15px; font-weight: 600;" class="text-dark">'.$row->profesor.'</td>
</tr>
<tr>
<td style="padding: 0.6rem"></td>
<td style="padding: 0.6rem"></td>
<td style="padding: 0.6rem"></td>
<td style="padding: 0.6rem"></td>
</tr>
';
}

}

echo '</table>';

break;
//CASO 3

default:
break;
}
}


?>



</div>
<!-- TABLA-->


</div>


<!-- BODY-->





</div>

</div>


</div>
</div>

<!-- Container-fluid Ends-->
</div>

<!-- footer start-->
<footer class="footer" style="margin-left: auto; margin-right: auto;">
<div class="row">
<div class="col-md-6 footer-copyright">
<p class="mb-0">Copyright 2022-23 © IMEI / CEP</p>
</div>
<div class="col-md-6">
<p class="pull-right mb-0">Made by "Hack the box" with <i class="fa fa-heart font-secondary"></i></p>
</div>
</div>

</footer>
</div>
</div>
<?=$this->load->view('Include/base_js','',TRUE);?>

</body>
</html>