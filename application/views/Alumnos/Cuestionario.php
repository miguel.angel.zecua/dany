<form id="formularioaenviar" action="<?= base_url('Alumnos/Env_evaluacion') ?>" method="post">
<!-- 1-->
<div class="card" style=" border-radius:25px;">
                  
<div class="card-body">
<div class="row">                  
<div class="col-sm-12">
<h6>1.  EXPLICA DE MANERA CLARA LOS CONTENIDOS DE LA ASIGNATURA.</h6>
</div>
<div class="col">
<div class="form-group m-checkbox-inline mb-0 custom-radio-ml">
<div class="radio radio-primary">
<input id="rl1" type="radio" name="radio1" value="0" required>
<label class="mb-0" for="rl1">0</label>
</div>
<div class="radio radio-primary">
<input id="rl2" type="radio" name="radio1" value="20" required>>
<label class="mb-0" for="rl2">20</label>
</div>
<div class="radio radio-primary">
<input id="rl3" type="radio" name="radio1" value="40" required>>
<label class="mb-0" for="rl3">40</label>
</div>
<div class="radio radio-primary">
<input id="rl4" type="radio" name="radio1" value="60" required>>
<label class="mb-0" for="rl4">60</label>
</div>
<div class="radio radio-primary">
<input id="rl5" type="radio" name="radio1" value="80" required>>
<label class="mb-0" for="rl5">80</label>
</div>
<div class="radio radio-primary">
<input id="rl6" type="radio" name="radio1" value="100" required>>
<label class="mb-0" for="rl6">100</label>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- 1-->

<!-- 2-->
<div class="card" style=" border-radius:25px;">
                  
<div class="card-body">
<div class="row">                  
<div class="col-sm-12">
<h6>2.  RESUELVE LAS DUDAS RELACIONADAS CON LOS CONTENIDOS DE LA ASIGNATURA.</h6>
</div>
<div class="col">
<div class="form-group m-checkbox-inline mb-0 custom-radio-ml">
<div class="radio radio-primary">
<input id="rl7" type="radio" name="radio2" value="0" required>
<label class="mb-0" for="rl7">0</label>
</div>
<div class="radio radio-primary">
<input id="rl8" type="radio" name="radio2" value="20" required>
<label class="mb-0" for="rl8">20</label>
</div>
<div class="radio radio-primary">
<input id="rl9" type="radio" name="radio2" value="40" required>
<label class="mb-0" for="rl9">40</label>
</div>
<div class="radio radio-primary">
<input id="rl10" type="radio" name="radio2" value="60" required>
<label class="mb-0" for="rl10">60</label>
</div>
<div class="radio radio-primary">
<input id="rl11" type="radio" name="radio2" value="80" required>
<label class="mb-0" for="rl11">80</label>
</div>
<div class="radio radio-primary">
<input id="rl12" type="radio" name="radio2" value="100" required>
<label class="mb-0" for="rl12">100</label>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- 2-->

<!-- 3-->
<div class="card" style=" border-radius:25px;">
                  
<div class="card-body">
<div class="row">                  
<div class="col-sm-12">
<h6>3.  PROPONE EJEMPLOS O EJERCICIOS QUE VINCULAN LA ASIGNATURA CON LA PRÁCTICA PROFESIONAL.</h6>
</div>
<div class="col">
<div class="form-group m-checkbox-inline mb-0 custom-radio-ml">
<div class="radio radio-primary">
<input id="rl13" type="radio" name="radio3" value="0" required>
<label class="mb-0" for="rl13">0</label>
</div>
<div class="radio radio-primary">
<input id="rl14" type="radio" name="radio3" value="20" required>
<label class="mb-0" for="rl14">20</label>
</div>
<div class="radio radio-primary">
<input id="rl15" type="radio" name="radio3" value="40" required>
<label class="mb-0" for="rl15">40</label>
</div>
<div class="radio radio-primary">
<input id="rl16" type="radio" name="radio3" value="60" required>
<label class="mb-0" for="rl16">60</label>
</div>
<div class="radio radio-primary">
<input id="rl17" type="radio" name="radio3" value="80" required>
<label class="mb-0" for="rl17">80</label>
</div>
<div class="radio radio-primary">
<input id="rl18" type="radio" name="radio3" value="100" required>
<label class="mb-0" for="rl18">100</label>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- 3-->

<!-- 4-->
<div class="card" style=" border-radius:25px;">
                  
<div class="card-body">
<div class="row">                  
<div class="col-sm-12">
<h6>4.  MUESTRA COMPROMISO Y ENTUSIASMO EN SUS ACTIVIDADES DOCENTES.</h6>
</div>
<div class="col">
<div class="form-group m-checkbox-inline mb-0 custom-radio-ml">
<div class="radio radio-primary">
<input id="rl19" type="radio" name="radio4" value="0" required>
<label class="mb-0" for="rl19">0</label>
</div>
<div class="radio radio-primary">
<input id="rl20" type="radio" name="radio4" value="20" required>
<label class="mb-0" for="rl20">20</label>
</div>
<div class="radio radio-primary">
<input id="rl21" type="radio" name="radio4" value="40" required>
<label class="mb-0" for="rl21">40</label>
</div>
<div class="radio radio-primary">
<input id="rl22" type="radio" name="radio4" value="60" required>
<label class="mb-0" for="rl22">60</label>
</div>
<div class="radio radio-primary">
<input id="rl23" type="radio" name="radio4" value="80" required>
<label class="mb-0" for="rl23">80</label>
</div>
<div class="radio radio-primary">
<input id="rl24" type="radio" name="radio4" value="100" required>
<label class="mb-0" for="rl24">100</label>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- 4-->

<!-- 5-->
<div class="card" style=" border-radius:25px;">
                  
<div class="card-body">
<div class="row">                  
<div class="col-sm-12">
<h6>5.  TOMA EN CUENTA LAS NECESIDADES, INTERESES Y EXPECTATIVAS DEL GRUPO.</h6>
</div>
<div class="col">
<div class="form-group m-checkbox-inline mb-0 custom-radio-ml">
<div class="radio radio-primary">
<input id="rl25" type="radio" name="radio5" value="0" required>
<label class="mb-0" for="rl25">0</label>
</div>
<div class="radio radio-primary">
<input id="rl26" type="radio" name="radio5" value="20" required>
<label class="mb-0" for="rl26">20</label>
</div>
<div class="radio radio-primary">
<input id="rl27" type="radio" name="radio5" value="40" required>
<label class="mb-0" for="rl27">40</label>
</div>
<div class="radio radio-primary">
<input id="rl28" type="radio" name="radio5" value="60" required>
<label class="mb-0" for="rl28">60</label>
</div>
<div class="radio radio-primary">
<input id="rl29" type="radio" name="radio5" value="80" required>
<label class="mb-0" for="rl29">80</label>
</div>
<div class="radio radio-primary">
<input id="rl30" type="radio" name="radio5" value="100" required>
<label class="mb-0" for="rl30">100</label>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- 5-->

<!-- 6-->
<div class="card" style=" border-radius:25px;">
                  
<div class="card-body">
<div class="row">                  
<div class="col-sm-12">
<h6>6.  EXISTE LA IMPRESIÓN DE QUE SE TOMAN REPRESALIAS CON ALGUNOS ESTUDIANTES.  </h6>
</div>
<div class="col">
<div class="form-group m-checkbox-inline mb-0 custom-radio-ml">
<div class="radio radio-primary">
<input id="rl31" type="radio" name="radio6" value="0" required>
<label class="mb-0" for="rl31">0</label>
</div>
<div class="radio radio-primary">
<input id="rl32" type="radio" name="radio6" value="20" required>
<label class="mb-0" for="rl32">20</label>
</div>
<div class="radio radio-primary">
<input id="rl33" type="radio" name="radio6" value="40" required>
<label class="mb-0" for="rl33">40</label>
</div>
<div class="radio radio-primary">
<input id="rl34" type="radio" name="radio6" value="60" required>
<label class="mb-0" for="rl34">60</label>
</div>
<div class="radio radio-primary">
<input id="rl35" type="radio" name="radio6" value="80" required>
<label class="mb-0" for="rl35">80</label>
</div>
<div class="radio radio-primary">
<input id="rl36" type="radio" name="radio6" value="100" required>
<label class="mb-0" for="rl36">100</label>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- 6-->

<!-- 7-->
<div class="card" style=" border-radius:25px;">
                  
<div class="card-body">
<div class="row">                  
<div class="col-sm-12">
<h6>7.  HACE INTERESANTE LA ASIGNATURA.</h6>
</div>
<div class="col">
<div class="form-group m-checkbox-inline mb-0 custom-radio-ml">
<div class="radio radio-primary">
<input id="rl37" type="radio" name="radio7" value="0" required>
<label class="mb-0" for="rl37">0</label>
</div>
<div class="radio radio-primary">
<input id="rl38" type="radio" name="radio7" value="20" required>
<label class="mb-0" for="rl38">20</label>
</div>
<div class="radio radio-primary">
<input id="rl39" type="radio" name="radio7" value="40" required>
<label class="mb-0" for="rl39">40</label>
</div>
<div class="radio radio-primary">
<input id="rl40" type="radio" name="radio7" value="60" required>
<label class="mb-0" for="rl40">60</label>
</div>
<div class="radio radio-primary">
<input id="rl41" type="radio" name="radio7" value="80" required>
<label class="mb-0" for="rl41">80</label>
</div>
<div class="radio radio-primary">
<input id="rl42" type="radio" name="radio7" value="100" required>
<label class="mb-0" for="rl42">100</label>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- 7-->

<!-- 8-->
<div class="card" style=" border-radius:25px;">
                  
<div class="card-body">
<div class="row">                  
<div class="col-sm-12">
<h6>8.  PROPORCIONA INFORMACIÓN PARA REALIZAR ADECUADAMENTE LAS ACTIVIDADES DE EVALUACIÓN.</h6>
</div>
<div class="col">
<div class="form-group m-checkbox-inline mb-0 custom-radio-ml">
<div class="radio radio-primary">
<input id="rl43" type="radio" name="radio8" value="0" required>
<label class="mb-0" for="rl43">0</label>
</div>
<div class="radio radio-primary">
<input id="rl44" type="radio" name="radio8" value="20" required>
<label class="mb-0" for="rl44">20</label>
</div>
<div class="radio radio-primary">
<input id="rl45" type="radio" name="radio8" value="40" required>
<label class="mb-0" for="rl45">40</label>
</div>
<div class="radio radio-primary">
<input id="rl46" type="radio" name="radio8" value="60" required>
<label class="mb-0" for="rl46">60</label>
</div>
<div class="radio radio-primary">
<input id="rl47" type="radio" name="radio8" value="80" required>
<label class="mb-0" for="rl47">80</label>
</div>
<div class="radio radio-primary">
<input id="rl48" type="radio" name="radio8" value="100" required>
<label class="mb-0" for="rl48">100</label>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- 8-->

<!-- 9-->
<div class="card" style=" border-radius:25px;">
                  
<div class="card-body">
<div class="row">                  
<div class="col-sm-12">
<h6>9.  TOMA EN CUENTA LAS ACTIVIDADES REALIZADAS Y LOS PRODUCTOS COMO EVIDENCIAS PARA LA CALIFICACIÓN Y ACREDITACIÓN DE LA ASIGNATURA.</h6>
</div>
<div class="col">
<div class="form-group m-checkbox-inline mb-0 custom-radio-ml">
<div class="radio radio-primary">
<input id="rl49" type="radio" name="radio9" value="0" required>
<label class="mb-0" for="rl49">0</label>
</div>
<div class="radio radio-primary">
<input id="rl50" type="radio" name="radio9" value="20" required>
<label class="mb-0" for="rl50">20</label>
</div>
<div class="radio radio-primary">
<input id="rl51" type="radio" name="radio9" value="40" required>
<label class="mb-0" for="rl51">40</label>
</div>
<div class="radio radio-primary">
<input id="rl52" type="radio" name="radio9" value="60" required>
<label class="mb-0" for="rl52">60</label>
</div>
<div class="radio radio-primary">
<input id="rl53" type="radio" name="radio9" value="80" required>
<label class="mb-0" for="rl53">80</label>
</div>
<div class="radio radio-primary">
<input id="rl54" type="radio" name="radio9" value="100" required>
<label class="mb-0" for="rl54">100</label>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- 9-->

<!-- 10-->
<div class="card" style=" border-radius:25px;">
                  
<div class="card-body">
<div class="row">                  
<div class="col-sm-12">
<h6>10. DA OPORTUNIDAD DE MEJORAR LOS RESULTADOS DE LA EVALUACIÓN DEL APRENDIZAJE.</h6>
</div>
<div class="col">
<div class="form-group m-checkbox-inline mb-0 custom-radio-ml">
<div class="radio radio-primary">
<input id="rl55" type="radio" name="radio10" value="0" required>
<label class="mb-0" for="rl55">0</label>
</div>
<div class="radio radio-primary">
<input id="rl56" type="radio" name="radio10" value="20" required>
<label class="mb-0" for="rl56">20</label>
</div>
<div class="radio radio-primary">
<input id="rl57" type="radio" name="radio10" value="40" required>
<label class="mb-0" for="rl57">40</label>
</div>
<div class="radio radio-primary">
<input id="rl58" type="radio" name="radio10" value="60" required>
<label class="mb-0" for="rl58">60</label>
</div>
<div class="radio radio-primary">
<input id="rl59" type="radio" name="radio10" value="80" required>
<label class="mb-0" for="rl59">80</label>
</div>
<div class="radio radio-primary">
<input id="rl60" type="radio" name="radio10" value="100" required>
<label class="mb-0" for="rl60">100</label>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- 10-->

<!-- 11-->
<div class="card" style=" border-radius:25px;">
                  
<div class="card-body">
<div class="row">                  
<div class="col-sm-12">
<h6>11. ASISTE A CLASES REGULAR Y PUNTUALMENTE.</h6>
</div>
<div class="col">
<div class="form-group m-checkbox-inline mb-0 custom-radio-ml">
<div class="radio radio-primary">
<input id="rl61" type="radio" name="radio11" value="0" required>
<label class="mb-0" for="rl61">0</label>
</div>
<div class="radio radio-primary">
<input id="rl62" type="radio" name="radio11" value="20" required>
<label class="mb-0" for="rl62">20</label>
</div>
<div class="radio radio-primary">
<input id="rl63" type="radio" name="radio11" value="40" required>
<label class="mb-0" for="rl63">40</label>
</div>
<div class="radio radio-primary">
<input id="rl64" type="radio" name="radio11" value="60" required>
<label class="mb-0" for="rl64">60</label>
</div>
<div class="radio radio-primary">
<input id="rl65" type="radio" name="radio11" value="80" required>
<label class="mb-0" for="rl65">80</label>
</div>
<div class="radio radio-primary">
<input id="rl66" type="radio" name="radio11" value="100" required>
<label class="mb-0" for="rl66">100</label>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- 11-->

<!-- 12-->
<div class="card" style=" border-radius:25px;">
                  
<div class="card-body">
<div class="row">                  
<div class="col-sm-12">
<h6>12. ES ACCESIBLE Y ESTÁ DISPUESTO A BRINDARTE AYUDA ACADÉMICA.</h6>
</div>
<div class="col">
<div class="form-group m-checkbox-inline mb-0 custom-radio-ml">
<div class="radio radio-primary">
<input id="rl67" type="radio" name="radio12" value="0" required>
<label class="mb-0" for="rl67">0</label>
</div>
<div class="radio radio-primary">
<input id="rl68" type="radio" name="radio12" value="20" required>
<label class="mb-0" for="rl68">20</label>
</div>
<div class="radio radio-primary">
<input id="rl69" type="radio" name="radio12" value="40" required>
<label class="mb-0" for="rl69">40</label>
</div>
<div class="radio radio-primary">
<input id="rl70" type="radio" name="radio12" value="60" required>
<label class="mb-0" for="rl70">60</label>
</div>
<div class="radio radio-primary">
<input id="rl71" type="radio" name="radio12" value="80" required>
<label class="mb-0" for="rl71">80</label>
</div>
<div class="radio radio-primary">
<input id="rl72" type="radio" name="radio12" value="100" required>
<label class="mb-0" for="rl72">100</label>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- 12-->

<!-- 13-->
<div class="card" style=" border-radius:25px;">
                  
<div class="card-body">
<div class="row">                  
<div class="col-sm-12">
<h6>13. PROPICIA LA PARTICIPACIÓN DE LOS ESTUDIANTES EN LAS CLASES.</h6>
</div>
<div class="col">
<div class="form-group m-checkbox-inline mb-0 custom-radio-ml">
<div class="radio radio-primary">
<input id="rl73" type="radio" name="radio13" value="0" required>
<label class="mb-0" for="rl73">0</label>
</div>
<div class="radio radio-primary">
<input id="rl74" type="radio" name="radio13" value="20" required>
<label class="mb-0" for="rl74">20</label>
</div>
<div class="radio radio-primary">
<input id="rl75" type="radio" name="radio13" value="40" required>
<label class="mb-0" for="rl75">40</label>
</div>
<div class="radio radio-primary">
<input id="rl76" type="radio" name="radio13" value="60" required>
<label class="mb-0" for="rl76">60</label>
</div>
<div class="radio radio-primary">
<input id="rl77" type="radio" name="radio13" value="80" required>
<label class="mb-0" for="rl77">80</label>
</div>
<div class="radio radio-primary">
<input id="rl78" type="radio" name="radio13" value="100" required>
<label class="mb-0" for="rl78">100</label>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- 13-->

<!-- 14-->
<div class="card" style=" border-radius:25px;">
                  
<div class="card-body">
<div class="row">                  
<div class="col-sm-12">
<h6>14. ¿TIENE MANEJO DE GRUPO?</h6>
</div>
<div class="col">
<div class="form-group m-checkbox-inline mb-0 custom-radio-ml">
<div class="radio radio-primary">
<input id="rl79" type="radio" name="radio14" value="0" required>
<label class="mb-0" for="rl79">0</label>
</div>
<div class="radio radio-primary">
<input id="rl80" type="radio" name="radio14" value="20" required>
<label class="mb-0" for="rl80">20</label>
</div>
<div class="radio radio-primary">
<input id="rl81" type="radio" name="radio14" value="40" required>
<label class="mb-0" for="rl81">40</label>
</div>
<div class="radio radio-primary">
<input id="rl82" type="radio" name="radio14" value="60" required>
<label class="mb-0" for="rl82">60</label>
</div>
<div class="radio radio-primary">
<input id="rl83" type="radio" name="radio14" value="80" required>
<label class="mb-0" for="rl83">80</label>
</div>
<div class="radio radio-primary">
<input id="rl84" type="radio" name="radio14" value="100" required>
<label class="mb-0" for="rl84">100</label>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- 14-->

<!-- 15-->
<div class="card" style=" border-radius:25px;">
                  
<div class="card-body">
<div class="row">                  
<div class="col-sm-12">
<h6>15. ¿ES JUSTO EN LAS EVALUACIONES?</h6>
</div>
<div class="col">
<div class="form-group m-checkbox-inline mb-0 custom-radio-ml">
<div class="radio radio-primary">
<input id="rl85" type="radio" name="radio15" value="0" required>
<label class="mb-0" for="rl85">0</label>
</div>
<div class="radio radio-primary">
<input id="rl86" type="radio" name="radio15" value="20" required>
<label class="mb-0" for="rl86">20</label>
</div>
<div class="radio radio-primary">
<input id="rl87" type="radio" name="radio15" value="40" required>
<label class="mb-0" for="rl87">40</label>
</div>
<div class="radio radio-primary">
<input id="rl88" type="radio" name="radio15" value="60" required>
<label class="mb-0" for="rl88">60</label>
</div>
<div class="radio radio-primary">
<input id="rl89" type="radio" name="radio15" value="80" required>
<label class="mb-0" for="rl89">80</label>
</div>
<div class="radio radio-primary">
<input id="rl90" type="radio" name="radio15" value="100" required>
<label class="mb-0" for="rl90">100</label>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- 15-->

<!-- 16-->
<div class="card" style=" border-radius:25px;">
                  
<div class="card-body">
<div class="row">                  
<div class="col-sm-12">
<h6>16. ¿EVALÚA OPORTUNAMENTE LOS TRABAJOS QUE PIDE?</h6>
</div>
<div class="col">
<div class="form-group m-checkbox-inline mb-0 custom-radio-ml">
<div class="radio radio-primary">
<input id="rl91" type="radio" name="radio1" value="0" required>
<label class="mb-0" for="rl91">0</label>
</div>
<div class="radio radio-primary">
<input id="rl92" type="radio" name="radio16" value="20" required>
<label class="mb-0" for="rl92">20</label>
</div>
<div class="radio radio-primary">
<input id="rl93" type="radio" name="radio16" value="40" required>
<label class="mb-0" for="rl93">40</label>
</div>
<div class="radio radio-primary">
<input id="rl94" type="radio" name="radio16" value="60" required>
<label class="mb-0" for="rl94">60</label>
</div>
<div class="radio radio-primary">
<input id="rl95" type="radio" name="radio16" value="80" required>
<label class="mb-0" for="rl95">80</label>
</div>
<div class="radio radio-primary">
<input id="rl96" type="radio" name="radio16" value="100" required>
<label class="mb-0" for="rl96">100</label>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- 16-->

<!-- 17-->
<div class="card" style=" border-radius:25px;">
                  
<div class="card-body">
<div class="row">                  
<div class="col-sm-12">
<h6>17. ¿ES RESPETUOSO CONTIGO?</h6>
</div>
<div class="col">
<div class="form-group m-checkbox-inline mb-0 custom-radio-ml">
<div class="radio radio-primary">
<input id="rl97" type="radio" name="radio17" value="0" required>
<label class="mb-0" for="rl97">0</label>
</div>
<div class="radio radio-primary">
<input id="rl98" type="radio" name="radio17" value="20" required>
<label class="mb-0" for="rl98">20</label>
</div>
<div class="radio radio-primary">
<input id="rl99" type="radio" name="radio17" value="40" required>
<label class="mb-0" for="rl99">40</label>
</div>
<div class="radio radio-primary">
<input id="rl100" type="radio" name="radio17" value="60" required>
<label class="mb-0" for="rl100">60</label>
</div>
<div class="radio radio-primary">
<input id="rl101" type="radio" name="radio17" value="80" required>
<label class="mb-0" for="rl101">80</label>
</div>
<div class="radio radio-primary">
<input id="rl102" type="radio" name="radio17" value="100" required>
<label class="mb-0" for="rl102">100</label>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- 17-->

<!-- 18-->
<div class="card" style=" border-radius:25px;">
                  
<div class="card-body">
<div class="row">                  
<div class="col-sm-12">
<h6>18. ¿A TRATADO DE MANIPULARTE O EXTORSIONARTE?</h6>
</div>
<div class="col">
<div class="form-group m-checkbox-inline mb-0 custom-radio-ml">
<div class="radio radio-primary">
<input id="rl103" type="radio" name="radio18" value="0" required>
<label class="mb-0" for="rl103">0</label>
</div>
<div class="radio radio-primary">
<input id="rl104" type="radio" name="radio18" value="20" required>
<label class="mb-0" for="rl104">20</label>
</div>
<div class="radio radio-primary">
<input id="rl105" type="radio" name="radio18" value="40" required>
<label class="mb-0" for="rl105">40</label>
</div>
<div class="radio radio-primary">
<input id="rl106" type="radio" name="radio18" value="60" required>
<label class="mb-0" for="rl106">60</label>
</div>
<div class="radio radio-primary">
<input id="rl107" type="radio" name="radio18" value="80" required>
<label class="mb-0" for="rl107">80</label>
</div>
<div class="radio radio-primary">
<input id="rl108" type="radio" name="radio18" value="100" required>
<label class="mb-0" for="rl108">100</label>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- 18-->

<!-- 19-->
<div class="card" style=" border-radius:25px;">
                  
<div class="card-body">
<div class="row">                  
<div class="col-sm-12">
<h6>19. ¿A TRATADO DE CONDICIONAR TU CALIFICACIÓN CON ACTOS NO ACADÉMICOS?</h6>
</div>
<div class="col">
<div class="form-group m-checkbox-inline mb-0 custom-radio-ml">
<div class="radio radio-primary">
<input id="rl109" type="radio" name="radio19" value="0" required>
<label class="mb-0" for="rl109">0</label>
</div>
<div class="radio radio-primary">
<input id="rl110" type="radio" name="radio19" value="20" required>
<label class="mb-0" for="rl110">20</label>
</div>
<div class="radio radio-primary">
<input id="rl111" type="radio" name="radio19" value="40" required>
<label class="mb-0" for="rl111">40</label>
</div>
<div class="radio radio-primary">
<input id="rl112" type="radio" name="radio19" value="60" required>
<label class="mb-0" for="rl112">60</label>
</div>
<div class="radio radio-primary">
<input id="rl113" type="radio" name="radio19" value="80" required>
<label class="mb-0" for="rl113">80</label>
</div>
<div class="radio radio-primary">
<input id="rl114" type="radio" name="radio19" value="100" required>
<label class="mb-0" for="rl114">100</label>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- 19-->

<!-- 20-->
<div class="card" style=" border-radius:25px;">
                  
<div class="card-body">
<div class="row">                  
<div class="col-sm-12">
<h6>20. ¿SE HA DIRIGIDO DESPECTIVAMENTE HACIA ALGÚN ALUMNO? </h6>
</div>
<div class="col">
<div class="form-group m-checkbox-inline mb-0 custom-radio-ml">
<div class="radio radio-primary">
<input id="rl115" type="radio" name="radio20" value="0" required>
<label class="mb-0" for="rl115">0</label>
</div>
<div class="radio radio-primary">
<input id="rl116" type="radio" name="radio20" value="20" required>
<label class="mb-0" for="rl116">20</label>
</div>
<div class="radio radio-primary">
<input id="rl117" type="radio" name="radio20" value="40" required>
<label class="mb-0" for="rl117">40</label>
</div>
<div class="radio radio-primary">
<input id="rl118" type="radio" name="radio20" value="60" required>
<label class="mb-0" for="rl118">60</label>
</div>
<div class="radio radio-primary">
<input id="rl119" type="radio" name="radio20" value="80" required>
<label class="mb-0" for="rl119">80</label>
</div>
<div class="radio radio-primary">
<input id="rl120" type="radio" name="radio20" value="100" required>
<label class="mb-0" for="rl120">100</label>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- 20-->

<!-- 21-->
<div class="card" style=" border-radius:25px;">
                  
<div class="card-body">
<div class="row">                  
<div class="col-sm-12">
<h6>21. ¿EL LENGUAJE QUE USA NO ES ADECUADO?</h6>
</div>
<div class="col">
<div class="form-group m-checkbox-inline mb-0 custom-radio-ml">
<div class="radio radio-primary">
<input id="rl121" type="radio" name="radio21" value="0" required>
<label class="mb-0" for="rl121">0</label>
</div>
<div class="radio radio-primary">
<input id="rl122" type="radio" name="radio21" value="20" required>
<label class="mb-0" for="rl122">20</label>
</div>
<div class="radio radio-primary">
<input id="rl123" type="radio" name="radio21" value="40" required>
<label class="mb-0" for="rl123">40</label>
</div>
<div class="radio radio-primary">
<input id="rl124" type="radio" name="radio21" value="60" required>
<label class="mb-0" for="rl124">60</label>
</div>
<div class="radio radio-primary">
<input id="rl125" type="radio" name="radio21" value="80" required>
<label class="mb-0" for="rl125">80</label>
</div>
<div class="radio radio-primary">
<input id="rl126" type="radio" name="radio21" value="100" required>
<label class="mb-0" for="rl126">100</label>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- 21-->

<!-- 22-->
<div class="card" style=" border-radius:25px;">
                  
<div class="card-body">
<div class="row">                  
<div class="col-sm-12">
<h6>22. ¿EN GENERAL, PIENSAS QUE ES UN BUEN DOCENTE?</h6>
</div>
<div class="col">
<div class="form-group m-checkbox-inline mb-0 custom-radio-ml">
<div class="radio radio-primary">
<input id="rl127" type="radio" name="radio22" value="0" required>
<label class="mb-0" for="rl127">0</label>
</div>
<div class="radio radio-primary">
<input id="rl128" type="radio" name="radio22" value="20" required>
<label class="mb-0" for="rl128">20</label>
</div>
<div class="radio radio-primary">
<input id="rl129" type="radio" name="radio22" value="40" required>
<label class="mb-0" for="rl129">40</label>
</div>
<div class="radio radio-primary">
<input id="rl130" type="radio" name="radio22" value="60" required>
<label class="mb-0" for="rl130">60</label>
</div>
<div class="radio radio-primary">
<input id="rl131" type="radio" name="radio22" value="80" required>
<label class="mb-0" for="rl131">80</label>
</div>
<div class="radio radio-primary">
<input id="rl132" type="radio" name="radio22" value="100" required>
<label class="mb-0" for="rl132">100</label>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- 22-->

<!-- 23-->
<div class="card" style=" border-radius:25px;">
                  
<div class="card-body">
<div class="row">                  
<div class="col-sm-12">
<h6>23. ¿ESTAS SATISFECHO (A) POR TU NIVEL DE DESEMPEÑO Y APRENDIZAJE LOGRADO GRACIAS A LA LABOR DEL DOCENTE?</h6>
</div>
<div class="col">
<div class="form-group m-checkbox-inline mb-0 custom-radio-ml">
<div class="radio radio-primary">
<input id="rl133" type="radio" name="radio23" value="0" required>
<label class="mb-0" for="rl133">0</label>
</div>
<div class="radio radio-primary">
<input id="rl134" type="radio" name="radio23" value="20" required>
<label class="mb-0" for="rl134">20</label>
</div>
<div class="radio radio-primary">
<input id="rl135" type="radio" name="radio23" value="40" required>
<label class="mb-0" for="rl135">40</label>
</div>
<div class="radio radio-primary">
<input id="rl136" type="radio" name="radio23" value="60" required>
<label class="mb-0" for="rl136">60</label>
</div>
<div class="radio radio-primary">
<input id="rl137" type="radio" name="radio23" value="80" required>
<label class="mb-0" for="rl137">80</label>
</div>
<div class="radio radio-primary">
<input id="rl138" type="radio" name="radio23" value="100" required>
<label class="mb-0" for="rl138">100</label>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- 23-->

<!-- 24-->
<div class="card" style=" border-radius:25px;">
                  
<div class="card-body">
<div class="row">                  
<div class="col-sm-12">
<h6>24. ¿RECOMENDARÍAS A ESTE DOCENTE?</h6>
</div>
<div class="col">
<div class="form-group m-checkbox-inline mb-0 custom-radio-ml">
<div class="radio radio-primary">
<input id="rl139" type="radio" name="radio24" value="0" required>
<label class="mb-0" for="rl139">0</label>
</div>
<div class="radio radio-primary">
<input id="rl140" type="radio" name="radio24" value="20" required>
<label class="mb-0" for="rl140">20</label>
</div>
<div class="radio radio-primary">
<input id="rl141" type="radio" name="radio24" value="40" required>
<label class="mb-0" for="rl141">40</label>
</div>
<div class="radio radio-primary">
<input id="rl142" type="radio" name="radio24" value="60" required>
<label class="mb-0" for="rl142">60</label>
</div>
<div class="radio radio-primary">
<input id="rl143" type="radio" name="radio24" value="80" required>
<label class="mb-0" for="rl143">80</label>
</div>
<div class="radio radio-primary">
<input id="rl144" type="radio" name="radio24" value="100" required>
<label class="mb-0" for="rl144">100</label>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- 24-->

<!-- 25-->
<div class="card" style=" border-radius:25px;">

<div class="card-body">
<div class="row">                  
<div class="col-sm-12">
<h6>25. OBSERVACIONES</h6>
<textarea class="form-control" id="observaciones" name="observaciones" rows="4"></textarea>
</div>

</div>


<div class="row mt-3 d-none">                  
<div class="col-md-12">
<input class="form-control mt-1" type="text" name="matricula" value="<?php	echo($Alumno);?>" readonly>
<input class="form-control mt-1" type="text" name="materia" value="<?php echo($Materia);?>" readonly>
<input class="form-control mt-1" type="text" name="profesor" value="<?php	echo($Maestro);?>" readonly>
</div>

</div>



</div>
</div>
<!-- 25-->

<div class="row">
<button class="btn btn-primary" type="submit" style="border-radius:25px;">Guardar</button>
</div>

</form>