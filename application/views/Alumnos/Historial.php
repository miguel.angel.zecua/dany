
<html lang="en">
<head>
<title>Historial</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Integra-Educa">
<meta name="keywords" content="Integra-Educa, Integra">
<meta name="author" content="Mike">

<?=$this->load->view('Include/base_css','',TRUE);?>
<script type="text/javascript" src="<?=base_url('public/assets/js/html2/html2pdf.bundle.min.js')?>"></script>

<link rel="stylesheet" type="text/css" href="<?=base_url('public/assets/css/table.css')?>">

</head>
<body>

<!-- Loader starts-->
<div class="loader-wrapper">
<div class="theme-loader">    
<div class="loader-p"></div>
</div>
</div>
<!-- Loader ends-->


<!-- page-wrapper Start-->
<div class="page-wrapper" id="pageWrapper">

<!-- Page Header Start-->
<?=$this->load->view('Alumnos/Header','',TRUE);?>
<!-- Page Header Start-->


<!-- Page Body Start-->
<div class="page-body-wrapper horizontal-menu">
 
<!-- Page Sidebar Inicio-->
<?=$this->load->view('Alumnos/Side_bar','',TRUE);?>
<!-- Page Sidebar Fin-->

<!-- TITULOS PAGINA-->
<div class="page-body">
<div class="container-fluid">
<div class="page-header">
<div class="row ">
<div class="col-sm-6 ">
<h3>Historial</h3>
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="<?= base_url('Alumnos')?>">Inicio</a></li>
<li class="breadcrumb-item">Home</li>
</ol>
</div>
</div>
</div>
</div>
<!-- TITULOS PAGINA-->

<div class="row justify-content-center mr-2 ml-2">
<div class="col-sm-12 col-xl-12 col-lg-12">
</div>
<span class="badge badge-warning text-dark" style="font-size: 22px; border-radius: 25px;">-Sistema Integral de Información-</span>
</div>

<br>



<!-- Container-fluid starts-->
<div class="container-fluid">

<!-- BODY-->




<div class="row justify-content-center">

<!-- FORMULARIO-->

<!-- TARGETA 1-->
<div class="col-sm-6 col-xl-6">

<div class="widget-data text-center" style="padding: 15px 15px; background-color: #c3dff9; border-radius: 25px;">
<h6 class="mb-2 text-gray-dark text-center" style="font-size: 16px;">
<strong>
<?php 
$N1=$row->Nombres;
$F1=" ";
$N2=$row->Apellido_Paterno;
$N3=$row->Apellido_Materno;
$Nombre = $N1.$F1.$N2.$F1.$N3;
echo $Nombre; 
?>   
</strong>
</h6>
<h6 class="mb-2" style="font-size: 15px;"><?php echo $row->Nombre_carrera; ?></h6>
<span class="badge badge-secondary" style="font-size: 13px; border-radius: 15px;"><?php echo $row->Matricula; ?></span>

</div>
</div>
<!-- TARGETA 1-->

<!-- TARGETA 2-->
<div class="col-sm-3 col-xl-3 text-center mt-2">
<h6 class="mb-2 text-gray-dark" style="font-size: 13px;">Promedio General:
<br>
<span class="badge badge-success  mt-1 text-center" style="font-size: 13px; height: 40px; border-radius: 25px;"><h6 class="mt-1"><?= $promedio->promedio ?></h6>  </span></h6>
</div>
<!-- TARGETA 2-->

<!-- TARGETA 3-->
<div class="col-sm-3 col-xl-3 text-center mt-2"> 
<h6 class="mb-2 text-gray-dark" style="font-size: 13px;">Suma de Créditos: 
<br>
<span class="badge badge-success  mt-1 text-center" style="font-size: 13px; height: 40px; border-radius: 25px;"><h6 class="mt-1"><?= $credito->Creditos ?></h6></span></h6>
</div>
<!-- TARGETA 3-->


</div>

<div class="row justify-content-center mt-4 mb-4" id="mensaje_alerta">
    
<div class="col-sm-12 col-xl-12">
<div class="alert alert-danger font-weight-bold text-dark text-center" role="alert" style="border-radius: 25px; font-weight: bold; font-size: 23;">
¡Estamos trabajando en su historial!
<br>
Para más información comuníquese al área administrativa 
</div>

</div>	

</div>

<div class="row justify-content-center mt-4 mb-4" id="tabla_historial">
    
<div class="col-sm-12 col-xl-12">
    

<!-- TABLA-->
<div class="table-responsive" style="border-radius: 18px;">
<table class="table table-hover align-content-center table-borderless table-info" >

<thead class="table-dark">

<tr>
<th style="padding: 0.6rem; font-size: 14px" class="text-white">MATERIA</th>
<th style="padding: 0.6rem; font-size: 14px " class="text-white">CRÉDITOS</th>
<th style="padding: 0.6rem; font-size: 14px" class="text-center text-white">CALIFICACIÓN FINAL </th>
<th style="padding: 0.6rem; font-size: 14px" class="text-center text-white">CUATRIMIESTRE</th>
<th style="padding: 0.6rem; font-size: 14px" class="text-center text-white">PERIODO</th>
</tr>

</thead>
<tbody>

<?php 

foreach ($historial->result() as $historial){?>

<tr>
<td style="padding: 0.6rem; font-size: 16px"><strong><?= $historial->nombre ?></strong></td>
<td style="padding: 0.6rem; font-size: 16px" class="text-center"><strong><?= $historial->Creditos ?></strong></td>
<td style="padding: 0.6rem; font-size: 16px" class="text-center"><strong><?= $historial->Final ?></strong></td>
<td style="padding: 0.6rem; font-size: 16px" class="text-center"><strong><?= $historial->Cuatrimestre ?></strong></td>
<td style="padding: 0.6rem; font-size: 16px" class="text-center"><strong><?= $historial->Periodo ?></strong></td>
</tr>

<?php
}
?>





</tbody>
</table>
</div>
<!-- TABLA-->


</div>

</div>
  




















</div>
<!-- TABLA-->





<!-- FORMULARIO-->



</div>


<!-- BODY-->





</div>


</div>
</div>

<!-- Container-fluid Ends-->
</div>

<!-- footer start-->
<footer class="footer" style="margin-left: auto; margin-right: auto;">
<div class="row">
<div class="col-md-6 footer-copyright">
<p class="mb-0">Copyright 2022-23 © IMEI / CEP</p>
</div>
<div class="col-md-6">
<p class="pull-right mb-0">Made by "Hack the box" with <i class="fa fa-heart font-secondary"></i></p>
</div>
</div>

</footer>
</div>
</div>

<script type="text/javascript">
	

var cuatrimestre = <?php echo $row->Cuatrimestre ?>;

if(cuatrimestre>=7)
{
$("#tabla_historial").addClass("d-none");
}
else
{
$("#mensaje_alerta").addClass("d-none");
}

</script>


<?=$this->load->view('Include/base_js','',TRUE);?>

</body>
</html>