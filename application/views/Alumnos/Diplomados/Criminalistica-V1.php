
<html lang="en">
<head>
<title>Calificaciones Diplomados</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Integra-Educa">
<meta name="keywords" content="Integra-Educa, Integra">
<meta name="author" content="Mike">

<?=$this->load->view('Include/base_css','',TRUE);?>
<script type="text/javascript" src="<?=base_url('public/assets/js/html2/html2pdf.bundle.min.js')?>"></script>

<link rel="stylesheet" type="text/css" href="<?=base_url('public/assets/css/table.css')?>">
</head>
<body>

<!-- Loader starts-->
<div class="loader-wrapper">
<div class="theme-loader">    
<div class="loader-p"></div>
</div>
</div>
<!-- Loader ends-->


<!-- page-wrapper Start-->
<div class="page-wrapper" id="pageWrapper">

<!-- Page Header Start-->
<?=$this->load->view('Alumnos/Header','',TRUE);?>
<!-- Page Header Start-->


<!-- Page Body Start-->
<div class="page-body-wrapper horizontal-menu">
 
<!-- Page Sidebar Inicio-->
<?=$this->load->view('Alumnos/Side_bar','',TRUE);?>
<!-- Page Sidebar Fin-->

<!-- TITULOS PAGINA-->
<div class="page-body">
<div class="container-fluid">
<div class="page-header">
<div class="row ">
<div class="col-sm-6 ">
<h3>Calificaciones Diplomado</h3>
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="<?= base_url('Alumnos')?>">Inicio</a></li>
<li class="breadcrumb-item">Home</li>
</ol>
</div>
</div>
</div>
</div>
<!-- TITULOS PAGINA-->

<div class="row justify-content-center mr-2 ml-2">
<div class="col-sm-12 col-xl-12 col-lg-12">
</div>
<span class="badge badge-warning text-dark" style="font-size: 22px; border-radius: 25px;">-Sistema Integral de Información-</span>
</div>

<br>



<!-- Container-fluid starts-->
<div class="container-fluid">

<!-- BODY-->


<div class="row card">
  

<!-- TABLA-->
<div class="table-responsive">
<table class="table table-hover align-content-center table-borderless" >

<thead class="table-dark">
<tr class="mt-1">

<th scope="col" style="font-size: 14px; width: 6%" class="text-center text-white">Materia</th>
<th scope="col" style="font-size: 14px; width: 6%" class="text-center text-white">Calificación</th>
</tr>
</thead>


<tbody class="mt-2">


<?php 

foreach ($usuarios->result() as $usuario){?>

<tr>



<td  style="font-size: 15px; font-weight: 600; padding: 0.8rem;" >
<?= $usuario->Materia ?></td>

<td  style="font-size: 15px; font-weight: 600; padding: 0.8rem; 
<?php  if($usuario->Calificacion>=6) {echo ("background-color: #A3E4D7;");} else {echo ("background-color: #E6B0AA;");} ?>" class="rounded-pill text-center text-dark  justify-content-center align-items-center datos"><?= $usuario->Calificacion ?></td>

</tr>

<tr>
<th style="padding: 0.8rem;"></th>
<td style="padding: 0.8rem;"></td>
</tr>



<?php
}
?>



</tbody>
</table>


<br>
<div class="row">

<button type="button" class="btn btn-primary btn-lg d-none">Imprimir</button>
</div>






<div id="content">
    <!-- el contenido HTML va aqui -->
</div>



<div id="content">
    <!-- el contenido HTML va aqui -->
</div>





</div>

</div>
<!-- TABLA-->



<!-- FORMULARIO-->



</div>


<!-- BODY-->





</div>


</div>
</div>

<!-- Container-fluid Ends-->
</div>

<!-- footer start-->
<footer class="footer" style="margin-left: auto; margin-right: auto;">
<div class="row">
<div class="col-md-6 footer-copyright">
<p class="mb-0">Copyright 2022-23 © IMEI / CEP</p>
</div>
<div class="col-md-6">
<p class="pull-right mb-0">Made by "Hack the box" with <i class="fa fa-heart font-secondary"></i></p>
</div>
</div>

</footer>
</div>
</div>
<?=$this->load->view('Include/base_js','',TRUE);?>


</body>
</html>