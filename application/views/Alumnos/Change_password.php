
<html lang="en">
<head>
<title>Contraseña</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Integra-Educa">
<meta name="keywords" content="Integra-Educa, Integra">
<meta name="author" content="Mike">

<?=$this->load->view('Include/base_css','',TRUE);?>


</head>
<body>

<!-- Loader starts-->
<div class="loader-wrapper">
<div class="theme-loader">    
<div class="loader-p"></div>
</div>
</div>
<!-- Loader ends-->


<!-- page-wrapper Start-->
<div class="page-wrapper" id="pageWrapper">

<!-- Page Header Start-->
<?=$this->load->view('Alumnos/Header','',TRUE);?>
<!-- Page Header Start-->


<!-- Page Body Start-->
<div class="page-body-wrapper horizontal-menu">
 
<!-- Page Sidebar Inicio-->
<?=$this->load->view('Alumnos/Side_bar','',TRUE);?>
<!-- Page Sidebar Fin-->

<!-- TITULOS PAGINA-->
<div class="page-body">
<div class="container-fluid">
<div class="page-header">
<div class="row ">
<div class="col-sm-6 ">
<h3>Contraseñas</h3>
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="<?= base_url('Alumnos')?>">Inicio</a></li>
<li class="breadcrumb-item">Home</li>
</ol>
</div>
</div>
</div>
</div>
<!-- TITULOS PAGINA-->

<div class="row justify-content-center mr-2 ml-2">
<div class="col-sm-12 col-xl-12 col-lg-12">
</div>
<span class="badge badge-warning text-dark" style="font-size: 22px; border-radius: 25px;">-Sistema Integral de Información-</span>
</div>

<br>



<div class="row justify-content-center align-items-center text-center p-2">
<div class="m-1 col-sm-8 col-md-8 col-lg-8 shadow-sm p-3 mb-5 bg-white border" style="border-radius: 2rem; !important">
<div class="pt-5 pb-5" style="background-color: #D9E4DD !important; border-radius: 25px;" >
<img class="rounded mx-auto d-block" src="<?php echo base_url('public/assets/images/logo/Cor.png'); ?>" alt="" width=70px height=70px>
<h6 class="text-center text-uppercase mt-3 text-dark">Contraseña</h6>

<form class="theme-form login-form" style="border-radius: 25px; background-color: #FBF7F0
 !important;" method="post" action="<?php echo site_url('Alumnos/Reset_password')?>">
<div class="form-group input-group-md">
<input type="password" class="form-control" type="password" required="" name="Password_1" id="Password_1" style="border-radius: 25px; border-color:#CDC9C3; ">

</div>
<div class="form-group input-group-md">
<input type="password" class="form-control" type="password" name="login[password]" required="" name="Password_2" id="Password_2" style="border-radius: 25px; border-color:#CDC9C3 ; ">


<div id="retun" class="form-control-feedback text-center text-success d-none mt-2"><strong>La contraseñas son iguales</strong></div>

<div id="retun_2" class="form-control-feedback text-center text-dark mt-4">

<strong>La contraseñas no son iguales</strong></div>


</div>
<div class="row justify-content-center">
<button class="btn btn-primary btn-block" type="submit" style="background-color: #2874A6 !important; border-radius: 25px;" value="save" disabled id="Ver">Enviar</button>	
</div>


</form>
</div>

</div>
</div>





<!-- Container-fluid starts-->
<div class="container-fluid">

<!-- BODY-->


<!-- BODY-->


<script type="text/javascript">
  
$("#Password_2").keyup(function(){

var log1 = $("#Password_1").val();
var log2 = $("#Password_2").val();
if(log1==log2)
{
$("#Ver").removeAttr('disabled');
$("#retun_2").addClass('d-none');
$("#retun").removeClass('d-none');
}
else
{
$("#Ver").attr('disabled', 'true');
$("#retun").addClass('d-none');
$("#retun_2").removeClass('d-none');
}
});

</script>


</div>


</div>
</div>

<!-- Container-fluid Ends-->
</div>

<!-- footer start-->
<footer class="footer" style="margin-left: auto; margin-right: auto;">
<div class="row">
<div class="col-md-6 footer-copyright">
<p class="mb-0">Copyright 2022-23 © IMEI / CEP</p>
</div>
<div class="col-md-6">
<p class="pull-right mb-0">Made by "Hack the box" with <i class="fa fa-heart font-secondary"></i></p>
</div>
</div>

</footer>
</div>
</div>
<?=$this->load->view('Include/base_js','',TRUE);?>

</body>
</html>