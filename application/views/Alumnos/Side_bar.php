<!-- Page Sidebar Start-->
<header class="main-nav">

<div class="sidebar-user text-center mt-0" id="sd">

<img class="img-90 rounded-circle mt-0" src="<?php echo base_url('public/assets/images/dashboard/1.png');?>" alt="">
<div class="badge-bottom"><span class="badge badge-primary">New</span></div><a href="user-profile.html">
<h6 class="mt-3 f-14 f-w-600"><?php echo $side_bar->Nombre; ?></h6></a>
<p class="mb-0 font-roboto"><?php echo $side_bar->Carrera; ?></p>
<ul>
<li><span><?php echo $side_bar->Matricula; ?></span>
<p>Matricula</p>
</li>
<li><span><?php echo $side_bar->escuela; ?></span>
<p>Escuela</p>
</li>
<li><span class="counter"><?php echo $side_bar->Promedio; ?></span>
<p>Promedio</p>
</li>
</ul>
</div>

<nav>
<div class="main-navbar">
<div class="left-arrow" id="left-arrow"><i data-feather="corner-down-left"></i></div>
<div id="mainnav">           
<ul class="nav-menu custom-scrollbar">
<li class="back-btn">
<div class="mobile-back text-end"><span>REGRESAR</span><i class="fa fa-angle-right ps-2" aria-hidden="true"></i></div>
</li>


<li class="sidebar-main-title mt-0">
<div class="mt-0">
<h6 class="text-center text-justify mt-0"><strong>GENERAL</strong></h6>
</div>
</li>

<!-- Menu 1-->
<li class="dropdown"><a class="nav-link menu-title" href="javascript:void(0)"><i data-feather="home"></i><span>Comienzo</span></a>
<ul class="nav-submenu menu-content">
<li><a href="<?= base_url('Alumnos')?>">Inicio</a></li>
<li><a href="<?= base_url('Alumnos/Datos_personales')?>">Datos personales</a></li>
<li><a href="<?= base_url('Alumnos/Change_password')?>">Contraseña</a></li>
</ul>
</li>
<!-- Menu 1-->

<!-- Menu 2-->
<li class="dropdown"><a class="nav-link menu-title" href="javascript:void(0)"><i data-feather="airplay"></i><span>Calificaciones</span></a>
<ul class="nav-submenu menu-content">
<li><a href="<?= base_url('Alumnos/Calificaciones')?>">Calificaciones Actuales</a></li>
<li class="d-none"><a href="">Lista de Boletas</a></li>
</ul>
</li>
<!-- Menu 2-->

<!-- Menu 3-->
<li class="dropdown"><a class="nav-link menu-title" href="javascript:void(0)"><i data-feather="layout"></i><span>Horario</span></a>
<ul class="nav-submenu menu-content">
<li><a href="<?= base_url('Alumnos/Horario')?>">Horario Actual</a></li>
</ul>
</li>
<!-- Menu 3-->

<!-- Menu 4-->
<li class="dropdown"><a class="nav-link menu-title" href="javascript:void(0)"><i data-feather="briefcase"></i><span>Diplomados</span></a>
<ul class="nav-submenu menu-content">
<li><a href="<?= base_url('Alumnos/Diplomados')?>">Diplomados Actuales</a></li>
<li><a href="<?= base_url('Alumnos/Diplomado_Criminalistica')?>">Diplomado Criminalistica</a></li>
</ul>
</li>
<!-- Menu 4-->

<!-- Menu 5-->
<li class="dropdown"><a class="nav-link menu-title" href="javascript:void(0)"><i data-feather="box"></i><span>Historial Académico</span></a>
<ul class="nav-submenu menu-content">
<li><a href="<?= base_url('Alumnos/Historial')?>">Historial Académico</a></li>
</ul>
</li>
<!-- Menu 5-->

<!-- Menu 6-->
<li class="dropdown"><a class="nav-link menu-title" href="javascript:void(0)"><i data-feather="edit-3"></i><span>Pagos</span></a>
<ul class="nav-submenu menu-content">
<li><a href="<?= base_url('Alumnos/Pagos')?>">Listado de Pagos</a></li>
</ul>
</li>
<!-- Menu 6-->

<!-- Menu 7-->
<li class="dropdown d-none"><a class="nav-link menu-title" href="javascript:void(0)"><i data-feather="cloud-drizzle"></i><span>Constancias</span></a>
<ul class="nav-submenu menu-content">
<li><a href="scroll-reval.html">Solicitud de Inasitencia</a></li>
<li><a href="animate.html">Solicitud de Constancia</a></li>
</ul>
</li>
<!-- Menu 7-->

<!-- Menu 8-->
<li class="dropdown"><a class="nav-link menu-title" href="javascript:void(0)"><i data-feather="layers"></i><span>Evaluación Continua</span></a>
<ul class="nav-submenu menu-content">
<li><a href="<?= base_url('Alumnos/Evaluacion_maestros')?>">Evaluación de Maestros</a></li>
<li class="d-none"><a href="chart-google.html">Evaluación de Institución</a></li>
</ul>
</li>
<!-- Menu 8-->

<!-- Menu 9-->
<li class="dropdown"><a class="nav-link menu-title" href="javascript:void(0)"><i data-feather="sliders"></i><span>Calendarios</span></a>
<ul class="nav-submenu menu-content">
<li><a href="<?= base_url('Alumnos/Calendario')?>">Calendario Académico</a></li>
</ul>
</li>
<!-- Menu 9-->


<!-- Menu 10-->
<li class="dropdown"><a class="nav-link menu-title" href="javascript:void(0)"><i data-feather="help-circle"></i><span>Otros</span></a>
<ul class="nav-submenu menu-content">
<li><a href="<?= base_url('Alumnos/Ayuda')?>">Ayuda</a></li>
</ul>
</li>
<!-- Menu 10-->

</ul>
</div>
<div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
</div>
</nav>
</header>
<!-- Page Sidebar Ends-->