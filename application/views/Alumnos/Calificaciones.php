
<html lang="en">
<head>
<title>Calificaciones</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Integra-Educa">
<meta name="keywords" content="Integra-Educa, Integra">
<meta name="author" content="Mike">

<?=$this->load->view('Include/base_css','',TRUE);?>
<script type="text/javascript" src="<?=base_url('public/assets/js/html2/html2pdf.bundle.min.js')?>"></script>

<link rel="stylesheet" type="text/css" href="<?=base_url('public/assets/css/table.css')?>">

</head>
<body>

<!-- Loader starts-->
<div class="loader-wrapper">
<div class="theme-loader">    
<div class="loader-p"></div>
</div>
</div>
<!-- Loader ends-->


<!-- page-wrapper Start-->
<div class="page-wrapper" id="pageWrapper">

<!-- Page Header Start-->
<?=$this->load->view('Alumnos/Header','',TRUE);?>
<!-- Page Header Start-->


<!-- Page Body Start-->
<div class="page-body-wrapper horizontal-menu">
 
<!-- Page Sidebar Inicio-->
<?=$this->load->view('Alumnos/Side_bar','',TRUE);?>
<!-- Page Sidebar Fin-->

<!-- TITULOS PAGINA-->
<div class="page-body">
<div class="container-fluid">
<div class="page-header">
<div class="row ">
<div class="col-sm-6 ">
<h3>Calificaciones</h3>
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="<?= base_url('Alumnos')?>">Inicio</a></li>
<li class="breadcrumb-item">Home</li>
</ol>
</div>
</div>
</div>
</div>
<!-- TITULOS PAGINA-->

<div class="row justify-content-center mr-2 ml-2">
<div class="col-sm-12 col-xl-12 col-lg-12">
</div>
<span class="badge badge-warning text-dark" style="font-size: 22px; border-radius: 25px;">-Sistema Integral de Información-</span>
</div>

<br>



<!-- Container-fluid starts-->
<div class="container-fluid">

<!-- BODY-->


<div class="row card">
  

<!-- TABLA-->
<div class="table-responsive">
<table class="table table-hover align-content-center table-borderless" >

<thead class="table-dark">
<tr class="mt-1">
<th scope="col" style="font-size: 15px; width: 25%" class="text-left align-self-center text-white">Materia</th>
<th scope="col" style="font-size: 15px; width: 25%" class="text-white">Profesor</th>
<th scope="col" style="font-size: 15px; width: 6%" class="text-white">Grupo</th>
<th scope="col" style="font-size: 14px; width: 6%" class="text-center text-white">Parcial 1</th>
<th scope="col" style="font-size: 14px; width: 6%" class="text-center text-white">Parcial 2</th>
<th scope="col" style="font-size: 14px; width: 6%" class="text-center text-white">Parcial 3</th>
<th scope="col" style="font-size: 14px; width: 6%" class="text-center text-white">Final</th>
</tr>
</thead>


<tbody class="mt-2">

<?php 

foreach ($usuarios->result() as $usuario){?>


<tr>
<th style="padding: 0.8rem;"></th>
<td style="padding: 0.8rem;"></td>
<td style="padding: 0.8rem;"></td>
<td style="padding: 0.8rem;"></td>
<td style="padding: 0.8rem;"></td>
<td style="padding: 0.8rem;"></td>
<td style="padding: 0.8rem;"></td>
</tr>

<tr>

<th scope="row" style="padding: 0.8rem; font-size: 12px" class="text-left"><strong><?= $usuario->nombre ?></strong></th>

<td style="padding: 0.8rem; font-size: 15px; font-weight: bold;
" class="text-left"><strong><?= $usuario->profesor ?></strong></td>

<td style="padding: 0.8rem; font-size: 15px" class="text-center"><strong><?= $usuario->id_salon ?></strong></td>

<td  style="font-size: 15px; font-weight: 600; padding: 0.8rem; 
<?php  if($usuario->calificacion_1>=6) {echo ("background-color: #A3E4D7;");} else {echo ("background-color: #E6B0AA;");} ?>" class="rounded-pill text-center text-dark justify-content-center align-items-center"><?= $usuario->calificacion_1 ?></td>

<td  style="font-size: 15px; font-weight: 600; padding: 0.8rem; 
<?php  if($usuario->calificacion_2>=6) {echo ("background-color: #A3E4D7;");} else {echo ("background-color: #E6B0AA;");} ?>" class="rounded-pill text-center text-dark  justify-content-center align-items-center"><?= $usuario->calificacion_2 ?></td>

<td style="font-size: 15px; font-weight: 600; padding: 0.8rem; 
<?php  if($usuario->calificacion_3>=6) {echo ("background-color: #A3E4D7;");} else {echo ("background-color: #E6B0AA;");} ?>" class="rounded-pill text-center text-dark  justify-content-center align-items-center"><?= $usuario->calificacion_3 ?></td>

<td style="font-size: 15px; padding: 0.8rem; background-color: #84b6f4;" class="rounded-pill text-center text-dark justify-content-center align-items-center"><?= $usuario->final ?></td>

</tr>

<tr>
<th style="padding: 0.8rem;"></th>
<td style="padding: 0.8rem;"></td>
<td style="padding: 0.8rem;"></td>
<td style="padding: 0.8rem;"></td>
<td style="padding: 0.8rem;"></td>
<td style="padding: 0.8rem;"></td>
<td style="padding: 0.8rem;"></td>
</tr>



<?php
}
?>

</tbody>
</table>


<br>
<div class="row">

<button type="button" class="btn btn-primary btn-lg " onclick="geneatePDF()">Imprimir</button>
</div>




<script type="text/javascript">
  
  function geneatePDF(){

var elementoParaConvertir;
var escuela = '<?php echo($side_bar->escuela); ?>';
if(escuela=='IMEI')
{
elementoParaConvertir = document.getElementById('Impresion');
}
else
{
elementoParaConvertir = document.getElementById('Impresion_2');
} 

html2pdf()
    .set({
        margin: 1,
        filename: 'Boleta_calificaciones.pdf',
        image: {
            type: 'jpeg',
            quality: 0.98
        },
        html2canvas: {
            scale: 3, // A mayor escala, mejores gráficos, pero más peso
            letterRendering: true,
        },
        jsPDF: {
            unit: "in",
            format: "a4",
            orientation: 'landscape' // landscape o portrait
        }
    })
    .from(elementoParaConvertir)
    .save()
    .catch(err => console.log(err));
  }

</script>

<div id="content">
    <!-- el contenido HTML va aqui -->
</div>

<div id="elementH"></div>


<!--Cuerpo de la boleta de IMEI-->
<div id="Impresion" style="position:relative; z-index: -100;">
  


  <div class="row">

  <div class="col-12"> 

<table border=0 cellpadding=0 cellspacing=0 width=960 style='border-collapse: 
 collapse;table-layout:auto;width:720pt' id="mytab1" >
 <col width=80 span=12 style='width:60pt'>
 <tr height=21 style='mso-height-source:userset;height:15.75pt' id='r0'>

<td width=80 style='width:60pt;'></td>
<td width=80 style='width:60pt;'></td>
<td width=80 style='width:60pt;'></td>
<td width=80 style='width:60pt;'></td>
<td width=80 style='width:60pt;'></td>
<td width=80 style='width:60pt;'></td>
<td width=80 style='width:60pt;'></td>
<td width=80 style='width:60pt;'></td>
<td width=80 style='width:60pt;'></td>
<td width=80 style='width:60pt;'></td>
<td width=80 style='width:60pt;'></td>
 </tr>

<tr height=25 style='mso-height-source:userset;height:18.75pt ' id='r1'>

<td colspan=11 height=23 class=x100 width=880 style='border-right:2px solid windowtext;height:17.25pt;width:660pt;' align=left valign=top><span style='mso-ignore:vglayout;position:absolute;z-index:0;margin-left:40px;margin-top:15px;width:95px;height:90px'><img width=95 height=90 src="<?=base_url('library/vendors/images/AM1.png')?>" name='Picture 55' alt=escudo imei></span><span  style='mso-ignore:vglayout2'><table cellpadding=0 cellspacing=0><tr><td class="text-center" colspan=11 height=23 class=x99 width=880 style='height:17.25pt;width:660pt; border-top: 2px solid windowtext;'><strong>&nbsp&nbsp&nbspGRUPO EDUCATIVO IMEI</strong></td></tr></table></span></td>
 </tr>


 <tr height=21 style='mso-height-source:userset;height:15.75pt' id='r2'>

<td colspan=11 class=x24 style='border-right:2px solid windowtext;' class="text-center">"La Verdad Hace Justicia a una Nación"</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15pt' id='r3'>

<td colspan=11 class=x27 style='border-right:2px solid windowtext;' class="text-center">CONTROL ESCOLAR</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15pt' id='r4'>

<td colspan=11 class=x27 style='border-right:2px solid windowtext;' class="text-center">PLANTEL TLAXCALA</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15pt' id='r5'>

<td colspan=11 class=x30 style='border-right:2px solid windowtext;'>Boleta de Calificaciones</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15pt' id='r6'>

<td colspan=8 class=x30></td>
<td class=x31></td>
<td class=x31></td>
<td class=x32></td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15pt' id='r7'>

<td class=x33>&nbsp; Nombre del Alumno: 
<?php 
$N1=$row->Nombres;
$F1=" ";
$N2=$row->Apellido_Paterno;
$N3=$row->Apellido_Materno;
$Nombre = $N1.$F1.$N2.$F1.$N3;
echo $Nombre; 
?>
</td>
<td class=x34></td>
<td class=x34></td>
<td class=x35></td>
<td class=x36></td>
<td class=x37></td>
<td colspan=2 class=x38>Cuatrimestre: <?= $row->Cuatrimestre ?></td>
<td colspan=2 class=x39><span style='mso-spacerun:yes'>&nbsp;&nbsp; </span>Generacion: SEP21-AGO24</td>
<td class=x40></td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15pt' id='r8'>

<td colspan=2 class=x41>&nbsp; Licenciatura: <?= $row->Carrera ?> </td>
<td class=x31></td>
<td class=x42></td>
<td class=x37></td>
<td class=x31></td>
<td colspan=2 class=x38>Grupo: </td>
<td class=x31></td>
<td class=x31></td>
<td class=x32></td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15pt' id='r9'>

<td class=x33>&nbsp; Turno: <?= $row->Turno ?></td>
<td class=x35></td>
<td class=x34></td>
<td class=x34></td>
<td class=x36></td>
<td class=x31></td>
<td colspan=2 class=x38>Ciclo<span style='mso-spacerun:yes'>&nbsp; </span>Escolar: SEP21</td>
<td class=x31></td>
<td class=x31></td>
<td class=x32></td>
 </tr>
 <tr height=21 style='mso-height-source:userset;height:15.75pt' id='r10'>

<td class=x43></td>
<td class=x44></td>
<td class=x44></td>
<td class=x44></td>
<td class=x45></td>
<td class=x46></td>
<td class=x44></td>
<td class=x44></td>
<td class=x47></td>
<td class=x47></td>
<td class=x48></td>
 </tr>
 <tr height=21 style='mso-height-source:userset;height:18pt' id='r11'>

<td colspan=3 class=x49 style='border-right:2px solid windowtext;border-bottom:2px solid windowtext;'>Materias</td>
<td colspan=2 class=x52 style='border-right:2px solid windowtext;border-bottom:2px solid windowtext;'>1er Parcial</td>
<td colspan=2 class=x52 style='border-right:2px solid windowtext;border-bottom:2px solid windowtext;'>2do Parcial</td>
<td colspan=2 class=x52 style='border-right:2px solid windowtext;border-bottom:2px solid windowtext;'>Final </td>
<td colspan=2 class=x53 style='border-right:2px solid windowtext;'>Promedio </td>
 </tr>
 <tr height=21 style='mso-height-source:userset;height:height:25pt' id='r12'>

<td colspan=3 class=x55 style='border-right:2px solid windowtext;border-bottom:2px solid windowtext;'></td>
<td class=x56>Número</td>
<td class=x56>Letra</td>
<td class=x57>Número</td>
<td class=x56>Letra</td>
<td class=x57>Número</td>
<td class=x58>Letra</td>
<td class=x59>Número</td>
<td class=x59>Letra</td>
 </tr>


<?php 
require_once "CifrasEnLetras.php";

$cont_1=1;
$cont_2=1;
$cont_3=1;
$cont_4=1;


foreach ($usuarios->result() as $usuario){?>

<?php 

$cont_1 = $cont_1 + 1; $cont_2 = $cont_2 + 1;
$cont_3 = $cont_3 + 1; $cont_4 = $cont_4 + 1;

$califi_1=0; $califi_2=0; $califi_3=0; $califi_4=0;

$califi_1= $califi_1 + $usuario->calificacion_1;
$califi_2= $califi_2 + $usuario->calificacion_2;
$califi_3= $califi_3 + $usuario->calificacion_3;
$califi_4= $califi_4 + $usuario->final;



$f1 = round($califi_1 / $cont_1, 0);
$f2 = round($califi_2 / $cont_2, 0);
$f3 = round($califi_3 / $cont_3, 0);
$f4 = round($califi_4 / $cont_4, 0);

$v=new CifrasEnLetras(); 
//Convertimos el total en letras
$letra_1=($v->convertirNumeroEnLetras(floatval($usuario->calificacion_1)));
$letra_2=($v->convertirNumeroEnLetras(floatval($usuario->calificacion_2)));
$letra_3=($v->convertirNumeroEnLetras(floatval($usuario->calificacion_3)));
$letra_4=($v->convertirNumeroEnLetras(floatval($usuario->final)));

$letra_5=($v->convertirNumeroEnLetras(floatval($row2->calif_1)));
$letra_6=($v->convertirNumeroEnLetras(floatval($row2->calif_2)));
$letra_7=($v->convertirNumeroEnLetras(floatval($row2->calif_3)));
$letra_8=($v->convertirNumeroEnLetras(floatval($row2->final)));

 ?>





<tr height=20 style='mso-height-source:userset;height:18pt'  class="text-center" id="01">
<td colspan=3 class=x60 style='border-right:2px solid windowtext;border-bottom:1px solid windowtext;' ><?= $usuario->nombre ?></td>
<td class=x63 id="02"><?= $usuario->calificacion_1 ?></td>
<td class=x64><?= $letra_1 ?></td>
<td class=x65 id="02"><?= $usuario->calificacion_2 ?></td>
<td class=x64><?= $letra_2 ?></td>
<td class=x65><?= $usuario->calificacion_3 ?></td>
<td class=x66><?= $letra_3 ?></td>
<td class=x67><?= $usuario->final ?></td>
<td class=x68><?= $letra_4 ?></td>
 </tr>



<?php
}
?>


 <tr height=21 style='mso-height-source:userset;height:18pt' id='r18'>

<td colspan=3 class=x91 style='border-right:2px solid windowtext;border-bottom:2px solid windowtext;height:25pt'>Promedio</td>
<td class=x94 align=right ><?= $row2->calif_1; ?></td>
<td class=x95><?= $letra_5 ?></td>
<td class=x94 align=center ><?= $row2->calif_2; ?></td>
<td class=x96><?= $letra_6 ?></td>
<td class=x94 align=center ><?= $row2->calif_3; ?></td>
<td class=x97><?= $letra_7 ?></td>
<td class=x98 align=center ><?= $row2->final; ?></td>
<td class=x96><?= $letra_8 ?></td>
 </tr>


</table>

</div>
</div>

</div> 
<!--Cuerpo de la boleta-->

<div id="content">
    <!-- el contenido HTML va aqui -->
</div>

<div id="elementH"></div>

<!--Cuerpo de la boleta CEP -->

<div id="Impresion_2" style="position:relative; z-index: -100;">
  


  <div class="row">

  <div class="col-12"> 

<table border=0 cellpadding=0 cellspacing=0 width=960 style='border-collapse: 
 collapse;table-layout:auto;width:720pt' id="mytab1" >
 <col width=80 span=12 style='width:60pt'>
 <tr height=21 style='mso-height-source:userset;height:15.75pt' id='r0'>

<td width=80 style='width:60pt;'></td>
<td width=80 style='width:60pt;'></td>
<td width=80 style='width:60pt;'></td>
<td width=80 style='width:60pt;'></td>
<td width=80 style='width:60pt;'></td>
<td width=80 style='width:60pt;'></td>
<td width=80 style='width:60pt;'></td>
<td width=80 style='width:60pt;'></td>
<td width=80 style='width:60pt;'></td>
<td width=80 style='width:60pt;'></td>
<td width=80 style='width:60pt;'></td>
 </tr>

<tr height=25 style='mso-height-source:userset;height:18.75pt ' id='r1'>

<td colspan=11 height=23 class=x100 width=880 style='border-right:2px solid windowtext;height:17.25pt;width:660pt;' align=left valign=top><span style='mso-ignore:vglayout;position:absolute;z-index:0;margin-left:40px;margin-top:15px;width:95px;height:90px'><img width=95 height=90 src="<?=base_url('library/vendors/images/unicep-logo.png')?>" name='Picture 55' alt=escudo imei></span><span  style='mso-ignore:vglayout2'><table cellpadding=0 cellspacing=0>
    <tr><td class="text-center" colspan=11 height=23 class=x99 width=880 style='height:17.25pt;width:660pt; border-top: 2px solid windowtext;'><strong>&nbsp&nbsp&nbspUniversidad del Centro Educativo de Puebla</strong></td></tr></table></span></td>
 </tr>


 <tr height=21 style='mso-height-source:userset;height:15.75pt' id='r2'>

<td colspan=11 class=x24 style='border-right:2px solid windowtext;' class="text-center"></td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15pt' id='r3'>

<td colspan=11 class=x27 style='border-right:2px solid windowtext;' class="text-center">CONTROL ESCOLAR</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15pt' id='r4'>

<td colspan=11 class=x27 style='border-right:2px solid windowtext;' class="text-center">PLANTEL TLAXCALA</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15pt' id='r5'>

<td colspan=11 class=x30 style='border-right:2px solid windowtext;'>Boleta de Calificaciones</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15pt' id='r6'>

<td colspan=8 class=x30></td>
<td class=x31></td>
<td class=x31></td>
<td class=x32></td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15pt' id='r7'>

<td class=x33>&nbsp; Nombre del Alumno: 
<?php 
$N1=$row->Nombres;
$F1=" ";
$N2=$row->Apellido_Paterno;
$N3=$row->Apellido_Materno;
$Nombre = $N1.$F1.$N2.$F1.$N3;
echo $Nombre; 
?>
</td>
<td class=x34></td>
<td class=x34></td>
<td class=x35></td>
<td class=x36></td>
<td class=x37></td>
<td colspan=2 class=x38>Cuatrimestre: <?= $row->Cuatrimestre ?></td>
<td colspan=2 class=x39><span style='mso-spacerun:yes'>&nbsp;&nbsp; </span>Generacion: SEP21-AGO24</td>
<td class=x40></td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15pt' id='r8'>

<td colspan=2 class=x41>&nbsp; Licenciatura: <?= $row->Carrera ?> </td>
<td class=x31></td>
<td class=x42></td>
<td class=x37></td>
<td class=x31></td>
<td colspan=2 class=x38>Grupo: </td>
<td class=x31></td>
<td class=x31></td>
<td class=x32></td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15pt' id='r9'>

<td class=x33>&nbsp; Turno: <?= $row->Turno ?></td>
<td class=x35></td>
<td class=x34></td>
<td class=x34></td>
<td class=x36></td>
<td class=x31></td>
<td colspan=2 class=x38>Ciclo<span style='mso-spacerun:yes'>&nbsp; </span>Escolar: SEP21</td>
<td class=x31></td>
<td class=x31></td>
<td class=x32></td>
 </tr>
 <tr height=21 style='mso-height-source:userset;height:15.75pt' id='r10'>

<td class=x43></td>
<td class=x44></td>
<td class=x44></td>
<td class=x44></td>
<td class=x45></td>
<td class=x46></td>
<td class=x44></td>
<td class=x44></td>
<td class=x47></td>
<td class=x47></td>
<td class=x48></td>
 </tr>
 <tr height=21 style='mso-height-source:userset;height:18pt' id='r11'>

<td colspan=3 class=x49 style='border-right:2px solid windowtext;border-bottom:2px solid windowtext;'>Materias</td>
<td colspan=2 class=x52 style='border-right:2px solid windowtext;border-bottom:2px solid windowtext;'>1er Parcial</td>
<td colspan=2 class=x52 style='border-right:2px solid windowtext;border-bottom:2px solid windowtext;'>2do Parcial</td>
<td colspan=2 class=x52 style='border-right:2px solid windowtext;border-bottom:2px solid windowtext;'>Final </td>
<td colspan=2 class=x53 style='border-right:2px solid windowtext;'>Promedio </td>
 </tr>
 <tr height=21 style='mso-height-source:userset;height:height:25pt' id='r12'>

<td colspan=3 class=x55 style='border-right:2px solid windowtext;border-bottom:2px solid windowtext;'></td>
<td class=x56>Número</td>
<td class=x56>Letra</td>
<td class=x57>Número</td>
<td class=x56>Letra</td>
<td class=x57>Número</td>
<td class=x58>Letra</td>
<td class=x59>Número</td>
<td class=x59>Letra</td>
 </tr>


<?php 
require_once "CifrasEnLetras.php";

$cont_1=1;
$cont_2=1;
$cont_3=1;
$cont_4=1;


foreach ($usuarios->result() as $usuario){?>

<?php 

$cont_1 = $cont_1 + 1; $cont_2 = $cont_2 + 1;
$cont_3 = $cont_3 + 1; $cont_4 = $cont_4 + 1;

$califi_1=0; $califi_2=0; $califi_3=0; $califi_4=0;

$califi_1= $califi_1 + $usuario->calificacion_1;
$califi_2= $califi_2 + $usuario->calificacion_2;
$califi_3= $califi_3 + $usuario->calificacion_3;
$califi_4= $califi_4 + $usuario->final;



$f1 = round($califi_1 / $cont_1, 0);
$f2 = round($califi_2 / $cont_2, 0);
$f3 = round($califi_3 / $cont_3, 0);
$f4 = round($califi_4 / $cont_4, 0);

$v=new CifrasEnLetras(); 
//Convertimos el total en letras
$letra_1=($v->convertirNumeroEnLetras(floatval($usuario->calificacion_1)));
$letra_2=($v->convertirNumeroEnLetras(floatval($usuario->calificacion_2)));
$letra_3=($v->convertirNumeroEnLetras(floatval($usuario->calificacion_3)));
$letra_4=($v->convertirNumeroEnLetras(floatval($usuario->final)));

$letra_5=($v->convertirNumeroEnLetras(floatval($row2->calif_1)));
$letra_6=($v->convertirNumeroEnLetras(floatval($row2->calif_2)));
$letra_7=($v->convertirNumeroEnLetras(floatval($row2->calif_3)));
$letra_8=($v->convertirNumeroEnLetras(floatval($row2->final)));

 ?>





<tr height=20 style='mso-height-source:userset;height:18pt'  class="text-center" id="01">
<td colspan=3 class=x60 style='border-right:2px solid windowtext;border-bottom:1px solid windowtext;' ><?= $usuario->nombre ?></td>
<td class=x63 id="02"><?= $usuario->calificacion_1 ?></td>
<td class=x64><?= $letra_1 ?></td>
<td class=x65 id="02"><?= $usuario->calificacion_2 ?></td>
<td class=x64><?= $letra_2 ?></td>
<td class=x65><?= $usuario->calificacion_3 ?></td>
<td class=x66><?= $letra_3 ?></td>
<td class=x67><?= $usuario->final ?></td>
<td class=x68><?= $letra_4 ?></td>
 </tr>



<?php
}
?>


 <tr height=21 style='mso-height-source:userset;height:18pt' id='r18'>

<td colspan=3 class=x91 style='border-right:2px solid windowtext;border-bottom:2px solid windowtext;height:25pt'>Promedio</td>
<td class=x94 align=right ><?= $row2->calif_1; ?></td>
<td class=x95><?= $letra_5 ?></td>
<td class=x94 align=center ><?= $row2->calif_2; ?></td>
<td class=x96><?= $letra_6 ?></td>
<td class=x94 align=center ><?= $row2->calif_3; ?></td>
<td class=x97><?= $letra_7 ?></td>
<td class=x98 align=center ><?= $row2->final; ?></td>
<td class=x96><?= $letra_8 ?></td>
 </tr>


</table>

</div>
</div>

</div>

<!--Cuerpo de la boleta CEP -->

</div>

</div>
<!-- TABLA-->


<script type="text/javascript">
  

  let celdad= document.querySelectorAll('tr')
  console.log(celdad)
</script>


<!-- FORMULARIO-->



</div>


<!-- BODY-->





</div>


</div>
</div>

<!-- Container-fluid Ends-->
</div>

<!-- footer start-->
<footer class="footer" style="margin-left: auto; margin-right: auto;">
<div class="row">
<div class="col-md-6 footer-copyright">
<p class="mb-0">Copyright 2022-23 © IMEI / CEP</p>
</div>
<div class="col-md-6">
<p class="pull-right mb-0">Made by "Hack the box" with <i class="fa fa-heart font-secondary"></i></p>
</div>
</div>

</footer>
</div>
</div>
<?=$this->load->view('Include/base_js','',TRUE);?>

</body>
</html>