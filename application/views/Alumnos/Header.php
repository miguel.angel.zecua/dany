<!-- HEADER DE LOS ALUMNOS-->

<!-- Page Header Start-->
<div class="page-main-header" id="barra">
<div class="main-header-right row m-0">
<div class=" row main-header-left ">

<div class="toggle-sidebar text-center"> <i class="status_toggle middle" data-feather="align-center" id="sidebar-toggle"></i></div>
</div>



<div class="nav-right col pull-right right-menu p-0">
<ul class="nav-menus">
<li><a class="text-dark" href="#!" onclick="javascript:toggleFullScreen()"><i data-feather="maximize"></i></a></li>
<li class="onhover-dropdown">
<div class="bookmark-box"><i data-feather="star"></i></div>
<div class="bookmark-dropdown onhover-show-div">

<ul class="m-t-5">
<li class="add-to-bookmark mt-2">
<a href="<?= base_url('Alumnos/Calificaciones')?>">
<i class="bookmark-icon" data-feather="inbox"></i>Acta de Calificaciones<span class="pull-right"><i data-feather="star"></i></span>
</a>
</li>

<li class="add-to-bookmark mt-2">
<a href="<?= base_url('Alumnos/Horario')?>">
<i class="bookmark-icon" data-feather="message-square"></i>Horarios Actuales<span class="pull-right"><i data-feather="star"></i></span>
</a>
</li>

<li class="add-to-bookmark mt-2">
<a href="<?= base_url('Alumnos/Pagos')?>">
<i class="bookmark-icon" data-feather="command"></i>Pagos<span class="pull-right"><i data-feather="star"></i></span>
</a>
</li>

<li class="add-to-bookmark mt-2">
<a href="<?= base_url('Alumnos/Historial')?>">
<i class="bookmark-icon" data-feather="airplay"></i>Historial Academico<span class="pull-right"><i data-feather="star">   </i></span></li>
</ul>
</div>
</a>
</li>


<!-- NOTIFICACIONES-->
<?=$this->load->view('Alumnos/Notify','',TRUE);?>
<!-- NOTIFICACIONES-->


<li>
<div class="mode"><i class="fa fa-moon-o"></i></div>
</li>


<li class="onhover-dropdown p-0">
<button class="btn btn-primary-light" type="button"><a href="<?= base_url('Alumnos/End')?>"><i data-feather="log-out"></i>Cerrar sesión</a></button>
</li>
</ul>
</div>
<div class="d-lg-none mobile-toggle pull-right w-auto"><i data-feather="more-horizontal"></i></div>
</div>
</div>
<!-- Page Header Ends-->