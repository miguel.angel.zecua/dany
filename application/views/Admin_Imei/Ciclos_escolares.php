
<html lang="en">
<head>
<title>Ciclos Escolares</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Integra-Educa">
<meta name="keywords" content="Integra-Educa, Integra">
<meta name="author" content="Mike">

<?=$this->load->view('Include/base_css','',TRUE);?>


</head>
<body>

<!-- Loader starts-->
<div class="loader-wrapper">
<div class="theme-loader">    
<div class="loader-p"></div>
</div>
</div>
<!-- Loader ends-->


<!-- page-wrapper Start-->
<div class="page-wrapper" id="pageWrapper">

<!-- Page Header Start-->
<?=$this->load->view('Admin_Imei/Header','',TRUE);?>
<!-- Page Header Start-->


<!-- Page Body Start-->
<div class="page-body-wrapper horizontal-menu">
 
<!-- Page Sidebar Inicio-->
<?=$this->load->view('Admin_Imei/Side_bar','',TRUE);?>
<!-- Page Sidebar Fin-->

<!-- TITULOS PAGINA-->
<div class="page-body">
<div class="container-fluid">
<div class="page-header">
<div class="row ">
<div class="col-sm-6 ">
<h3>Ciclos Escolares</h3>
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="index.html">Inicio</a></li>
<li class="breadcrumb-item">Home</li>
</ol>
</div>
</div>
</div>
</div>
<!-- TITULOS PAGINA-->




<div class="row justify-content-center mr-2 ml-2">
<div class="col-sm-12 col-xl-12 col-lg-12">
</div>
<span class="badge badge-warning text-dark" style="font-size: 22px; border-radius: 25px;">-Sistema Integral de Información-</span>
</div>

<br>




<!-- Container-fluid starts-->
<div class="container-fluid">

<!-- BODY-->
<div class="row">
    
<div class="col-sm-12 col-xl-12 col-lg-12">

<?php foreach ($ciclos->result() as $ciclos){?>

<div class="col-sm-6 col-xl-3 col-lg-6">
<div class="card o-hidden border-0">
<a href="Ciclo/<?= $ciclos->periodo ?>" onclick="miFunc('<?= $ciclos->periodo ?>')">
<div class="bg-danger b-r-4 card-body">
<div class="media static-top-widget">
<div class="align-self-center text-center"><i data-feather="database"></i></div>
<div class="media-body"><span class="m-0"></span>
<h5 class="mb-0"><strong><?= $ciclos->periodo ?></strong></h5><i class="icon-bg" data-feather="archive"></i>
</div>
</div>
</div>
</a>
</div>
</div>


<?php
}
?>


</div>

</div>





</div>

<!-- BODY-->


</div>


</div>
</div>

<!-- Container-fluid Ends-->
</div>




<!-- footer start-->
<footer class="footer" style="margin-left: auto; margin-right: auto;">
<div class="row">
<div class="col-md-6 footer-copyright">
<p class="mb-0">Copyright 2022-23 © IMEI / CEP</p>
</div>
<div class="col-md-6">
<p class="pull-right mb-0">Made by "Hack the box" with <i class="fa fa-heart font-secondary"></i></p>
</div>
</div>

</footer>
</div>
</div>





<?=$this->load->view('Include/base_js','',TRUE);?>


<!-- jquery y bootstrap -->
<script src="https://code.jquery.com/jquery-3.5.1.js"></script> 


<script type="text/javascript">
$( document ).ready(function() {
sessionStorage.clear();
});

function miFunc(periodo) 
{
sessionStorage.clear();
sessionStorage.setItem("periodo", periodo);
}

</script>



</body>
</html>