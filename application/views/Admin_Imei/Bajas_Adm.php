
<html lang="en">
<head>
<title>Bajas</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Integra-Educa">
<meta name="keywords" content="Integra-Educa, Integra">
<meta name="author" content="Mike">

<?=$this->load->view('Include/base_css','',TRUE);?>
<script type="text/javascript" src="<?=base_url('public/assets/js/html2/html2pdf.bundle.min.js')?>"></script>

<link rel="stylesheet" type="text/css" href="<?=base_url('public/assets/css/table.css')?>">


<link rel="stylesheet" type="text/css" href="<?=base_url('public/assets/css/feather-icon.css')?>">


<link rel="stylesheet" type="text/css" href="<?=base_url('public/assets/css/datatables.css')?>">

<script src="<?php echo base_url('public/assets/js/Jquery/jquery.js') ?>"></script>

</head>
<body>

<!-- Loader starts-->
<div class="loader-wrapper">
<div class="theme-loader">    
<div class="loader-p"></div>
</div>
</div>
<!-- Loader ends-->


<!-- page-wrapper Start-->
<div class="page-wrapper" id="pageWrapper">

<!-- Page Header Start-->
<?=$this->load->view('Admin_Imei/Header','',TRUE);?>
<!-- Page Header Start-->


<!-- Page Body Start-->
<div class="page-body-wrapper horizontal-menu">
 
<!-- Page Sidebar Inicio-->
<?=$this->load->view('Admin_Imei/Side_bar','',TRUE);?>
<!-- Page Sidebar Fin-->

<!-- TITULOS PAGINA-->
<div class="page-body">
<div class="container-fluid">
<div class="page-header">
<div class="row ">
<div class="col-sm-6 ">
<h3>Bajas Administrativas</h3>
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="index.html">Inicio</a></li>
<li class="breadcrumb-item">Home</li>
</ol>
</div>
</div>
</div>
</div>
<!-- TITULOS PAGINA-->


<!-- Modal -->
<div class="modal fade" id="update_user_modal" tabindex="-1" aria-labelledby="myLargeModalLabel"  aria-modal="true" role="dialog">
<div class="modal-dialog modal-xl">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title" id="exampleModalLabel" style="font-weight: bold;">Boleta de calificaciones</h5>
<button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close" data-bs-original-title="" title=""></button>
</div>

<div class="modal-body">

<div class="row">

<div class="col-sm-12 col-xl-12 col-lg-12">

<div class="table-responsive" style="border-radius: 25px !important; font-family: sans-serif;">

<table class="table table-hover table-bordered align-content-center table-secondary" >

<thead class="table-dark">
<tr class="mt-1">
<th scope="col" style="font-size: 16px; width: 25%" class="text-center text-white">Materia</th>
<th scope="col" style="font-size: 16px; width: 25%" class="text-center text-white">Profesor</th>
<th scope="col" style="font-size: 16px; width: 6%" class="text-center text-white">Grupo</th>
<th scope="col" style="font-size: 16px; width: 10%" class="text-center text-white">Parcial 1</th>
<th scope="col" style="font-size: 16px; width: 10%" class="text-center text-white">Parcial 2</th>
<th scope="col" style="font-size: 16px; width: 10%" class="text-center text-white">Parcial 3</th>
<th scope="col" style="font-size: 16px; width: 10%" class="text-center text-white">Final</th>
</tr>
</thead>


<tbody class="mt-2" id="records_content">




</tbody>
</table>



</div>

</div>
</div>
<hr style="height: 4px; background-color: darkgrey; border-radius: 25px !important;" >
<div class="row justify-content-center">

<a  class="btn btn-danger btn-lg active" role="button" aria-pressed="true" id="newlocation" style="border-radius: 25px;">Ver Boleta</a>   

</div>

</div>

</div>
</div>
</div>
<!-- Modal -->


<div class="row justify-content-center mr-2 ml-2">
<div class="col-sm-12 col-xl-12 col-lg-12">
</div>
<span class="badge badge-warning text-dark" style="font-size: 22px; border-radius: 25px;">-Sistema Integral de Información-</span>
</div>

<br>



<!-- Container-fluid starts-->

<div class="container-fluid">

<!-- BODY-->

<div class="row justify-content-center">
  


<div class="col-sm-12">

<h2 class="text-center"><strong>Tabla de Alumnos</strong></h2>

<div class="card" style="background-color: #D0ECE7 ; border-radius: 50px;">

<div class="card-body">
<div class="table-responsive">
<div id="basic-1_wrapper" class="dataTables_wrapper no-footer">

<table class="display dataTable no-footer" id="basic-1" role="grid" aria-describedby="basic-1_info">
<thead>
<tr role="row">
<th class="sorting_asc" tabindex="0" aria-controls="basic-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 123.188px;">Matricula</th>

<th class="sorting" tabindex="0" aria-controls="basic-1" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 208.141px;">Nombre</th>

<th class="sorting" tabindex="0" aria-controls="basic-1" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 90.5625px;">Cuatrimestre</th>

<th class="sorting" tabindex="0" aria-controls="basic-1" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 90.5625px;">Turno</th>

<th class="sorting" tabindex="0" aria-controls="basic-1" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 90.5625px;">Carrera</th>

<th class="sorting" tabindex="0" aria-controls="basic-1" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 90.5625px;">Escuela</th>

<th class="sorting" tabindex="0" aria-controls="basic-1" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 90.5625px;">Status</th>

<th class="sorting" tabindex="0" aria-controls="basic-1" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending" style="width: 34.6406px;">Modoficar Status</th>


</thead>

<tbody>
                          
<?php foreach ($datos_alums->result() as $datos_alums){?>
                          
<tr role="row" class="odd">
<td class="sorting_1 text-center" style="font-size: 15px;"><strong><?= $datos_alums->Matricula ?></strong></td>
<td class="text-center" style="font-size: 15px;"><strong><?= $datos_alums->nombre ?></strong></td>
<td class="text-center" style="font-size: 15px;"><strong><?= $datos_alums->Cuatrimestre ?></strong></td>
<td class="text-center" style="font-size: 15px;"><strong><?= $datos_alums->Id_Salon ?></strong></td>
<td class="text-center" style="font-size: 15px;"><strong><?= $datos_alums->Turno ?></strong></td>
<td class="text-center" style="font-size: 15px;"><strong><?= $datos_alums->Carrera ?></strong></td>
<td class="text-center" style="font-size: 15px;"><strong><?= $datos_alums->escuela ?></strong></td>
<td class="text-center" style="font-size: 15px;"><strong><?= $datos_alums->status ?></strong></td>
<td class="text-center" style="font-size: 15px;"><button onclick="GetUserDetails('')" class="btn btn-warning"><i data-feather="eye" style="font-size: 28px; color: black; height: 18px;"></i></button></td>
</tr>

<?php
}
?>

</tbody>

</table>


</div>
</div>
</div>
</div>
</div>


</div>




</div>

<!-- BODY-->

<script type="text/javascript">

function GetUserDetails(id) {
// Add User ID to the hidden field for furture usage
$("#hidden_user_id").val(id);
$.post("GetCalificaciones", {
id: id
},
function (data) {
$("#records_content").html(data);

//alert(user.post[0].nombre)
}
);
// Open modal popup


//var local= window.location;
var uax = 'Boleta_ciclo/'
var ref = id;
var con = uax+ref;
document.getElementById('newlocation').setAttribute('href', con);
$("#update_user_modal").modal("show");

}

</script>


</div>


</div>
</div>
<!-- Container-fluid Ends-->
</div>

<!-- footer start-->
<footer class="footer" style="margin-left: auto; margin-right: auto;">
<div class="row">
<div class="col-md-6 footer-copyright">
<p class="mb-0">Copyright 2022-23 © IMEI / CEP</p>
</div>
<div class="col-md-6">
<p class="pull-right mb-0">Made by "Hack the box" with <i class="fa fa-heart font-secondary"></i></p>
</div>
</div>

</footer>
</div>
</div>
<?=$this->load->view('Include/base_js','',TRUE);?>



<script src="<?=base_url('public/assets/js/datatable/datatables/jquery.dataTables.min.js')?>"></script>

<script src="<?=base_url('public/assets/js/datatable/datatables/datatable.custom.js')?>"></script>


</body>
</html>