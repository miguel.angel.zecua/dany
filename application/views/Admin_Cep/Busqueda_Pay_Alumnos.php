
<html lang="en">
<head>
<title>Inicio</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Integra-Educa">
<meta name="keywords" content="Integra-Educa, Integra">
<meta name="author" content="Mike">

<?=$this->load->view('Include/base_css','',TRUE);?>
  
<!-- Datepicker -->
<link rel="stylesheet" href="<?php echo base_url('especials/datatables_seach/jquery-ui.css') ?>">
<!-- Datepicker -->

<!-- Datatables -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('especials/datatables_seach/datatables.min.css') ?>" />
<!-- Datatables -->
</head>
<body>

<!-- Loader starts-->
<div class="loader-wrapper">
<div class="theme-loader">    
<div class="loader-p"></div>
</div>
</div>
<!-- Loader ends-->


<!-- page-wrapper Start-->
<div class="page-wrapper" id="pageWrapper">

<!-- Page Header Start-->
<?=$this->load->view('Admin/Header','',TRUE);?>
<!-- Page Header Start-->


<!-- Page Body Start-->
<div class="page-body-wrapper horizontal-menu">
 
<!-- Page Sidebar Inicio-->
<?=$this->load->view('Admin/Side_bar','',TRUE);?>
<!-- Page Sidebar Fin-->

<!-- TITULOS PAGINA-->
<div class="page-body">
<div class="container-fluid">
<div class="page-header">
<div class="row ">
<div class="col-sm-6 ">
<h3>Busqueda de Pagos Alumnos</h3>
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="index.html">Inicio</a></li>
<li class="breadcrumb-item">Home</li>
</ol>
</div>
</div>
</div>
</div>
<!-- TITULOS PAGINA-->

<div class="row justify-content-center mr-2 ml-2">
<div class="col-sm-12 col-xl-12 col-lg-12">
</div>
<span class="badge badge-warning text-dark" style="font-size: 22px; border-radius: 25px;">-Sistema Integral de Información-</span>
</div>

<br>



<!-- Container-fluid starts-->
<div class="container-fluid general-widget">
<div class="row justify-content-center align-items-center">

<!-- Body Principal-->

<div class="row">
<div class="col-md-12">

<div class="row">
<div class="col-md-6">
<div class="input-group mb-3">
<div class="input-group-prepend">
<span class="input-group-text bg-info text-white" id="basic-addon1" style="height:40px; background-color: #3498DB !important; border-radius: 5px;">
<i data-feather="calendar" style="font-size: 28px; color: black; height: 18px;"></i></span>
</div>
<input type="text" class="form-control" id="start_date" placeholder="Fecha de Inicio" readonly>
</div>
</div>
<div class="col-md-6">
<div class="input-group mb-3">
<div class="input-group-prepend">
<span class="input-group-text bg-info text-white" id="basic-addon1" style="height:40px; background-color: #3498DB !important; border-radius: 5px;">
<i data-feather="calendar" style="font-size: 28px; color: black; height: 18px;"></i></span>
</div>
<input type="text" class="form-control" id="end_date" placeholder="Fecha Fin" readonly>
</div>
</div>
</div>

<div>
<button id="filter" class="btn btn-outline-info btn-sm">Filtar</button>
<button id="reset" class="btn btn-outline-warning btn-sm">Reiniciar</button>
</div>


<div class="row mt-3">
<div class="col-md-12" style="background-color: #D0ECE7 !important; border-radius: 25px;">
<!-- Table -->
<div class="table-responsive mt-4" style="border-radius:25px;">
<table class="table table-borderless display nowrap table-danger" id="records" style="width:100%;">
<thead>
<tr>
<th>Nombre</th>
<th>Carrera</th>
<th>Mes 1</th>
<th>Fecha Pago</th>
<th>Tipo de Pago</th>
<th>Estado</th>

<th>Mes 2</th>
<th>Fecha Pago</th>
<th>Tipo de Pago</th>
<th>Estado</th>

<th>Mes 3</th>
<th>Fecha Pago</th>
<th>Tipo de Pago</th>
<th>Estado</th>

<th>Mes 4</th>
<th>Fecha Pago</th>
<th>Tipo de Pago</th>
<th>Estado</th>

<th>Observaciones</th>

</tr>
</thead>
</table>
</div>
</div>
</div>
</div>
</div>


<!-- Body Principal-->

</div>
</div>

<!-- Container-fluid Ends-->
</div>

<!-- footer start-->
<footer class="footer">
<div class="container-fluid">
<div class="row">
<div class="col-md-6 footer-copyright">
<p class="mb-0">Copyright 2022-23 © IMEI / CEP</p>
</div>
<div class="col-md-6">
<p class="pull-right mb-0">Made by "Hack the box" with <i class="fa fa-heart font-secondary"></i></p>
</div>
</div>
</div>
</footer>
</div>
</div>
<?=$this->load->view('Include/base_js','',TRUE);?>






<script src="<?php echo base_url('especials/datatables_seach/all.min.js') ?>"></script>
<!-- Datepicker -->

<!-- Datatables -->
<script type="text/javascript" src="<?php echo base_url('especials/datatables_seach/pdfmake.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('especials/datatables_seach/datatables.min.js') ?>">
</script>
<!-- Momentjs -->
<script src="<?php echo base_url('especials/datatables_seach/moment.min.js') ?>"></script>

<script>
$(function() {
$("#start_date").datepicker({
"dateFormat": "yy-mm-dd"
});
$("#end_date").datepicker({
"dateFormat": "yy-mm-dd"
});
});
</script>


<script>

function fetch(start_date, end_date) {
$.ajax({
url: "Busqueda",
type: "POST",
data: {
start_date: start_date,
end_date: end_date
},
dataType: "json",
success: function(data) {
// Datatables
var i = "1";

$('#records').DataTable({
"data": data,
// buttons
"dom": "<'row'<'col-sm-12 col-md-4'l><'col-sm-12 col-md-4'B><'col-sm-12 col-md-4'f>>" +
"<'row'<'col-sm-12'tr>>" +
"<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
"buttons": [
'copy', 'csv', 'excel', 'pdf', 'print'
],
// responsive
"responsive": true,
"columns": [
{"data": "Nombre",},
{"data": "Carrera"},

{"data": "mes_1",},
{"data": "fecha_1",},
{"data": "tipo_dep_1",},
{"data": "status_1"},

{"data": "mes_2",},
{"data": "fecha_2",},
{"data": "tipo_dep_2",},
{"data": "status_2"},

{"data": "mes_3",},
{"data": "fecha_3",},
{"data": "tipo_dep_3",},
{"data": "status_3"},

{"data": "mes_4",},
{"data": "fecha_4",},
{"data": "tipo_dep_4",},
{"data": "status_4"},

{"data": "observaciones"}

]
});
}
});
}
fetch();

// Filter

$(document).on("click", "#filter", function(e) {
e.preventDefault();

var start_date = $("#start_date").val();
var end_date = $("#end_date").val();

if (start_date == "" || end_date == "") {
alert("both date required");
} else {
$('#records').DataTable().destroy();
fetch(start_date, end_date);
}
});

// Reset

$(document).on("click", "#reset", function(e) {
e.preventDefault();

$("#start_date").val(''); // empty value
$("#end_date").val('');

$('#records').DataTable().destroy();
fetch();
});
</script>

<style type="text/css">
td {
font-weight: bold;
font-size: 13px;
}
</style>



</body>
</html>