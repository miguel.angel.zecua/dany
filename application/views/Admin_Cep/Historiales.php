
<html lang="en">
<head>
<title>Pagos Alumnos</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Integra-Educa">
<meta name="keywords" content="Integra-Educa, Integra">
<meta name="author" content="Mike">

<?=$this->load->view('Include/base_css','',TRUE);?>


<!-- CSS DATATABLES-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('especials/datatables/css/buttons.dataTables.min.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('especials/datatables/css/dataTables.bootstrap5.min.css') ?>">
<!-- CSS DATATABLES-->


</head>
<body>

<!-- Loader starts-->
<div class="loader-wrapper">
<div class="theme-loader">    
<div class="loader-p"></div>
</div>
</div>
<!-- Loader ends-->


<!-- page-wrapper Start-->
<div class="page-wrapper" id="pageWrapper">

<!-- Page Header Start-->
<?=$this->load->view('Admin_Cep/Header','',TRUE);?>
<!-- Page Header Start-->


<!-- Page Body Start-->
<div class="page-body-wrapper horizontal-menu">
 
<!-- Page Sidebar Inicio-->
<?=$this->load->view('Admin_Cep/Side_bar','',TRUE);?>
<!-- Page Sidebar Fin-->

<!-- TITULOS PAGINA-->
<div class="page-body">
<div class="container-fluid">
<div class="page-header">
<div class="row ">
<div class="col-sm-6 ">
<h3>Historiales Alumnos</h3>
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="index.html">Inicio</a></li>
<li class="breadcrumb-item">Home</li>
</ol>
</div>
</div>
</div>
</div>
<!-- TITULOS PAGINA-->




<div class="row justify-content-center mr-2 ml-2">
<div class="col-sm-12 col-xl-12 col-lg-12">
</div>
<span class="badge badge-warning text-dark" style="font-size: 22px; border-radius: 25px;">-Sistema Integral de Información-</span>
</div>

<br>




<!-- Container-fluid starts-->
<div class="container-fluid">

<!-- BODY-->
<div class="row">
    
<div class="col-sm-12 col-xl-12 col-lg-12" style="background-color: #D0ECE7 !important; border-radius: 25px;">


<table id="myTable" class="table table-striped table-bordered mt-4 text-center" style="width:100%; font-size:13px;">
                        
<thead class="text-dark">
<tr role="row">

<th class="sorting" tabindex="0" aria-controls="basic-1" rowspan="1" colspan="1" style="width: 300px;">Nombre</th>

<th class="sorting" tabindex="0" aria-controls="basic-1" rowspan="1" colspan="1"style="width: 35px;">Carrera</th>

<th class="sorting" tabindex="0" aria-controls="basic-1" rowspan="1" colspan="1" style="width: 35px;">Promedio General</th>

<th class="sorting" tabindex="0" aria-controls="basic-1" rowspan="1" colspan="1"style="width: 35px;">Escuela</th>


<th class="sorting" tabindex="0" aria-controls="basic-1" rowspan="1" colspan="1" style="width: 35px;">Operaciones</th>

</tr>

</thead>

<tbody>


<?php foreach ($Historiales->result() as $Historiales){?>


<tr role="row" class="odd">
<td class="sorting_1 text-left" style="font-weight: bold;"><?= $Historiales->Nombre ?></td>
<td class="text-center" style="font-weight: bold;"><?= $Historiales->Carrera ?></td>
<td class="text-center" style="font-weight: bold;"><?= $Historiales->Promedio ?></td>
<td class="text-center" style="font-weight: bold;"><?= $Historiales->escuela ?></td>


<td class="text-center" style="font-weight: bold;">
<a href="Historiales_View/<?= $Historiales->Matricula ?>" class="btn btn-warning btn-lg active" role="button" aria-pressed="true">
<i data-feather="eye" style="font-size: 28px; color: black; height: 18px;"></i></a>
</td>
</tr>


   <?php
}
?>                       

</tbody>

</table>





</div>

</div>





</div>

<!-- BODY-->


</div>


</div>
</div>

<!-- Container-fluid Ends-->
</div>

<script type="text/javascript">
     


</script>




<!-- footer start-->
<footer class="footer" style="margin-left: auto; margin-right: auto;">
<div class="row">
<div class="col-md-6 footer-copyright">
<p class="mb-0">Copyright 2022-23 © IMEI / CEP</p>
</div>
<div class="col-md-6">
<p class="pull-right mb-0">Made by "Hack the box" with <i class="fa fa-heart font-secondary"></i></p>
</div>
</div>

</footer>
</div>
</div>





<?=$this->load->view('Include/base_js','',TRUE);?>


<!-- jquery y bootstrap -->
<script src="https://code.jquery.com/jquery-3.5.1.js"></script> 

<!-- datatables con bootstrap -->
<script src="<?php echo base_url('especials/datatables/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url('especials/datatables/js/dataTables.bootstrap5.min.js') ?>"></script>
<!-- datatables con bootstrap -->

<!-- Para usar los botones -->
<script src="<?php echo base_url('especials/datatables/js/dataTables.buttons.min.js') ?>"></script>
<script src="<?php echo base_url('especials/datatables/js/jszip.min.js') ?>"></script>
<script src="<?php echo base_url('especials/datatables/js/buttons.html5.min.js') ?>"></script>
<!-- Para usar los botones -->

<!-- Para los estilos en Excel-->
<script src="<?php echo base_url('especials/datatables/js/buttons.html5.styles.min.js') ?>"></script>
<script src="<?php echo base_url('especials/datatables/js/buttons.html5.styles.templates.min.js') ?>"></script>
<!-- Para los estilos en Excel-->



<script>
$(document).ready(function () {
$("#myTable").DataTable({
dom: "Bfrtip",
buttons:{
dom: {
button: {
className: 'btn mt-3'
}
},
buttons: [
{
//definimos estilos del boton de excel
extend: "excel",
text:'Exportar a Excel',
className:'btn btn-outline-success',

// 1 - ejemplo básico - uso de templates pre-definidos
//definimos los parametros al exportar a excel

excelStyles: {                
//template: "header_blue",  // Apply the 'header_blue' template part (white font on a blue background in the header/footer)
//template:"green_medium" 

"template": [
"blue_medium",
"header_green",
"title_medium"
] 

},


}
]            
}            
});
});
</script>


</body>
</html>