
<html lang="en">
<head>
<title>Pagos Alumnos</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Integra-Educa">
<meta name="keywords" content="Integra-Educa, Integra">
<meta name="author" content="Mike">

<?=$this->load->view('Include/base_css','',TRUE);?>


<!-- CSS DATATABLES-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('especials/datatables/css/buttons.dataTables.min.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('especials/datatables/css/dataTables.bootstrap5.min.css') ?>">
<!-- CSS DATATABLES-->


</head>
<body>

<!-- Loader starts-->
<div class="loader-wrapper">
<div class="theme-loader">    
<div class="loader-p"></div>
</div>
</div>
<!-- Loader ends-->


<!-- page-wrapper Start-->
<div class="page-wrapper" id="pageWrapper">

<!-- Page Header Start-->
<?=$this->load->view('Admin_Cep/Header','',TRUE);?>
<!-- Page Header Start-->


<!-- Page Body Start-->
<div class="page-body-wrapper horizontal-menu">
 
<!-- Page Sidebar Inicio-->
<?=$this->load->view('Admin_Cep/Side_bar','',TRUE);?>
<!-- Page Sidebar Fin-->

<!-- TITULOS PAGINA-->
<div class="page-body">
<div class="container-fluid">
<div class="page-header">
<div class="row ">
<div class="col-sm-6 ">
<h3>Pagos Alumnos</h3>
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="index.html">Inicio</a></li>
<li class="breadcrumb-item">Home</li>
</ol>
</div>
</div>
</div>
</div>
<!-- TITULOS PAGINA-->




<div class="row justify-content-center mr-2 ml-2">
<div class="col-sm-12 col-xl-12 col-lg-12">
</div>
<span class="badge badge-warning text-dark" style="font-size: 22px; border-radius: 25px;">-Sistema Integral de Información-</span>
</div>

<br>


<!-- MODAL-->
<div class="modal fade bd-example-modal-lg" tabindex="-1" aria-labelledby="myLargeModalLabel"  aria-modal="true" role="dialog" id="modal">
<div class="modal-dialog modal-xl">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title" id="myLargeModalLabel">PAGOS</h4>
<button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close" data-bs-original-title="" title=""></button>
</div>
<div class="modal-body">
<!-- BODY MODAL-->  
<div class="row ">



<!-- FILA 1-->  
<div class="row justify-content-center mt-1">

<label for="staticEmail" class="col-sm-1 col-form-label">Matricula:</label>
<div class="col-sm-5">
<input type="text" readonly class="form-control" id="matricula_alumno" style="text-align: center;" readonly>
</div>
<label for="inputPassword" class="col-sm-1 col-form-label">Nombre:</label>
<div class="col-sm-5">
<input type="text" class="form-control" id="nombre_alumno" style="text-align: center;" readonly>
</div>

</div>
<!-- FILA 1--> 

<!-- FILA 2--> 
<div class="row justify-content-center mt-3">

<label for="staticEmail" class="col-sm-2 col-form-label">Promedio:</label>
<div class="col-sm-2">
<input type="text" readonly class="form-control" id="promedio_alumno" style="text-align: center;" readonly>
</div>
<label for="inputPassword" class="col-sm-1 col-form-label">Turno:</label>
<div class="col-sm-3">
<input type="text" class="form-control" id="turno_alumno" style="text-align: center;" readonly>
</div>
<label for="inputPassword" class="col-sm-1 col-form-label">Carrera:</label>
<div class="col-sm-3">
<input type="text" class="form-control" id="carrera_alumno" style="text-align: center;" readonly>
</div>

</div>
<!-- FILA 2--> 




<div class="row mt-3">



<form onsubmit="enviar_ajax(); return false" id="form1">

<div class=" form-group row d-none">
<label for="staticEmail" class="col-sm-2 col-form-label">Matricula</label>
<div class="col-sm-10">
<input type="text" style="text-align: center;" readonly class="form-control" id="matricula" name="matricula">
</div>
</div>

<hr style="background-color: #008B8B; height: 3px; " >

<!-- DIV 1--> 
<div class=" form-group row">

<div class="col-sm-3">
<label class="form-label"><strong style="color: black;">Pago Sugerido Enero</strong></label>
<div class="input-group">
<input type="text" style="text-align: center;" readonly class="form-control" id="pago_s1" >
</div>
</div>

<div class="col-sm-3">
<label class="form-label"><strong style="color: black;">Pago Enero</strong></label>
<div class="input-group">
<input type="text" style="text-align: center;" name="enero" id="enero" class="form-control" >
</div>
</div>

<div class="col-sm-3">
<label class="form-label"><strong style="color: black;">Fecha</strong></label>
<div class="input-group">
<input class="form-control digits" type="date" id="fecha_1" name="fecha_1">
</div>
</div>

<div class="col-sm-3">
<label class="form-label"><strong style="color: black;">Tipo de pago</strong></label>
<div class="input-group">
<select class="form-select digits" id="tip_1" name="tip_1">
<option value="" selected></option>
<option value="EFECTIVO">EFECTIVO</option>
<option value="DEPOSITO">DEPOSITO</option>
<option value="TRANSFERENCIA">TRANSFERENCIA</option>
<option value="MIXTO">MIXTO</option>
</select>
</div>
</div>


<div class="col-sm-12 d-none">
<label class="form-label">Status</label>
<div class="input-group">
<input type="text" style="text-align: center;" name="status_1" id="status_1" class="form-control" >
</div>
</div>


</div>
<!-- DIV 1--> 

<hr style="background-color: #008B8B; height: 3px; " >


<!-- DIV 2--> 
<div class=" form-group row">

<div class="col-sm-3">
<label class="form-label"><strong style="color: black;">Pago Sugerido Febrero:</strong></label>
<div class="input-group">
<input type="text" style="text-align: center;" readonly class="form-control" id="pago_s2">
</div>
</div>

<div class="col-sm-3">
<label class="form-label"><strong style="color: black;">Pago Febrero:</strong></label>
<div class="input-group">
<input type="text" style="text-align: center;" name="febrero" id="febrero" class="form-control">
</div>
</div>

<div class="col-sm-3">
<label class="form-label"><strong style="color: black;">Fecha</strong></label>
<div class="input-group">
<input class="form-control digits" type="date" id="fecha_2" name="fecha_2">
</div>
</div>

<div class="col-sm-3">
<label class="form-label"><strong style="color: black;">Tipo de pago</strong></label>
<div class="input-group">
<select class="form-select digits" id="tip_2" name="tip_2">
<option value="" selected></option>
<option value="EFECTIVO">EFECTIVO</option>
<option value="DEPOSITO">DEPOSITO</option>
<option value="TRANSFERENCIA">TRANSFERENCIA</option>
</select>
</div>
</div>


<div class="col-sm-12 d-none">
<label class="form-label"><strong style="color: black;">Status</strong></label>
<div class="input-group">
<input type="text" style="text-align: center;" name="status_2" id="status_2" class="form-control" >
</div>
</div>


</div>
<!-- DIV 2--> 

<hr style="background-color: #008B8B; height: 3px; " >


<!-- DIV 3--> 
<div class=" form-group row">

<div class="col-sm-3">
<label class="form-label"><strong style="color: black;">Pago Sugerido Marzo:</strong></label>
<div class="input-group">
<input type="text" style="text-align: center;" readonly class="form-control" id="pago_s3">
</div>
</div>

<div class="col-sm-3">
<label class="form-label"><strong style="color: black;">Pago Marzo:</strong></label>
<div class="input-group">
<input type="text" style="text-align: center;" name="marzo" id="marzo" class="form-control">
</div>
</div>

<div class="col-sm-3">
<label class="form-label"><strong style="color: black;">Fecha</strong></label>
<div class="input-group">
<input class="form-control digits" type="date" id="fecha_3" name="fecha_3">
</div>
</div>

<div class="col-sm-3">
<label class="form-label"><strong style="color: black;">Tipo de pago</strong></label>
<div class="input-group">
<select class="form-select digits" id="tip_3" name="tip_3">
<option value="" selected></option>
<option value="EFECTIVO">EFECTIVO</option>
<option value="DEPOSITO">DEPOSITO</option>
<option value="TRANSFERENCIA">TRANSFERENCIA</option>
</select>
</div>
</div>

<div class="col-sm-12 d-none">
<label class="form-label"><strong style="color: black;">Status</strong></label>
<div class="input-group">
<input type="text" style="text-align: center;" name="status_3" id="status_3" class="form-control" >
</div>
</div>


</div>
<!-- DIV 3--> 

<hr style="background-color: #008B8B; height: 3px; " >


<!-- DIV 4--> 
<div class=" form-group row">

<div class="col-sm-3">
<label class="form-label"><strong style="color: black;">Pago Sugerido Abril:</strong></label>
<div class="input-group">
<input type="text" style="text-align: center;" readonly class="form-control" id="pago_s4">
</div>
</div>

<div class="col-sm-3">
<label class="form-label"><strong style="color: black;">Pago Abril:</strong></label>
<div class="input-group">
<input type="text" style="text-align: center;" name="abril" id="abril" class="form-control">
</div>
</div>

<div class="col-sm-3">
<label class="form-label"><strong style="color: black;">Fecha</strong></label>
<div class="input-group">
<input class="form-control digits" type="date" id="fecha_4" name="fecha_4">
</div>
</div>

<div class="col-sm-3">
<label class="form-label"><strong style="color: black;">Tipo de pago</strong></label>
<div class="input-group">
<select class="form-select digits" id="tip_4" name="tip_4">
<option value="" selected></option>
<option value="EFECTIVO">EFECTIVO</option>
<option value="DEPOSITO">DEPOSITO</option>
<option value="TRANSFERENCIA">TRANSFERENCIA</option>
</select>
</div>
</div>

<div class="col-sm-12 d-none">
<label class="form-label"><strong style="color: black;">Status</strong></label>
<div class="input-group">
<input type="text" style="text-align: center;" name="status_4" id="status_4" class="form-control" >
</div>
</div>


</div>
<!-- DIV 4--> 

<hr style="background-color: #008B8B; height: 3px; " >


<!-- DIV 5--> 
<div class=" form-group row">

<div class="col-sm-3">
<label class="form-label"><strong style="color: black;">Pago Sugerido Reinscripción:</strong></label>
<div class="input-group">
<input type="text" style="text-align: center;" readonly class="form-control" id="pago_s5">
</div>
</div>

<div class="col-sm-3">
<label class="form-label"><strong style="color: black;">Pago Reinscripción:</strong></label>
<div class="input-group">
<input type="text" style="text-align: center;" name="reinscripcion" id="reinscripcion" class="form-control">
</div>
</div>

<div class="col-sm-3">
<label class="form-label"><strong style="color: black;">Fecha</strong></label>
<div class="input-group">
<input class="form-control digits" type="date" id="fecha_5" name="fecha_5">
</div>
</div>

<div class="col-sm-3">
<label class="form-label"><strong style="color: black;">Tipo de pago</strong></label>
<div class="input-group">
<select class="form-select digits" id="tip_5" name="tip_5">
<option value="" selected></option>
<option value="EFECTIVO">EFECTIVO</option>
<option value="DEPOSITO">DEPOSITO</option>
<option value="TRANSFERENCIA">TRANSFERENCIA</option>
</select>
</div>
</div>

<div class="col-sm-12 d-none">
<label class="form-label">Status</label>
<div class="input-group">
<input type="text" style="text-align: center;" name="status_5" id="status_5" class="form-control" >
</div>
</div>


</div>
<!-- DIV 5--> 

<hr style="background-color: #008B8B; height: 3px; " >

<div class="col-sm-12 ">
<label class="form-label"><strong style="color: black;">Observaciones:</strong></label>
<div class="input-group">
<input type="text" style="text-align: center;" name="Observaciones" id="Observaciones" class="form-control" >
</div>
</div>


</div>
<div class="row">
<button class="btn btn-danger" type="submit" style="border-radius: 25px;">GUARDAR</button>
</div>

</div>
</form>

<!-- BODY MODAL--> 
</div>
</div>
</div>
</div>
<!-- MODAL-->



<!-- Container-fluid starts-->
<div class="container-fluid">

<!-- BODY-->
<div class="row">
    
<div class="col-sm-12 col-xl-12 col-lg-12" style="background-color: #D0ECE7 !important; border-radius: 25px;">


<table id="myTable" class="table table-striped table-bordered mt-4 " style="width:100%">
                        
<thead class="text-dark">
<tr role="row">

<th class="sorting" tabindex="0" aria-controls="basic-1" rowspan="1" colspan="1" style="width: 300px;">Nombre</th>

<th class="sorting" tabindex="0" aria-controls="basic-1" rowspan="1" colspan="1"style="width: 35px;">Mes 1</th>

<th class="sorting" tabindex="0" aria-controls="basic-1" rowspan="1" colspan="1" style="width: 35px;">Mes 2</th>

<th class="sorting" tabindex="0" aria-controls="basic-1" rowspan="1" colspan="1"style="width: 35px;">Mes 3</th>

<th class="sorting" tabindex="0" aria-controls="basic-1" rowspan="1" colspan="1" style="width: 35px;">Mes 4</th>

<th class="sorting" tabindex="0" aria-controls="basic-1" rowspan="1" colspan="1" style="width: 35px;">Reinscripción </th>

<th class="sorting" tabindex="0" aria-controls="basic-1" rowspan="1" colspan="1" style="width: 35px;">Operaciones</th>

</tr>

</thead>

<tbody>

<?php foreach ($pagos->result() as $pagos){?>



<tr role="row" class="odd">
<td class="sorting_1 text-left" style="font-weight: bold; font-size: 14px;"><?= $pagos->nombre ?></td>
<td class="text-center" style="font-weight: bold; font-size: 14px;"><?= $pagos->mes_1 ?></td>
<td class="text-center" style="font-weight: bold; font-size: 14px;"><?= $pagos->mes_2 ?></td>
<td class="text-center" style="font-weight: bold; font-size: 14px;"><?= $pagos->mes_3 ?></td>
<td class="text-center" style="font-weight: bold; font-size: 14px;"><?= $pagos->mes_4 ?></td>
<td class="text-center" style="font-weight: bold; font-size: 14px;"><?= $pagos->reinscripcion ?></td>

<td class="text-center" style="font-weight: bold;"><button onclick="Pay('<?= $pagos->Matricula_alumno ?>')" class="btn btn-warning"><i data-feather="eye" style="font-size: 28px; color: black; height: 18px;"></i></button></td>
</tr>

<?php
}
?>
                          

</tbody>

</table>





</div>

</div>





</div>

<!-- BODY-->


</div>


</div>
</div>

<!-- Container-fluid Ends-->
</div>

<script type="text/javascript">
     
function Pay(id) 
{

$.post("GetPagos", {
id: id
},
function (data) {
var myArray = JSON.parse(data);
console.log(myArray);
var pago_1;
var pago_2;
var pago_3;
var pago_4;
var pago_5;


var conafe=myArray.datos.Conafe;
$("#matricula_alumno").val(myArray.datos.Matricula_alumno);
$("#nombre_alumno").val(myArray.datos.nombre);
$("#promedio_alumno").val(myArray.promedio.promedio);
$("#turno_alumno").val(myArray.datos.Turno);
$("#carrera_alumno").val(myArray.datos.carrera);

$("#fecha_1").val(myArray.datos.fecha_1);
$("#fecha_2").val(myArray.datos.fecha_2);
$("#fecha_3").val(myArray.datos.fecha_3);
$("#fecha_4").val(myArray.datos.fecha_4);
$("#fecha_5").val(myArray.datos.fecha_5);


$("#tip_1").val(myArray.datos.tipo_dep_1);
$("#tip_2").val(myArray.datos.tipo_dep_2);
$("#tip_3").val(myArray.datos.tipo_dep_3);
$("#tip_4").val(myArray.datos.tipo_dep_4);
$("#tip_5").val(myArray.datos.tipo_dep_5);

$("#status_1").val(myArray.datos.status_1);
$("#status_2").val(myArray.datos.status_2);
$("#status_3").val(myArray.datos.status_3);
$("#status_4").val(myArray.datos.status_4);
$("#status_5").val(myArray.datos.status_5);

$("#Observaciones").val(myArray.datos.observaciones);




if(myArray.datos.status_1==1){pago_1="PAGADO";$("#enero").prop("readonly",true);}
else{pago_1=name2($("#carrera_alumno").val(),$("#turno_alumno").val(),conafe);
$("#enero").prop("readonly",false);
}

if(myArray.datos.status_2==1){pago_2="PAGADO";$("#febrero").prop("readonly",true);}
else{pago_2=name3($("#carrera_alumno").val(),$("#turno_alumno").val(),conafe);
$("#febrero").prop("readonly",false);
}

if(myArray.datos.status_3==1){pago_3="PAGADO";$("#marzo").prop("readonly",true);}
else{pago_3=name4($("#carrera_alumno").val(),$("#turno_alumno").val(),conafe);
$("#marzo").prop("readonly",false);
}

if(myArray.datos.status_4==1){pago_4="PAGADO";$("abril").prop("readonly",true);}
else{pago_4=name5($("#carrera_alumno").val(),$("#turno_alumno").val(),conafe);
$("#abril").prop("readonly",false);
}

if(myArray.datos.status_5==1){pago_5="PAGADO";$("#reinscripcion").prop("readonly",true);}
else{pago_5=promedio(myArray.promedio.promedio);
$("#reinscripcion").prop("readonly",false);
}



$("#pago_s1").val(pago_1);
$("#pago_s2").val(pago_2);
$("#pago_s3").val(pago_3);
$("#pago_s4").val(pago_4);
$("#pago_s5").val(pago_5);

$("#matricula").val(myArray.datos.Matricula_alumno);
$("#enero").val(myArray.datos.mes_1);
$("#febrero").val(myArray.datos.mes_2);
$("#marzo").val(myArray.datos.mes_3);
$("#abril").val(myArray.datos.mes_4);
$("#reinscripcion").val(myArray.datos.reinscripcion);

//alert(user.post[0].nombre)
}
);

$('#modal').modal('show');

}

</script>


<script>
function enviar_ajax(){ 

$.ajax({
type: 'POST',
url: 'Update_pagos',
data: $('#form1').serialize(),
success: function() {
Swal.fire('Guardado');
$('#modal').modal('toggle');
}
});
}
</script>



<script type="text/javascript">
    

$("#enero").keyup(function(){
var ref_1 = $("#pago_s1").val();
var actual_1 = $("#enero").val();
if(actual_1>=ref_1){$("#status_1").val("1");}
else{$("#status_1").val("0");}
});

$("#febrero").keypress(function(){
var ref_2 = $("#pago_s2").val();
var actual_2 = $("#enero").val();
if(actual_2>=ref_2){$("#status_2").val("1");}
else{$("#status_1").val("0");}
});

$("#marzo").keypress(function(){
var ref_3 = $("#pago_s3").val();
var actual_3 = $("#febrero").val();
if(actual_3>=ref_3){$("#status_3").val("1");}
else{$("#status_1").val("0");}
});

$("#abril").keypress(function(){
var ref_4 = $("#pago_s4").val();
var actual_4 = $("#abril").val();
if(actual_4>=ref_4){$("#status_4").val("1");}
else{$("#status_1").val("0");}
});

$("#reinscripcion").keypress(function(){
var ref_5 = $("#pago_s5").val();
var actual_5 = $("#reinscripcion").val();
if(actual_1>=ref_1){$("#status_5").val("1");}
else{$("#status_1").val("0");}
});


</script>




<?=$this->load->view('Admin_Cep/Scripts','',TRUE);?>



<!-- footer start-->
<footer class="footer" style="margin-left: auto; margin-right: auto;">
<div class="row">
<div class="col-md-6 footer-copyright">
<p class="mb-0">Copyright 2022-23 © IMEI / CEP</p>
</div>
<div class="col-md-6">
<p class="pull-right mb-0">Made by "Hack the box" with <i class="fa fa-heart font-secondary"></i></p>
</div>
</div>

</footer>
</div>
</div>





<?=$this->load->view('Include/base_js','',TRUE);?>








 <!-- jquery y bootstrap -->
 <script src="https://code.jquery.com/jquery-3.5.1.js"></script> 

<!-- datatables con bootstrap -->
 <script src="<?php echo base_url('especials/datatables/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url('especials/datatables/js/dataTables.bootstrap5.min.js') ?>"></script>
<!-- datatables con bootstrap -->

<!-- Para usar los botones -->
<script src="<?php echo base_url('especials/datatables/js/dataTables.buttons.min.js') ?>"></script>
<script src="<?php echo base_url('especials/datatables/js/jszip.min.js') ?>"></script>
<script src="<?php echo base_url('especials/datatables/js/buttons.html5.min.js') ?>"></script>
<!-- Para usar los botones -->

<!-- Para los estilos en Excel-->
<script src="<?php echo base_url('especials/datatables/js/buttons.html5.styles.min.js') ?>"></script>
<script src="<?php echo base_url('especials/datatables/js/buttons.html5.styles.templates.min.js') ?>"></script>
<!-- Para los estilos en Excel-->



<script>
$(document).ready(function () {
$("#myTable").DataTable({
dom: "Bfrtip",
buttons:{
dom: {
button: {
className: 'btn mt-3'
}
},
buttons: [
{
//definimos estilos del boton de excel
extend: "excel",
text:'Exportar a Excel',
className:'btn btn-outline-success',

// 1 - ejemplo básico - uso de templates pre-definidos
//definimos los parametros al exportar a excel

excelStyles: {                
//template: "header_blue",  // Apply the 'header_blue' template part (white font on a blue background in the header/footer)
//template:"green_medium" 

"template": [
"blue_medium",
"header_green",
"title_medium"
] 

},


}
]            
}            
});
});
</script>

<script type="text/javascript">
	
//var table = $('#myTable').DataTable();

//clear datatable
//table.clear().draw();

//destroy datatable
//table.destroy();

</script>






<script src="<?php echo base_url('library/especials/sweetalert.js') ?>"></script>

</body>
</html>