
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="viho admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
<meta name="keywords" content="admin template, viho admin template, dashboard template, flat admin template, responsive admin template, web app">
<meta name="author" content="pixelstrap">

<?=$this->load->view('Include/base_css','',TRUE);?>
<!-- Head de imei-->
<!-- CSS -->
<?=$this->load->view('Include/Head','',TRUE);?>
<!-- CSS -->
<!-- Head de imei-->

</head>
<body>

<!-- Loader starts-->
<div class="loader-wrapper">
<div class="theme-loader">    
<div class="loader-p"></div>
</div>
</div>
<!-- Loader ends-->


<!-- page-wrapper Start-->
<div class="page-wrapper" id="pageWrapper">

<!-- Page Header Start-->
<?=$this->load->view('Alumnos/Header','',TRUE);?>
<!-- Page Header Start-->


<!-- Page Body Start-->
<div class="page-body-wrapper horizontal-menu">
 
<!-- Page Sidebar Inicio-->
<?=$this->load->view('Profesores/Side_bar','',TRUE);?>
<!-- Page Sidebar Fin-->

<!-- TITULOS PAGINA-->
<div class="page-body">
<div class="container-fluid">
<div class="page-header">
<div class="row ">
<div class="col-sm-6 ">
<h3>Inicio</h3>

</div>
</div>
</div>
</div>
<!-- TITULOS PAGINA-->

<br>



<!-- Container-fluid starts-->
<div class="container-fluid general-widget">
<div class="row justify-content-center d-flex align-items-center">

<hr>
<br>
<!-- Codigo de IMEI -->

<!-- CONTENEDOR-->	
<div class="xs-pd-20-10 pd-ltr-20">


<!-- FILA DE CUADROS-->	
<div class="row pb-10 justify-content-center d-none">

<!-- CUADRO 1 -->
<div class="col-xl-3 col-lg-3 col-md-6 mb-20">
<div class="card-box height-100-p widget-style3">

<div class="d-flex flex-wrap">
<div class="widget-data">
<div class="weight-700 font-24 text-dark">Fecha</div>
<div class="font-14 text-secondary weight-500">Icon</div>
</div>

<div class="widget-icon">
<div class="icon" data-color="#00eccf"><i class="icon-copy dw dw-calendar1"></i></div>
</div>

</div>
</div>
</div>
<!-- CUADRO 1 -->

<!-- CUADRO 2 -->
<div class="col-xl-3 col-lg-3 col-md-6 mb-20">
<div class="card-box height-100-p widget-style3">

<div class="d-flex flex-wrap">
<div class="widget-data">
<div class="weight-700 font-24 text-dark">Hora</div>
<div class="font-14 text-secondary weight-500">Icon</div>
</div>

<div class="widget-icon">
<div class="icon" data-color="#ff5b5b"><span class="icon-copy dw dw-wall-clock"></span></div>
</div>

</div>
</div>
</div>
<!-- CUADRO 2 -->

<!-- CUADRO 3 -->
<div class="col-xl-3 col-lg-3 col-md-6 mb-20">
<div class="card-box height-100-p widget-style3">

<div class="d-flex flex-wrap">
<div class="widget-data">
<div class="weight-700 font-24 text-dark">Inicio</div>
<div class="font-14 text-secondary weight-500">Icon</div>
</div>

<div class="widget-icon">
<div class="icon" data-color="#09cc06"><span class="icon-copy dw dw-school"></span></div>
</div>

</div>
</div>
</div>
<!-- CUADRO 3 -->				

</div>
<!-- FILA DE CUADROS-->	

<div class="title pb-20 pt-20 text-center">
<h2 class="h3 mb-1">Acciones rápidas</h2>
</div>

<!-- FILA DE CARDS-->	
<div class="row justify-content-center">

<!-- CARD 1-->		
<div class="col-md-4 mb-20 conten">
<a href="<?=base_url('Profesores/Calificaciones')?>" class="card-box d-block mx-auto pd-20 text-secondary">

<div class="img pb-30">
<img class="rounded" src="<?=base_url('library/vendors/images/4.png')?>" style="border-radius: 100px;">
</div>

<div class="content"> <a href="<?=base_url('Profesores_2/Calificaciones')?>"> 
<h3 class="h4 text-center"><span class="badge badge-pill badge-success text-dark"><strong style="font-size: 1.20rem;">CALIFICACIONES</strong></span></h3>
<p class="max-width-200 d-none">Descripcion</p>
</a>
</div>

</a>
</div>
<!-- CARD 1-->

<!-- CARD 2-->
<div class="col-md-4 mb-20 conten">
<a href="<?=base_url('Profesores/Horario')?>" class="card-box d-block mx-auto pd-20 text-secondary">

<div class="img pb-30">
<img class="rounded" src="<?=base_url('library/vendors/images/5.png')?>" style="border-radius: 100px;">
</div>

<div class="content"> <a href="<?=base_url('Profesores_2/Horarios')?>">
<h3 class="h4 text-center"><span class="badge badge-pill badge-warning text-dark"><strong style="font-size: 1.20rem;">HORARIOS ACTUALES</strong></span></h3>
<p class="max-width-200 d-none">Descripcion</p>
</div>

</a>
</div>
<!-- CARD 2-->



</div>
<!-- FILA DE CARDS-->	

<!-- FOOTER-->	
<div class="footer-wrap pd-20 mb-20 card-box d-none">Power by Mike</div>
<!-- FOOTER-->	

</div>

<!-- CONTENEDOR-->

<!-- Codigo de IMEI -->


</div>
</div>

<!-- Container-fluid Ends-->
</div>

<!-- footer start-->
<footer class="footer">
<div class="container-fluid">
<div class="row">
<div class="col-md-6 footer-copyright">
<p class="mb-0">Copyright 2022-23 © IMEI / CEP</p>
</div>
<div class="col-md-6">
<p class="pull-right mb-0">Made by "Hack the box" with <i class="fa fa-heart font-secondary"></i></p>
</div>
</div>
</div>
</footer>
</div>
</div>
<?=$this->load->view('Include/base_js','',TRUE);?>

<!-- js y estilos -->
<style type="text/css">
	

.conten:hover {
-webkit-transform:scale(1.0);
transition: 0.5s;
transform:scale(1.2);}
.conten {overflow:hidden;}


</style>

<!-- JS -->
<?=$this->load->view('Include/Js','',TRUE);?>
<!-- JS -->
<!-- js y estilos -->

</body>
</html>