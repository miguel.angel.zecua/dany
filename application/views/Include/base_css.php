    <link rel="icon" href="<?php echo base_url('public/assets/images/logo/Log2.png'); ?>" type="image/x-icon">
    <link rel="shortcut icon" href="<?php echo base_url('public/assets/images/logo/Log2.png'); ?>" type="image/x-icon">

    <!-- Google font-->


<!-- latest jquery-->

    <!-- feather icon js-->

    <!-- Font Awesome-->

<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/css/fontawesome.css') ?>">

    <!-- ico-font-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/css/icofont.css') ?>">
    <!-- Themify icon-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/css/themify.css') ?>">
    <!-- Flag icon-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/css/flag-icon.css') ?>">    

    <!-- Feather icon-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/css/feather-icon.css') ?>">
    <!-- Plugins css start-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/css/date-picker.css') ?>">
    
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/css/owlcarousel.css') ?>">
    
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/css/prism.css') ?>">
   
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/css/whether-icon.css') ?>">
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/css/bootstrap.css') ?>">
    <!-- App css-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/css/style.css') ?>">
    
<link id="color" rel="stylesheet" href="<?php echo base_url('public/assets/css/color-1.css') ?>" media="screen">
    <!-- Responsive css-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/css/responsive.css') ?>">

<script src="<?php echo base_url('public/assets/js/jquery-3.5.1.min.js') ?>"></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/css/tabla_excel.css') ?>">