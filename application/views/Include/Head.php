<script src="<?=base_url('library/src/scripts/jquery.js')?>"></script>
<!-- ICONOS -->
<link rel="apple-touch-icon" sizes="180x180" href="<?=base_url('library/vendors/images/apple-touch-icon.png')?>">
<link rel="icon" type="image/png" sizes="32x32" href="<?=base_url('library/vendors/images/favicon-32x32.png')?>">
<link rel="icon" type="image/png" sizes="16x16" href="<?=base_url('library/vendors/images/favicon-16x16.png')?>">
<!-- ICONOS -->

<!-- MOBILE SPECIFIC METAS -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- MOBILE SPECIFIC METAS -->

<!-- GOOGLE FONT -->
<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
<!-- GOOGLE FONT -->

<!-- CSS -->
<link rel="stylesheet" type="text/css" href="<?=base_url('library/vendors/styles/core.css')?>">
<link rel="stylesheet" type="text/css" href="<?=base_url('library/vendors/styles/icon-font.min.css')?>">
<link rel="stylesheet" type="text/css" href="<?=base_url('library/src/plugins/datatables/css/dataTables.bootstrap4.min.css')?>">
<link rel="stylesheet" type="text/css" href="<?=base_url('library/src/plugins/datatables/css/responsive.bootstrap4.min.css')?>">
<link rel="stylesheet" type="text/css" href="<?=base_url('library/vendors/styles/style.css')?>">
<!-- CSS -->


<!-- GOOGLE ANALYTICS -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119386393-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-119386393-1');
</script>
<!-- GOOGLE ANALYTICS -->

<script type="text/javascript" src="<?=base_url('library/src/scripts/html2pdf.bundle.min.js')?>"></script>



