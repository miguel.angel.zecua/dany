
<html lang="en">
<head>
<title>HORARIOS</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Integra-Educa">
<meta name="keywords" content="Integra-Educa, Integra">
<meta name="author" content="Mike">

<?=$this->load->view('Include/base_css','',TRUE);?>


</head>
<body>

<!-- Loader starts-->
<div class="loader-wrapper">
<div class="theme-loader">    
<div class="loader-p"></div>
</div>
</div>
<!-- Loader ends-->


<!-- page-wrapper Start-->
<div class="page-wrapper" id="pageWrapper">

<!-- Page Header Start-->
<?=$this->load->view('Profesores/Header','',TRUE);?>
<!-- Page Header Start-->


<!-- Page Body Start-->
<div class="page-body-wrapper horizontal-menu">
 
<!-- Page Sidebar Inicio-->
<?=$this->load->view('Profesores/Side_bar','',TRUE);?>
<!-- Page Sidebar Fin-->

<!-- TITULOS PAGINA-->
<div class="page-body">
<div class="container-fluid">
<div class="page-header">
<div class="row ">
<div class="col-sm-6 ">
<h3>Horario</h3>
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="<?=base_url('Profesores/')?>">Inicio</a></li>
<li class="breadcrumb-item">Home</li>
</ol>
</div>
</div>
</div>
</div>
<!-- TITULOS PAGINA-->
<div class="row justify-content-center mr-2 ml-2">
<div class="col-sm-12 col-xl-12 col-lg-12">
</div>
<span class="badge badge-warning text-dark" style="font-size: 22px; border-radius: 25px;">-Sistema Integral de Información-</span>
</div>

<br>



<!-- Container-fluid starts-->
<div class="container-fluid">
<div class="row justify-content-center">
<div class="table-responsive" style="border-radius: 25px;">
<?php 

echo '<table class="table table-striped table-bordered"> <!-- Lo cambiaremos por CSS -->
<thead class="table-dark">
		
<tr>
<th style="width: 7%" class="text-white">Dia</th>
<th style="width: 20%" class="text-center text-white">Hora</th>
<th style="width: 10%" class="text-center text-white">Cuatrimestre</th>
<th style="width: 15%" class="text-center text-white">Carrera</th>
<th style="width: 35%" class="text-center text-white">Materia</th>
<th class="text-center text-white">Turno</th>
</tr>

</thead>';


foreach ($row->result() as $row)
{

if($row->Dia=='LUNES')
{
echo '
<tr class="table-warning">
<td style="padding: 0.6rem"><strong>'.$row->Dia.'</strong></td>
<td style="padding: 0.6rem"><strong>'.$row->Hora_Inicio .' - '.$row->Hora_Fin.'</strong></td>
<td style="padding: 0.6rem"><strong>'.$row->Cuatrimestre .'</strong></td>
<td style="padding: 0.6rem"><strong>'.$row->Nombre_carrera .'</strong></td>
<td style="padding: 0.6rem"><strong>'.$row->nombre.'</strong></td>
<td style="padding: 0.6rem"><strong>'.$row->Turno.'</strong></td>
</tr>
<tr>
<td style="padding: 0.6rem"></td>
<td style="padding: 0.6rem"></td>
<td style="padding: 0.6rem"></td>
<td style="padding: 0.6rem"></td>
</tr>
';
}

if($row->Dia=='MARTES')
{
echo '
<tr class="table-primary">
<td style="padding: 0.6rem"><strong>'.$row->Dia.'</strong></td>
<td style="padding: 0.6rem"><strong>'.$row->Hora_Inicio .' - '.$row->Hora_Fin.'</strong></td>
<td style="padding: 0.6rem"><strong>'.$row->Cuatrimestre .'</strong></td>
<td style="padding: 0.6rem"><strong>'.$row->Nombre_carrera .'</strong></td>
<td style="padding: 0.6rem"><strong>'.$row->nombre.'</strong></td>
<td style="padding: 0.6rem"><strong>'.$row->Turno.'</strong></td>
</tr>
<tr>
<td style="padding: 0.6rem"></td>
<td style="padding: 0.6rem"></td>
<td style="padding: 0.6rem"></td>
<td style="padding: 0.6rem"></td>
</tr>
';
}

if($row->Dia=='MI?RCOLES' || $row->Dia=='MIERCOLES')
{
echo '
<tr class="table-primary">
<td style="padding: 0.6rem"><strong>'.$row->Dia.'</strong></td>
<td style="padding: 0.6rem"><strong>'.$row->Hora_Inicio .' - '.$row->Hora_Fin.'</strong></td>
<td style="padding: 0.6rem"><strong>'.$row->Cuatrimestre .'</strong></td>
<td style="padding: 0.6rem"><strong>'.$row->Nombre_carrera .'</strong></td>
<td style="padding: 0.6rem"><strong>'.$row->nombre.'</strong></td>
<td style="padding: 0.6rem"><strong>'.$row->Turno.'</strong></td>
</tr>
<tr>
<td style="padding: 0.6rem"></td>
<td style="padding: 0.6rem"></td>
<td style="padding: 0.6rem"></td>
<td style="padding: 0.6rem"></td>
</tr>
';
}

if($row->Dia=='JUEVES')
{
echo '
<tr class="table-danger">
<td style="padding: 0.6rem"><strong>'.$row->Dia.'</strong></td>
<td style="padding: 0.6rem"><strong>'.$row->Hora_Inicio .' - '.$row->Hora_Fin.'</strong></td>
<td style="padding: 0.6rem"><strong>'.$row->Cuatrimestre .'</strong></td>
<td style="padding: 0.6rem"><strong>'.$row->Nombre_carrera .'</strong></td>
<td style="padding: 0.6rem"><strong>'.$row->nombre.'</strong></td>
<td style="padding: 0.6rem"><strong>'.$row->Turno.'</strong></td>
</tr>
<tr>
<td style="padding: 0.6rem"></td>
<td style="padding: 0.6rem"></td>
<td style="padding: 0.6rem"></td>
<td style="padding: 0.6rem"></td>
</tr>
';
}

if($row->Dia=='VIERNES')
{
echo '
<tr class="table-info">
<td style="padding: 0.6rem"><strong>'.$row->Dia.'</strong></td>
<td style="padding: 0.6rem"><strong>'.$row->Hora_Inicio .' - '.$row->Hora_Fin.'</strong></td>
<td style="padding: 0.6rem"><strong>'.$row->Cuatrimestre .'</strong></td>
<td style="padding: 0.6rem"><strong>'.$row->Nombre_carrera .'</strong></td>
<td style="padding: 0.6rem"><strong>'.$row->nombre.'</strong></td>
<td style="padding: 0.6rem"><strong>'.$row->Turno.'</strong></td>
</tr>
<tr>
<td style="padding: 0.6rem"></td>
<td style="padding: 0.6rem"></td>
<td style="padding: 0.6rem"></td>
<td style="padding: 0.6rem"></td>
</tr>
';
}

if($row->Dia=='SABADO' || $row->Dia=='SABATINO')
{
echo '
<tr class="table-danger">
<td style="padding: 0.6rem"><strong>'.$row->Dia.'</strong></td>
<td style="padding: 0.6rem"><strong>'.$row->Hora_Inicio .' - '.$row->Hora_Fin.'</strong></td>
<td style="padding: 0.6rem"><strong>'.$row->Cuatrimestre .'</strong></td>
<td style="padding: 0.6rem"><strong>'.$row->Nombre_carrera .'</strong></td>
<td style="padding: 0.6rem"><strong>'.$row->nombre.'</strong></td>
<td style="padding: 0.6rem"><strong>'.$row->Turno.'</strong></td>
</tr>
<tr>
<td style="padding: 0.6rem"></td>
<td style="padding: 0.6rem"></td>
<td style="padding: 0.6rem"></td>
<td style="padding: 0.6rem"></td>
</tr>
';
}

}

echo '</table>';


?>

</div>

</div>

<!-- Container-fluid Ends-->
</div>
</div>

<!-- footer start-->
<footer class="footer" style="margin-left: auto; margin-right: auto;">
<div class="row">
<div class="col-md-6 footer-copyright">
<p class="mb-0">Copyright 2022-23 © IMEI / CEP</p>
</div>
<div class="col-md-6">
<p class="pull-right mb-0">Made by "Hack the box" with <i class="fa fa-heart font-secondary"></i></p>
</div>
</div>

</footer>
</div>

<?=$this->load->view('Include/base_js','',TRUE);?>

</body>
</html>