<!-- Page Sidebar Start-->
<header class="main-nav">


<div class="sidebar-user text-center" id="sd">
<img class="img-90 rounded-circle mt-0" src="<?php echo base_url('public/assets/images/dashboard/1.png');?>" alt="">
<div class="badge-bottom"><span class="badge badge-primary">New</span></div><a href="user-profile.html">
<h6 class="mt-3 f-14 f-w-600"><?php 

if(empty($side_bar->Nombres)==true)
{
	echo "Temporal";
}
else{
	echo $side_bar->Nombres;
}
 ?></h6></a>
<p class="mb-0 font-roboto"><?php 
if(empty($Matricula_Profesor)==true)
{
	echo "-";
}
else{
	echo $side_bar->Matricula_Profesor;
}

 ?></p>
<ul>
<li><span class="counter"><?php 

if(is_null($side_bar->Salones)==true)
{
	echo "0";
}
else{
	echo $side_bar->Salones;
}

 ?></span>
<p>No. Salones</p>
</li>
<li><span class="counter"><?php echo $side_bar->Materias; ?></span>
<p>No. Materias</p>
</li>
<li><span class="counter"><?php echo $side_bar->Alumnos; ?></span>
<p>No. Alumnos</p>
</li>
</ul>
</div>



<nav>
<div class="main-navbar">
<div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
<div id="mainnav">           
<ul class="nav-menu custom-scrollbar">
<li class="back-btn">
<div class="mobile-back text-end"><span>REGRESAR</span><i class="fa fa-angle-right ps-2" aria-hidden="true"></i></div>
</li>
<li class="sidebar-main-title mt-0">
<div>
<h6 class="text-center mt-0"><strong>GENERAL</strong></h6>
</div>
</li>

<!-- Menu 1-->
<li class="dropdown"><a class="nav-link menu-title" href="javascript:void(0)"><i data-feather="home"></i><span>Comienzo</span></a>
<ul class="nav-submenu menu-content">
<li><a href="<?=base_url('Profesores/')?>">Inicio</a></li>
<li><a href="<?=base_url ('Profesores/Datos_personales')?>">Datos personales</a></li>
<li><a href="<?=base_url('Profesores/Change_password')?>">Contraseña</a></li>

</ul>
</li>
<!-- Menu 1-->

<!-- Menu 2-->
<li class="dropdown"><a class="nav-link menu-title" href="javascript:void(0)"><i data-feather="file-text"></i><span>Calificaciones</span></a>
<ul class="nav-submenu menu-content">
<li><a href="<?=base_url('Profesores/Calificaciones')?>">Calificaciones</a></li>

</ul>
</li>
<!-- Menu 2-->

<!-- Menu 3-->
<li class="dropdown"><a class="nav-link menu-title" href="javascript:void(0)"><i data-feather="clock"></i><span>Horario</span></a>
<ul class="nav-submenu menu-content">
<li><a href="<?=base_url ('Profesores/Horario')?>">Horario Actual</a></li>

</ul>
</li>
<!-- Menu 3-->

<!-- Menu 4-->
<li class="dropdown"><a class="nav-link menu-title" href="javascript:void(0)"><i data-feather="clipboard"></i><span>Planeaciones</span></a>
<ul class="nav-submenu menu-content">
<li><a href="<?=base_url('Profesores/Temario')?>">Planeaciones</a></li>
</ul>
</li>
<!-- Menu 4-->

<!-- Menu 5-->
<li class="dropdown"><a class="nav-link menu-title" href="javascript:void(0)"><i data-feather="briefcase"></i><span>Diplomados</span></a>
<ul class="nav-submenu menu-content">
<li><a href="<?=base_url('Profesores/Diplomados')?>">Diplomados Actuales</a></li>
<li><a href="<?=base_url('Profesores/Diplomado_Criminalistica')?>">Diplomado Criminalistica</a></li>
</ul>
</li>
<!-- Menu 5-->




</ul>
</div>
<div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
</div>
</nav>
</header>
<!-- Page Sidebar Ends-->