
<html lang="en">
<head>
<title>DIPLOMADO DER</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Integra-Educa">
<meta name="keywords" content="Integra-Educa, Integra">
<meta name="author" content="Mike">

<?=$this->load->view('Include/base_css','',TRUE);?>


</head>
<body>

<!-- Loader starts-->
<div class="loader-wrapper">
<div class="theme-loader">    
<div class="loader-p"></div>
</div>
</div>
<!-- Loader ends-->


<!-- page-wrapper Start-->
<div class="page-wrapper" id="pageWrapper">

<!-- Page Header Start-->
<?=$this->load->view('Profesores/Header','',TRUE);?>
<!-- Page Header Start-->


<!-- Page Body Start-->
<div class="page-body-wrapper horizontal-menu">
 
<!-- Page Sidebar Inicio-->
<?=$this->load->view('Profesores/Side_bar','',TRUE);?>
<!-- Page Sidebar Fin-->

<!-- TITULOS PAGINA-->
<div class="page-body">
<div class="container-fluid">
<div class="page-header">
<div class="row ">
<div class="col-sm-6 ">
<h3>Diplomado Derecho</h3>
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="<?=base_url('Profesores/')?>">Inicio</a></li>
<li class="breadcrumb-item">Home</li>
</ol>
</div>
</div>
</div>
</div>
<!-- TITULOS PAGINA-->
<div class="row justify-content-center mr-2 ml-2">
<div class="col-sm-12 col-xl-12 col-lg-12">
</div>
<span class="badge badge-warning text-dark" style="font-size: 22px; border-radius: 25px;">-Sistema Integral de Información-</span>
</div>

<br>



<!-- Container-fluid starts-->
<div class="container-fluid">
<div class="row justify-content-center">

<form id="frmData">

<div class="table-responsive" style="border-radius: 15px;">
<table class="table table-striped text-center table-sm table-hover" style="background-color: #90adc1 !important; " id="tabla">
<thead class="table-dark">
<tr>
<th scope="col" class="text-white">Matricula</th>
<th scope="col" class="text-white" >Alumno</th>
<th scope="col" class="text-white">Calif 1</th>
<th scope="col" class="text-white">Calif 2</th>
<th scope="col" class="text-white">Calif 3</th>


<th scope="col" class="text-white">Final</th>


</tr>
</thead>



<tbody>
<?php foreach ($usuarios->result() as $usuario){?>
<tr>
<td><input class="form-control" name="<?= $usuario->Matricula_Alumno?>[Matricula]" value="<?= $usuario->Matricula_Alumno ?>" type="text" readonly style="height: 50%; font-weight:bold; text-align: center; width: 125px;"></td>

<td><input class="form-control" name="<?= $usuario->Matricula_Alumno?>[Nombre]" value="<?= $usuario->Nombre ?>" type="text" readonly style="height: 50%; font-weight:bold; text-align: center; width: 300px;"></td>


<td><input class="form-control input-focus" name="<?= $usuario->Matricula_Alumno?>[Calificacion_1]" value="<?= $usuario->calificacion_1 ?>" type="text" style="height: 50%; text-align: center; width: 75px;" ></td>




<td><input class="form-control input-focus" name="<?= $usuario->Matricula_Alumno?>[Calificacion_2]" value="<?= $usuario->calificacion_2 ?>" type="text" style="height: 50%; text-align: center; width: 75px;" ></td>



<td><input class="form-control input-focus" name="<?= $usuario->Matricula_Alumno?>[Calificacion_3]" value="<?= $usuario->calificacion_3 ?>" type="text" style="height: 50%; text-align: center; width: 75px;" ></td>



<td><input class="form-control input-focus" name="<?= $usuario->Matricula_Alumno?>[Final]" value="<?= $usuario->final ?>" type="text" style="height: 50%; text-align: center; width: 75px;" readonly></td>



</tr>
<?php
}
?>
</tbody>
</table>
</div>


<div class="btn-list row mt-2 justify-content-center ml-3 mr-3">
<button type="submit" class="btn btn-success btn-lg btn-block" style="border-radius: 25px;" >Guardar</button>

</div>


</form>
</div>


<!-- Container-fluid Ends-->
</div>
</div>

<!-- footer start-->
<footer class="footer" style="margin-left: auto; margin-right: auto;">
<div class="row">
<div class="col-md-6 footer-copyright">
<p class="mb-0">Copyright 2022-23 © IMEI / CEP</p>
</div>
<div class="col-md-6">
<p class="pull-right mb-0">Made by "Hack the box" with <i class="fa fa-heart font-secondary"></i></p>
</div>
</div>

</footer>
</div>

<?=$this->load->view('Include/base_js','',TRUE);?>

<script src="<?=base_url('library/src/plugins/sweetalert2/sweetalert2.all.js')?>"></script>
<script src="<?=base_url('library/src/plugins/sweetalert2/sweet-alert.init.js')?>"></script>


<script type="text/javascript">

jQuery(document).ready(function() {

jQuery('#frmData').on('change', function() {


$('#frmData tbody tr').each(function() {

var calif_1 =  parseFloat( $(this).find('input[type="text"]').eq(2).val() );
var calif_2 =  parseFloat( $(this).find('input[type="text"]').eq(3).val() );
var calif_3 =  parseFloat( $(this).find('input[type="text"]').eq(4).val() );


var suma = ((calif_1 + calif_2 + calif_3)/3); 
var round = Math.round(suma)

$(this).find('input[type="text"]').eq(5).val(round.toFixed(1));


});

});
});

</script>


<script type="text/javascript">

$("#frmData").on('submit', function(e){
e.preventDefault()
var dataForm = new FormData($("#frmData")[0]);

$.ajax({
type: "POST",
url: "../Profesores/Insert_Calificaciones_Dip_Derecho",
data: dataForm,
processData: false,
contentType: false,
cache:false,
dataType: 'json',
success:function(data){
var a = data;
if(a==1)
{

swal(
{
title: 'Calificaciones guardadas',
type: 'success',
confirmButtonClass: 'btn btn-success'

}).then(function () {
window.location.reload(true);  
})
}
else
{

}


}
});

})

</script>
<!-- SCRIPT DE ENVIO-->


</body>
</html>