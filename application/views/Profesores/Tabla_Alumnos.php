
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Integra-Educa">
<meta name="keywords" content="Integra-Educa, Integra">
<meta name="author" content="Mike">

<?=$this->load->view('Include/base_css','',TRUE);?>

<!-- CSS -->

<!-- CSS -->

</head>
<body>
<!-- IMEI -->


<!--IMEI -->
<!-- Loader starts-->
<div class="loader-wrapper">
<div class="theme-loader">    
<div class="loader-p"></div>
</div>
</div>
<!-- Loader ends-->


<!-- page-wrapper Start-->
<div class="page-wrapper" id="pageWrapper">

<!-- Page Header Start-->
<?=$this->load->view('Profesores/Header','',TRUE);?>
<!-- Page Header Start-->


<!-- Page Body Start-->
<div class="page-body-wrapper horizontal-menu">
 
<!-- Page Sidebar Inicio-->
<?=$this->load->view('Profesores/Side_bar','',TRUE);?>
<!-- Page Sidebar Fin-->

<!-- TITULOS PAGINA-->
<div class="page-body">
<div class="container-fluid">
<div class="page-header">
<div class="row ">
<div class="col-sm-6 ">
<h3>Datos personales</h3>
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="<?=base_url('Profesores/')?>">Inicio</a></li>
<li class="breadcrumb-item">Home</li>
</ol>
</div>
</div>
</div>
</div>
<!-- TITULOS PAGINA-->

<div class="row justify-content-center mr-2 ml-2">
<div class="col-sm-12 col-xl-12 col-lg-12">
</div>
<span class="badge badge-warning text-dark" style="font-size: 22px; border-radius: 25px;"></span>
</div>

<br>

<!-- Container-fluid starts-->
<div class="container-fluid general-widget">
<div class="row justify-content-center d-flex align-items-center">


<!--Aqui va el codigo de IMEI para las calificaciones -->
<!-- A Contar Inicio-->

<!-- CONTENEDOR-->  


<div class="pd-20 card-box mb-30">
<div class="clearfix mb-20">
<div class="text-center mb-2">
<h4 class="h4 text-center mb-2"><strong>LISTA DE CALIFICACIONES</strong>
<br>
<span class="badge badge-success mt-2 mb-2" id="nombre_materia" style="border-radius: 25px;"></span> 
<span class="badge badge-warning text-dark mt-2 mb-2" id="nombre_carrera" style="border-radius: 25px;"></span></h4>

</div>



<form id="frmData" method="post">
<div class="table-responsive" style="border-radius: 15px;">
<table class="table table-striped text-center table-sm table-hover" style="background-color: #90adc1 !important; " id="tabla">
<thead class="table-dark">
<tr>
<th scope="col" class="text-white">Matricula</th>
<th scope="col" class="text-white" >Alumno</th>
<th scope="col" class="text-white">Parcial 1</th>
<th scope="col" class="text-white">Parcial 2</th>
<th scope="col" class="text-white">Parcial 3</th>
<th scope="col" class="text-white">Promedio</th>
<th scope="col" class="d-none">Materia</th>
<th scope="col" class="d-none">Profesor</th>
</tr>
</thead>

<tbody>
<?php foreach ($usuarios->result() as $usuario){?>
<tr>
<td><input class="form-control" name="<?= $usuario->Matricula_Alumno?>[matricula]" value="<?= $usuario->Matricula_Alumno ?>" type="text" readonly style="height: 50%; font-weight:bold; text-align: center; width: 125px;"></td>
<td><input class="form-control" name="<?= $usuario->Matricula_Alumno?>[alumno]" value="<?= $usuario->Alumno ?>" type="text" readonly style="height: 50%; font-weight:bold; text-align: center; width: 300px;"></td>
<td><input class="form-control input-focus" name="<?= $usuario->Matricula_Alumno?>[Calificacion_1]" value="<?= $usuario->calificacion_1 ?>" type="text" style="height: 50%; text-align: center; width: 75px;" ></td>
<td><input  class="form-control input-focus" name="<?= $usuario->Matricula_Alumno?>[Calificacion_2]" value="<?= $usuario->calificacion_2 ?>" type="text" style="height: 50%; text-align: center; width: 75px;" ></td>
<td><input class="form-control input-focus" name="<?= $usuario->Matricula_Alumno?>[Calificacion_3]" value="<?= $usuario->calificacion_3 ?>" type="text" style="height: 50%; text-align: center; width: 75px;"></td>
<td><input class="form-control input-focus" name="<?= $usuario->Matricula_Alumno?>[final]" value="<?= $usuario->final ?>" type="text" style="height: 50%; text-align: center; width: 75px;" readonly></td>
<td class="d-none"><input class="form-control" name="<?= $usuario->Matricula_Alumno?>[Id_materia]" value="<?= $usuario->id_materia ?>" type="text" readonly style="height: 50%; text-align: center;"></td>
<td class="d-none"><input class="form-control" name="<?= $usuario->Matricula_Alumno?>[Matricula_profesor]" value="<?= $usuario->matricula_profesor ?>" type="text" readonly style="height: 50%;"></td>
</tr>
<?php
}
?>
</tbody>
</table>
</div>

<div class="btn-list row mt-2 justify-content-center ml-3 mr-3">
<button type="submit" class="btn btn-success btn-lg btn-block" style="border-radius: 25px;" >Guardar</button>

</div>
</form>




<hr>



</div>          

</div>
<!-- FORMULARIO-->

<!-- FOOTER-->  
<div class="footer-wrap pd-20 mb-20 card-box d-none">Power by Mike</div>
<!-- FOOTER-->  


</div>
<!-- CONTENEDOR-->

<!-- A Contar Fin-->
<!--Aqui va el codigo de IMEI para las calificaciones -->

</div>
</div>

<!-- Container-fluid Ends-->
</div>

<!-- Scripts y estilos-->

<!-- Scripts y estilos-->


<!-- footer start-->
<footer class="footer">
<div class="container-fluid">
<div class="row">
<div class="col-md-6 footer-copyright">
<p class="mb-0">Copyright 2022-23 © IMEI / CEP</p>
</div>
<div class="col-md-6">
<p class="pull-right mb-0">Made by "Hack the box" with <i class="fa fa-heart font-secondary"></i></p>
</div>
</div>
</div>
</footer>
</div>
</div>
<?=$this->load->view('Include/base_js','',TRUE);?>




<!-- Scripts estilos-->
<script> 

document.addEventListener('DOMContentLoaded', () => {

localStorage.clear();
document
.querySelectorAll('a')
.forEach(input => {
input.addEventListener('click', function (e) {

e.preventDefault();

var nombre = input.getAttribute("value");
var carrera = input.getAttribute("title");
var ref = input.getAttribute("href"); 
localStorage.setItem('nombre',nombre);
localStorage.setItem('carrera',carrera);

window.location.href = ref;
console.log(ref)
})

})


});
</script>


<style>

.btn-default{
  background:#006EFF; width: 100%; color:#fff; font-weight:700; text-shadow:1px 1px 0 rgba(0,0,0,0.2); font-size:14px;
}
.card{
  box-shadow:2px 2px 20px rgba(0,0,0,0.3); border:none; margin-bottom:30px;
}
.card:hover{
  transform: scale(1.1);
  transition: all 1s ease;
  z-index: 999;
}
.card-01 .card-body{
  position:relative; padding-top:40px;
}
.card-01 .badge-box{
  position:absolute; 
  top:-20px; left:50%; width:100px; height:100px;margin-left:-105px; text-align:center;
}
.card-01 .badge-box i{
  font: message-box;
  background:#006EFF; color:#fff; border-radius:20px;  width:200px; height:50px; line-height:50px; text-align:center; font-size:20px;
}
.card-01 .height-fix{
  height:455px; overflow:hidden;
}

.social-box i {
  border:1px solid #006EFF; color:#006EFF; width:30px; height:30px; border-radius:50%;line-height:30px;
}

.social-box i:hover{
  background:#DFC717; color:#fff;
}
</style>


<!-- JS -->
<?=$this->load->view('Include/Js','',TRUE);?>
<!-- JS -->

<!-- Scripts y estilos-->

<!-- Script para las calificaciones-->

<script> 

document.addEventListener('DOMContentLoaded', () => {
document
.querySelectorAll('.input-focus')
.forEach(input => {
input.addEventListener('keypress', function (e) {

if (e.keyCode === 13) {
e.preventDefault()
const parentRow = e.srcElement.parentElement
const parentCellIndex = parentRow.cellIndex
const nextRow = parentRow.parentElement.nextElementSibling
if (nextRow && nextRow.localName === 'tr') {

nextRow.children[parentCellIndex].children[0].focus()
} else {

}
}

})

})


});
</script>


<!-- SCRIPT DE SUMA-->

<script type="text/javascript">



jQuery(document).ready(function() {



jQuery('#frmData').on('change', function() {

$('#frmData tbody tr').each(function() {

var calif_1 =  parseFloat( $(this).find('input[type="text"]').eq(2).val() );
var calif_2 =  parseFloat( $(this).find('input[type="text"]').eq(3).val() );
var calif_3 =  parseFloat( $(this).find('input[type="text"]').eq(4).val() );

var suma = ((calif_1 + calif_3 + calif_2)/3); 
var round = Math.round(suma)

$(this).find('input[type="text"]').eq(5).val(round.toFixed(1));
console.log(suma) 

});
//console.log('HOLA');
});
});
</script>


<!-- SCRIPT DE SUMA-->






<!-- SCRIPT DE ENVIO-->

<script type="text/javascript">

$("#frmData").on('submit', function(e){
e.preventDefault()
var dataForm = new FormData($("#frmData")[0]);

$.ajax({
type: "POST",
url: "<?php echo base_url(); ?>Profesores/Insert_Calificaciones",
data: dataForm,
processData: false,
contentType: false,
cache:false,
dataType: 'json',
success:function(data){
var a = data;
if(a==1)
{

swal(
{
title: 'Calificaciones guardadas',
type: 'success',
confirmButtonClass: 'btn btn-success'

}).then(function () {
window.location.reload(true);  
})
}
else
{

}

}
});

})

</script>
<!-- SCRIPT DE ENVIO-->


<script type="text/javascript">
  
document.addEventListener('DOMContentLoaded', () => {

var a='MATERIA: ';
var b='CARRERA: ';

var c=sessionStorage.getItem('nombre');
var d=sessionStorage.getItem('carrera');


document.getElementById("nombre_materia").innerHTML = a + c;
document.getElementById("nombre_carrera").innerHTML = b + d;
  });
</script>



<!-- JS -->
<?=$this->load->view('Include/Js','',TRUE);?>
<!-- JS -->

<!-- add sweet alert js & css in footer -->


  <script src="<?=base_url('library/src/plugins/sweetalert2/sweetalert2.all.js')?>"></script>
  <script src="<?=base_url('library/src/plugins/sweetalert2/sweet-alert.init.js')?>"></script>

<!-- Script para las calificaciones-->


</body>
</html>