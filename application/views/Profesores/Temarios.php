
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Integra-Educa">
<meta name="keywords" content="Integra-Educa, Integra">
<meta name="author" content="Mike">

<?=$this->load->view('Include/base_css','',TRUE);?>


</head>
<body>

<!-- Loader starts-->
<div class="loader-wrapper">
<div class="theme-loader">    
<div class="loader-p"></div>
</div>
</div>
<!-- Loader ends-->


<!-- page-wrapper Start-->
<div class="page-wrapper" id="pageWrapper">

<!-- Page Header Start-->
<?=$this->load->view('Profesores/Header','',TRUE);?>
<!-- Page Header Start-->


<!-- Page Body Start-->
<div class="page-body-wrapper horizontal-menu">
 
<!-- Page Sidebar Inicio-->
<?=$this->load->view('Profesores/Side_bar','',TRUE);?>
<!-- Page Sidebar Fin-->

<!-- TITULOS PAGINA-->
<div class="page-body">
<div class="container-fluid">
<div class="page-header">
<div class="row ">
<div class="col-sm-6 ">
<h3>Temarios</h3>
</div>
</div>
</div>
</div>
<!-- TITULOS PAGINA-->

<div class="row justify-content-center mr-2 ml-2">
<div class="col-sm-12 col-xl-12 col-lg-12">
</div>
<span class="badge badge-warning text-dark" style="font-size: 22px; border-radius: 25px;">Bienvenido</span>
</div>

<br>



<!-- Container-fluid starts-->
<div class="container-fluid general-widget">
<div class="row justify-content-center d-flex align-items-center">

<hr>
<br>

<!-- Card 1-->
<div class="col-sm-6 col-xl-3 col-lg-6">
<div class="card o-hidden border-0">
<div class="bg-primary b-r-4 card-body">
<div class="media static-top-widget">
<div class="align-self-center text-center"><i data-feather="database"></i></div>
<div class="media-body"><span class="m-0"></span>
<h5 class="mb-0"><strong>Derecho</strong></h5><i class="icon-bg" data-feather="user-plus"></i>
</div>
</div>
</div>
</div>
</div>
<!-- Card 1-->

<!-- Card 2-->
<div class="col-sm-6 col-xl-3 col-lg-6">
<div class="card o-hidden border-0">
<div class="bg-secondary b-r-4 card-body">
<div class="media static-top-widget">
<div class="align-self-center text-center"><i data-feather="shopping-bag"></i></div>
<div class="media-body"><span class="m-0"></span>
<h5 class="mb-0"><strong>Psicologia</strong></h5><i class="icon-bg" data-feather="shopping-bag"></i>
</div>
</div>
</div>
</div>
</div>
<!-- Card 2-->

<!-- Card 1-->
<div class="col-sm-6 col-xl-3 col-lg-6">
<div class="card o-hidden border-0">
<div class="bg-primary b-r-4 card-body">
<div class="media static-top-widget">
<div class="align-self-center text-center"><i data-feather="database"></i></div>
<div class="media-body"><span class="m-0"></span>
<h5 class="mb-0"><strong>Administracion</strong></h5><i class="icon-bg" data-feather="user-plus"></i>
</div>
</div>
</div>
</div>
</div>
<!-- Card 1-->




</div>
</div>

<!-- Container-fluid Ends-->
</div>

<!-- footer start-->
<footer class="footer">
<div class="container-fluid">
<div class="row">
<div class="col-md-6 footer-copyright">
<p class="mb-0">Copyright 2022-23 © IMEI / CEP</p>
</div>
<div class="col-md-6">
<p class="pull-right mb-0">Made by "Hack the box" with <i class="fa fa-heart font-secondary"></i></p>
</div>
</div>
</div>
</footer>
</div>
</div>
<?=$this->load->view('Include/base_js','',TRUE);?>

</body>
</html>