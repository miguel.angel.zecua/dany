<!DOCTYPE html>
<html>
<!-- HEAD -->
<head>
<meta charset="utf-8">
<title>Login Profesores</title>
<!-- CSS -->
<?=$this->load->view('Include/Head','',TRUE);?>
<!-- CSS -->
</head>
<!-- HEAD -->

<body class="login-page">

<div class="login-header box-shadow ">
<div class="container-fluid d-flex justify-content-between align-items-center">
<div class="brand-logo">
<a href="">

<h1 class="text-center">Inicio de sesión Maestros</h1>
</a>
</div>
</div>
</div>

<div class="login-wrap d-flex align-items-center flex-wrap justify-content-center">
<div class="container">

<div class="row align-items-center">
<div class="col-md-6 col-lg-7">
<img src="<?=base_url('library/vendors/images/banner-img.png')?>" alt="">
</div>
<div class="col-md-6 col-lg-5">
<div class="login-box bg-white box-shadow border-radius-10">
<div class="login-title">
<h2 class="text-center text-dark">Inicio de sesión</h2>
</div>

<form method="post" action="<?php echo site_url('Profesores/Enter')?>">

<div class="input-group custom">
<input type="text" class="form-control form-control-lg" name="Matricula" id="Matricula" placeholder="Usuario" style="border-radius: 25px;" onkeyup="mayus(this);">
<div class="input-group-append custom">
<span class="input-group-text"><i class="icon-copy dw dw-user1"></i></span>
</div>
</div>

<div class="input-group custom">
<input type="password" class="form-control form-control-lg" name="Password" id="Password" placeholder="**********" style="border-radius: 25px;">
<div class="input-group-append custom">
<span class="input-group-text"><i class="dw dw-padlock1"></i></span>
</div>
</div>

<div class="row justify-content-center text-center">
<div class="col-sm-12">


<button class="btn btn-info margin-bottom-none" type="submit" style="width: 250px; border-radius: 20px;">Iniciar</button>


</div>
</div>

</form>

</div>
</div>
</div>
</div>
</div>



<script type="text/javascript">
	
	function mayus(e) {
    e.value = e.value.toUpperCase();
}
</script>

<!-- JS -->
<?=$this->load->view('Include/Js','',TRUE);?>
<!-- JS -->

</body>
</html>