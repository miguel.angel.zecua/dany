<!-- HEADER DE LOS PROFESORES-->

<!-- Page Header Start-->
<div class="page-main-header">
<div class="main-header-right row m-0">
<div class="main-header-left">
<div class="logo-wrapper"><a href="index.html"><img class="img-fluid" src="" 
alt=""></a></div>
<div class="dark-logo-wrapper"><a href="index.html"><img class="img-fluid" src="" alt=""></a></div>
<div class="toggle-sidebar"><i class="status_toggle middle" data-feather="align-center" id="sidebar-toggle"></i></div>
</div>



<div class="nav-right col pull-right right-menu p-0">
<ul class="nav-menus">
<li><a class="text-dark" href="#!" onclick="javascript:toggleFullScreen()"><i data-feather="maximize"></i></a></li>
<li class="onhover-dropdown">
<div class="bookmark-box"><i data-feather="star"></i></div>
<div class="bookmark-dropdown onhover-show-div">

<ul class="m-t-5">
<li class="add-to-bookmark">
<a href="<?=base_url('Profesores/Calificaciones')?>">
<i class="bookmark-icon" data-feather="inbox"></i>Calificaciones<span class="pull-right"><i data-feather="star"></i></span>
</a>
</li>

<li class="add-to-bookmark">
<a href="<?=base_url ('Profesores/Horario')?>">
<i class="bookmark-icon" data-feather="message-square"></i>Horarios Actuales<span class="pull-right"><i data-feather="star"></i></span>
</a>
</li>

<li class="add-to-bookmark">
<a href="<?=base_url('Profesores/Temario')?>">
<i class="bookmark-icon" data-feather="command"></i>Planeaciones<span class="pull-right"><i data-feather="star"></i></span>
</a>
</li>

</ul>
</div>
</li>


<!-- NOTIFICACIONES-->
<?=$this->load-> view('Alumnos/Notify','',TRUE); ?>
<!-- NOTIFICACIONES-->


<li>
<div class="mode"><i class="fa fa-moon-o"></i></div>
</li>


<li class="onhover-dropdown p-0">
<button class="btn btn-primary-light" type="button"><a href="<?=base_url('Profesores/End')?>"><i data-feather="log-out"></i>Cerrar sesión</a></button>
</li>
</ul>
</div>
<div class="d-lg-none mobile-toggle pull-right w-auto"><i data-feather="more-horizontal"></i></div>
</div>
</div>
<!-- Page Header Ends  -->