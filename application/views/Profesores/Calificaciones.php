
<html lang="en">
<head>
<title>CALIFICACIONES</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Integra-Educa">
<meta name="keywords" content="Integra-Educa, Integra">
<meta name="author" content="Mike">

<?=$this->load->view('Include/base_css','',TRUE);?>

<!-- CSS -->

<!-- CSS -->

</head>
<body>
<!-- IMEI -->


<!--IMEI -->
<!-- Loader starts-->
<div class="loader-wrapper">
<div class="theme-loader">    
<div class="loader-p"></div>
</div>
</div>
<!-- Loader ends-->


<!-- page-wrapper Start-->
<div class="page-wrapper" id="pageWrapper">

<!-- Page Header Start-->
<?=$this->load->view('Profesores/Header','',TRUE);?>
<!-- Page Header Start-->


<!-- Page Body Start-->
<div class="page-body-wrapper horizontal-menu">
 
<!-- Page Sidebar Inicio-->
<?=$this->load->view('Profesores/Side_bar','',TRUE);?>
<!-- Page Sidebar Fin-->

<!-- TITULOS PAGINA-->
<div class="page-body">
<div class="container-fluid">
<div class="page-header">
<div class="row ">
<div class="col-sm-6 ">
<h3>Calificaciones</h3>
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="<?=base_url('Profesores/')?>">Inicio</a></li>
<li class="breadcrumb-item">Home</li>
</ol>
</div>
</div>
</div>
</div>
<!-- TITULOS PAGINA-->

<div class="row justify-content-center mr-2 ml-2">
<div class="col-sm-12 col-xl-12 col-lg-12">
</div>
<span class="badge badge-warning text-dark" style="font-size: 22px; border-radius: 25px;">-Sistema Integral de Información-</span>
</div>

<!-- Container-fluid starts-->
<div class="container-fluid">
<div class="row justify-content-center mt-3">


<!--Aqui va el codigo de IMEI para las calificaciones -->

<!--Codigo a cortar -->

<!-- CONTENEDOR-->  

<?php 

foreach ($usuario->result() as $usuario){?>



 <div class="col-sm-3 col-md-3 col-xs-3 col-lg-3 d-flex align-items-stretch mt-2 mb-2">

<div class="card card-01">
  <img class="card-img-top" src="https://images.pexels.com/photos/247599/pexels-photo-247599.jpeg?h=350&auto=compress&cs=tinysrgb" alt="Card image cap">
  <div class="card-body" style="padding: 15px;">
    <span class="badge-box d-inline-block text-truncate" style="width:200px;"><i class="fa text-truncate" style="font-size: 15px;"><?= $usuario->carrera ?></i></span>
    <hr class="mt-1">
    <h6 class="card-title text-center"><strong><?= $usuario->nombre ?></strong></h6>
    <hr class="mb-2">
    <p class="card-text text-center mb-0 ">
      
<span class="badge bg-info text-dark mb-0" style="font-size:14px;"><?= $usuario->cuatrimestre ?>°</span>

<span class="badge bg-warning text-dark mb-0" style="font-size:14px"><?= $usuario->turno ?></span>

</p>



  </div>
  
  <?php
$uno = $usuario->id_salon;
$inter ='/';
$dos = $usuario->id_materia;
$tres = $uno . $inter .$dos;
?>

  <div class="card-footer mt-0 text-center" style="padding: 15px;">
<a href="<?=
base_url('Profesores/Tabla_Alumnos/'.$tres) ?>" class="btn btn-primary text-dark align-self-end" value="<?= $usuario->nombre ?>" title="<?= $usuario->carrera ?>">Ver Calificaciones</a>

  </div> 
</div>
</div>





 

<?php
}
?>

<!-- FIN DE LA FILA-->



<!-- FORMULARIO-->

      <!--Codigo a cortar -->
<!--Aqui va el codigo de IMEI para las calificaciones -->

</div>


<!-- Container-fluid Ends-->
</div>

<!-- Scripts y estilos-->

<!-- Scripts y estilos-->

</div>
</div>

<!-- footer start-->
<footer class="footer" style="margin-left: auto; margin-right: auto;">
<div class="row">
<div class="col-md-6 footer-copyright">
<p class="mb-0">Copyright 2022-23 © IMEI / CEP</p>
</div>
<div class="col-md-6">
<p class="pull-right mb-0">Made by "Hack the box" with <i class="fa fa-heart font-secondary"></i></p>
</div>
</div>

</footer>
</div>
</div>
<?=$this->load->view('Include/base_js','',TRUE);?>





<!-- Scripts estilos-->
<script> 

document.addEventListener('DOMContentLoaded', () => {

sessionStorage.clear();
document
.querySelectorAll('a')
.forEach(input => {
input.addEventListener('click', function (e) {

e.preventDefault();

var nombre = input.getAttribute("value");
var carrera = input.getAttribute("data-bs-original-title");
var ref = input.getAttribute("href"); 
console.log(nombre)
console.log(carrera)

sessionStorage.setItem('nombre',nombre);
sessionStorage.setItem('carrera',carrera);

window.location.href = ref;

})

})


});
</script>


<style>

.btn-default{
  background:#006EFF; width: 100%; color:#fff; font-weight:700; text-shadow:1px 1px 0 rgba(0,0,0,0.2); font-size:14px;
}
.card{
  box-shadow:2px 2px 20px rgba(0,0,0,0.3); border:none; margin-bottom:30px;
}
.card:hover{
  transform: scale(1.1);
  transition: all 1s ease;
  z-index: 999;
}
.card-01 .card-body{
  position:relative; padding-top:40px;
}
.card-01 .badge-box{
  position:absolute; 
  top:-20px; left:50%; width:100px; height:100px;margin-left:-105px; text-align:center;
}
.card-01 .badge-box i{
  font: message-box;
  background:#006EFF; color:#fff; border-radius:20px;  width:200px; height:50px; line-height:50px; text-align:center; font-size:20px;
}
.card-01 .height-fix{
  height:455px; overflow:hidden;
}

.social-box i {
  border:1px solid #006EFF; color:#006EFF; width:30px; height:30px; border-radius:50%;line-height:30px;
}

.social-box i:hover{
  background:#DFC717; color:#fff;
}
</style>


<!-- JS -->
<?=$this->load->view('Include/Js','',TRUE);?>
<!-- JS -->

<!-- Scripts y estilos-->

</body>
</html>