
<html lang="en">
<head>
<title>DATOS PERSONALES</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Integra-Educa">
<meta name="keywords" content="Integra-Educa, Integra">
<meta name="author" content="Mike">

<?=$this->load->view('Include/base_css','',TRUE);?>

<!--IMEI-->



<!--IMEI-->

</head>
<body>

<!-- Loader starts-->
<div class="loader-wrapper">
<div class="theme-loader">    
<div class="loader-p"></div>
</div>
</div>
<!-- Loader ends-->


<!-- page-wrapper Start-->
<div class="page-wrapper" id="pageWrapper">

<!-- Page Header Start-->
<?=$this->load->view('Profesores/Header','',TRUE);?>
<!-- Page Header Start-->


<!-- Page Body Start-->
<div class="page-body-wrapper horizontal-menu">
 
<!-- Page Sidebar Inicio-->
<?=$this->load->view('Profesores/Side_bar','',TRUE);?>
<!-- Page Sidebar Fin-->

<!-- TITULOS PAGINA-->
<div class="page-body">
<div class="container-fluid">
<div class="page-header">
<div class="row ">
<div class="col-sm-6 ">
<h3>Datos personales</h3>
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="<?=base_url('Profesores/')?>">Inicio</a></li>
<li class="breadcrumb-item">Home</li>
</ol>
</div>
</div>
</div>
</div>
<!-- TITULOS PAGINA-->

<div class="row justify-content-center mr-2 ml-2">
<div class="col-sm-12 col-xl-12 col-lg-12">
</div>
<span class="badge badge-warning text-dark" style="font-size: 22px; border-radius: 25px;">-Sistema Integral de Información-</span>
</div>


<hr style="height: 3px; background-color: #2C3E50 ; border-radius: 15px;">
<!-- Container-fluid starts-->
<div class="container-fluid">

<!--Codigo de IMEI para los datos personales de los profesores -->
<!-- FORMULARIO-->




<h6 class="text-left" style="color: #000000">Datos generales</h6>
<br>
<form method="post" action="<?php echo site_url('Profesores/update')?>/<?php echo $row->Matricula; ?>">

<!-- FILA 1-->
<div class="row text-left">

<div class="form-group col-md-4">
<label for="inputEmail4">Apellido Paterno</label>
<input type="text" class="form-control" name="Apellido_Paterno" value="<?php echo $row->Apellido_Paterno;?>" readonly>
</div>

<div class="form-group col-md-4">
<label for="inputPassword4">Apellido Materno</label>
<input type="text" class="form-control" name="Apellido_Materno" value="<?php echo $row->Apellido_Materno;?>" readonly>
</div>

<div class="form-group col-md-4">
<label for="inputPassword4">Nombre(s)</label>
<input type="text" class="form-control" name="Nombres" value="<?php echo $row->Nombres;?>" readonly>
</div>

</div>
<!-- FILA 1-->


<hr style="height: 3px; background-color: #2C3E50 ; border-radius: 15px;">
<h6 class="text-left" style="color: #000000">Lugar de Nacimiento</h6>
<br>

<!-- FILA 3-->
<div class="row text-left">




<div class="form-group col-md-4">
<label for="inputCity">Estado</label>
<select class="form-control" name="Estado" id="id_estado">
<option selected value="<?php echo $row->Estado;?>"></option>
<?php
   $mysqli = new mysqli('localhost', 'u425634773_integraeduca', 'k$56o$f9K#MB', 'u425634773_integraeduca');

$query = $mysqli -> query ("SELECT * FROM estados");
          while ($valores = mysqli_fetch_array($query)) {
            echo '<option value="'.$valores['id'].'">'.$valores['nombre'].'</option>';
          }
?>
</select>
</div>

<div class="form-group col-md-4">
<label for="inputState">Municipio</label>
<select class="form-control" name="Municipio" id="id_municipio">
<option selected value="<?php echo $row->Municipio;?>"></option>
</select>
</div>


<script type="text/javascript">
  
    $(document).ready(function(){
    
        var municipio = $('#id_municipio');
       
        $('#id_estado').change(function(){
          var id_estado = $(this).val();        

            $.ajax({
              data: {id_estado:id_estado}, 
              dataType: 'html', 
              type: 'POST', 
              url: '../get_municipio.php', 

              }).done(function(data){   
                municipio.html(data);       
              });      
            
        });

    });
</script>



<div class="form-group col-md-4">
<label for="inputZip">Localidad</label>
<select class="form-control" name="Localidad" id="id_localidad">
<option selected value="<?php echo $row->Localidad;?>"></option>
</select>
</div>


<script type="text/javascript">
  
    $(document).ready(function(){
    
        var localidad = $('#id_localidad');
       
        $('#id_municipio').change(function(){
          var id_municipio = $(this).val();        

            $.ajax({
              data: {id_municipio:id_municipio}, 
              dataType: 'html', 
              type: 'POST', 
              url: '../get_localidad.php', 

              }).done(function(data){   
                localidad.html(data);       
              });      
            
        });

    });
</script>


</div>
<!-- FILA 3-->

<hr style="height: 3px; background-color: #2C3E50 ; border-radius: 15px;">
<h6 class="text-left" style="color: #000000">Domicilio Particular</h6>
<br>

<!-- FILA 4-->
<div class="row text-left">

<div class="form-group col-md-3">
<label for="inputEmail4">Codigo Postal</label>
<input type="text" class="form-control" name="Codigo_Postal" value="<?php echo $row->Codigo_Postal;?>">
</div>

<div class="form-group col-md-3">
<label for="inputCity">Estado</label>
<select class="form-control" name="Estado_Actual" id="id_estado_2">
<option selected value="<?php echo $row->Estado_Actual;?>"></option>
<?php
  $mysqli = new mysqli('localhost', 'u425634773_integraeduca', 'k$56o$f9K#MB', 'u425634773_integraeduca');

$query = $mysqli -> query ("SELECT * FROM estados");
          while ($valores = mysqli_fetch_array($query)) {
            echo '<option value="'.$valores['id'].'">'.$valores['nombre'].'</option>';
          }
?>
</select>
</div>

<div class="form-group col-md-3">
<label for="inputState">Municipio</label>
<select class="form-control" name="Municipio_Actual" id="id_municipio_2">
<option selected value="<?php echo $row->Municipio_Actual;?>"></option>
</select>
</div>

<script type="text/javascript">
  
    $(document).ready(function(){
    
        var municipio2 = $('#id_municipio_2');
       
        $('#id_estado_2').change(function(){
          var id_estado_2 = $(this).val();        

            $.ajax({
              data: {id_estado_2:id_estado_2}, 
              dataType: 'html', 
              type: 'POST', 
              url: '../get_municipio_2.php', 

              }).done(function(data){   
                municipio2.html(data);       
              });      
            
        });

    });
</script>

<div class="form-group col-md-3">
<label for="inputZip">Localidad</label>
<select class="form-control" name="Localidad_Actual" id="id_localidad_2">
<option selected value="<?php echo $row->Localidad_Actual;?>"></option>
</select>
</div>

<script type="text/javascript">
  
    $(document).ready(function(){
    
        var localidad_2 = $('#id_localidad_2');
       
        $('#id_municipio_2').change(function(){
          var id_municipio_2 = $(this).val();        

            $.ajax({
              data: {id_municipio_2:id_municipio_2}, 
              dataType: 'html', 
              type: 'POST', 
              url: '../get_localidad_2.php', 

              }).done(function(data){   
                localidad_2.html(data);       
              });      
            
        });

    });
</script>

<div class="form-group col-md-12">
<label for="inputPassword4">Calle</label>
<input type="text" class="form-control" name="Calle_Actual" value="<?php echo $row->Calle_Actual;?>">
</div>

<div class="form-group col-md-3">
<label for="inputPassword4">No. Exterior</label>
<input type="text" class="form-control" name="No_Exterior" value="<?php echo $row->No_Exterior;?>">
</div>

<div class="form-group col-md-3">
<label for="inputPassword4">No. Interior</label>
<input type="text" class="form-control" name="No_Interior" value="<?php echo $row->No_Interior;?>" >
</div>

<div class="form-group col-md-6">
<label for="inputPassword4">Correo Electrónico</label>
<input type="email" class="form-control" name="Correo_Electronico" value="<?php echo $row->Correo_Electronico;?>" >
</div>

<div class="form-group col-md-3">
<label for="inputPassword4">Teléfono</label>
<input type="text" class="form-control" name="Telefono" value="<?php echo $row->Telefono;?>">
</div>

<div class="form-group col-md-3">
<label for="inputPassword4">Teléfono Movil</label>
<input type="text" class="form-control" name="Telefono_Movil" value="<?php echo $row->Telefono_Movil;?>">
</div>

<div class="form-group col-md-3">
<label for="inputPassword4">Teléfono de Emergencia</label>
<input type="text" class="form-control" name="Telefono_Emergencia" value="<?php echo $row->Telefono_Emergencia;?>">
</div>

<div class="form-group col-md-3">
<label for="inputPassword4">Tipo de sangre</label>
<input type="text" class="form-control" name="Sangre" value="<?php echo $row->Sangre;?>">
</div>

</div>
<!-- FILA 4-->


<hr style="height: 3px; background-color: #2C3E50 ; border-radius: 15px;"> 
<br>
<div class="row">
<button type="submit" class="btn btn-primary" value="save" style="border-radius: 25px;">
<i class="dw dw-check"></i>
Guardar</button>
</div>

</form>
</div>
<!-- FORMULARIO-->

<!--Fin del codigo de IMEI para los datos personales de los profesores -->

</div>
</div>

<!-- Container-fluid Ends-->
</div>

<!-- footer start-->
<footer class="footer">
<div class="container-fluid">
<div class="row">
<div class="col-md-6 footer-copyright">
<p class="mb-0">Copyright 2022-23 © IMEI / CEP</p>
</div>
<div class="col-md-6">
<p class="pull-right mb-0">Made by "Hack the box" with <i class="fa fa-heart font-secondary"></i></p>
</div>
</div>
</div>
</footer>
</div>
</div>
<?=$this->load->view('Include/base_js','',TRUE);?>


<!--Script de envio y otro js -->

<!-- SCRIPT DE ENVIO-->

<script type="text/javascript">

function mayusculas(e) {
    e.value = e.value.toUpperCase();
}




document.getElementById('getFile').onchange = function () 
{
$('#nombre_archivo').html(this.files[0].name)
}






$(document).ready(function() {


$("body").on('submit', '#formArchivos', function(event) {

  var base_url='<?php echo base_url(); ?>';


var formData = new FormData($("#formArchivos").get(0));


$("#smtArchivo").prop('disabled',true);

$.ajax({

url: base_url+"Profesores/Smt_Temario",
type: "POST",
//dataType: "json", 
data : formData,
contentType: false,
processData: false,
success: function (resultadoItem){

location.reload();

}

});

return false;



});

});


  



</script>
<!-- SCRIPT DE ENVIO-->



<!-- JS -->
<?=$this->load->view('Include/Js','',TRUE);?>
<!-- JS -->
<!--Script de envio y otro js -->

</body>
</html>