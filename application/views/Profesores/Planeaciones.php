
<html lang="en">
<head>
<title>PLANEACIONES</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Integra-Educa">
<meta name="keywords" content="Integra-Educa, Integra">
<meta name="author" content="Mike">

<?=$this->load->view('Include/base_css','',TRUE);?>

<!--IMEI -->
<!-- CSS -->
<?=$this->load->view('Include/Head','',TRUE);?>
<!-- CSS -->
<!--IMEI -->
</head>
<body>

<!-- Loader starts-->
<div class="loader-wrapper">
<div class="theme-loader">    
<div class="loader-p"></div>
</div>
</div>
<!-- Loader ends-->


<!-- page-wrapper Start-->
<div class="page-wrapper" id="pageWrapper">

<!-- Page Header Start-->
<?=$this->load->view('Profesores/Header','',TRUE);?>
<!-- Page Header Start-->


<!-- Page Body Start-->
<div class="page-body-wrapper horizontal-menu">
 
<!-- Page Sidebar Inicio-->
<?=$this->load->view('Profesores/Side_bar','',TRUE);?>
<!-- Page Sidebar Fin-->

<!-- TITULOS PAGINA-->
<div class="page-body">
<div class="container-fluid">
<div class="page-header">
<div class="row ">
<div class="col-sm-6 ">
<h3>Planeaciones</h3>
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="<?=base_url('Profesores/')?>">Inicio</a></li>
<li class="breadcrumb-item">Home</li>
</ol>
</div>
</div>
</div>
</div>
<!-- TITULOS PAGINA-->

<div class="row justify-content-center mr-2 ml-2">
<div class="col-sm-12 col-xl-12 col-lg-12">
</div>
<span class="badge badge-warning text-dark" style="font-size: 22px; border-radius: 25px;">-Sistema Integral de Información-</span>
</div>



<!-- Container-fluid starts-->
<div class="container-fluid general-widget">
<div class="row justify-content-center d-flex align-items-center">


<!--Codigo de IMEI -->
<!-- CONTENEDOR-->	
<div class="pd-20 card-box mb-30">

  <form autocomplete="off"  id="formArchivos" method="post">

<div class="form-group row justify-content-center mt-1">
  



<input type="text" class="form-control" required onkeyup="mayusculas(this);" name="nombre">
<small class="form-text text-muted text-center">
Coloque aqui el nombre de la planeación usando la nomeclatura Materia-Cuatrimestre-Carrera
<br>
<strong>Solo archivos PDF</strong>
</small>

<br><br><br>


<div class="custom-file">
<input type="file" name="archivo" id="getFile" required class="custom-file-input" style="border-radius: 35px;" accept="application/pdf">
<label class="custom-file-label" id="nombre_archivo">Elija su archivo</label>
<br><br>
</div>



</div>
<div class="btn-list text-center">
<input class="btn btn-info" type="submit" id="smtArchivo" value="Agregar">
</div>
</form>

<br>

<table class="table">
<thead class="thead-dark">
<tr>
<th scope="col" class="text-center">Id</th>
<th scope="col" class="text-center">Nombre del archivo</th>
<th scope="col" width="15%">Acciones</th>
</tr>
</thead>

<body>
 <?php foreach ($row->result() as $row){?>
<tr>
<th scope="row" class="text-center"><?= $row->Id ?></th>
<td class="text-center"><strong><?= $row->Nombre ?></strong></td>
<td>

<a class="btn btn-primary btn-xs" href="<?=base_url()?>Profesores/verArchivo/<?=$row->Id ?>" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i></a>

<a class="btn btn-danger btn-xs" href="<?=base_url()?>Profesores/deleteArchivo/<?=$row->Id ?>"><i class="fa fa-trash" aria-hidden="true"></i></a>



</td>
</tr>
        <?php
      }
      ?>
</tbody>
</table>

</div>
</div>
<!-- CONTENEDOR-->

<!--Codigo de IMEI -->

</div>
</div>

<!-- Container-fluid Ends-->
</div>

<!-- footer start-->
<footer class="footer">
<div class="container-fluid">
<div class="row">
<div class="col-md-6 footer-copyright">
<p class="mb-0">Copyright 2022-23 © IMEI / CEP</p>
</div>
<div class="col-md-6">
<p class="pull-right mb-0">Made by "Hack the box" with <i class="fa fa-heart font-secondary"></i></p>
</div>
</div>
</div>
</footer>
</div>
</div>
<?=$this->load->view('Include/base_js','',TRUE);?>


<!--Script de envio y otro js -->

<!-- SCRIPT DE ENVIO-->

<script type="text/javascript">

function mayusculas(e) {
    e.value = e.value.toUpperCase();
}




document.getElementById('getFile').onchange = function () 
{
$('#nombre_archivo').html(this.files[0].name)
}






$(document).ready(function() {


$("body").on('submit', '#formArchivos', function(event) {

  var base_url='<?php echo base_url(); ?>';


var formData = new FormData($("#formArchivos").get(0));


$("#smtArchivo").prop('disabled',true);

$.ajax({

url: base_url+"Profesores/Smt_Temario",
type: "POST",
//dataType: "json", 
data : formData,
contentType: false,
processData: false,
success: function (resultadoItem){

location.reload();

}

});

return false;



});

});


  



</script>
<!-- SCRIPT DE ENVIO-->



<!-- JS -->
<?=$this->load->view('Include/Js','',TRUE);?>
<!-- JS -->
<!--Script de envio y otro js -->

</body>
</html>