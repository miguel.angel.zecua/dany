<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model 
{

public function __construct() 
{
$this->load->database();
}
//-------------------------------------------
public function Permisos_View ($usuario)
{
$query = $this->db->query('select Permisos from alumno where Matricula='."'".$usuario."'");
return $query->row();
}
//-------------------------------------------
public function Eval_total ($Maestro)
{
$query = $this->db->query('select preg_1,preg_2,preg_3,preg_4,preg_5,preg_6,preg_7,preg_8,preg_9,preg_10,preg_11,preg_12,preg_13,preg_14,preg_15,preg_16,preg_17,preg_18,preg_19,preg_20,preg_21,preg_22,preg_23,preg_24 from evaluacion where matricula_Profesor='."'".$Maestro."'");
}
//-------------------------------------------
public function Datos_estadistica($Materia,$Maestro)
{
$query = $this->db->query('select  Matricula_Profesor, Id_Materia, Matricula_Alumno, round(AVG(preg_1),1) as preg_1, round(AVG(preg_2),1) as preg_2, round(AVG(preg_3),1) as preg_3, round(AVG(preg_4),1) as preg_4, round(AVG(preg_5),1) as preg_5, round(AVG(preg_6),1) as preg_6, round(AVG(preg_7),1) as preg_7, round(AVG(preg_8),1) as preg_8, round(AVG(preg_9),1) as preg_9, round(AVG(preg_10),1) as preg_10, round(AVG(preg_11),1) as preg_11, round(AVG(preg_12),1) as preg_12, round(AVG(preg_13),1) as preg_13, round(AVG(preg_14),1) as preg_14, round(AVG(preg_15),1) as preg_15, round(AVG(preg_16),1) as preg_16, round(AVG(preg_17),1) as preg_17, round(AVG(preg_18),1) as preg_18, round(AVG(preg_19),1) as preg_19, round(AVG(preg_20),1) as preg_20, round(AVG(preg_21),1) as preg_21, round(AVG(preg_22),1) as preg_22, round(AVG(preg_23),1) as preg_23, round(AVG(preg_24),1) as preg_24 FROM evaluacion where Id_Materia='."'".$Materia."'". ' and Matricula_Profesor='."'".$Maestro."'");
return $query->row();
}
//-------------------------------------------
public function Edit_notify_alum($id,$prioridad,$texto)
{
$data = array (
'Id' => $id,
'prioridad' => $prioridad,
'descripcion' => $texto
);
$this->db->where('Id', $id);
$this->db->update('Noty_alum_imei', $data);
}
//-------------------------------------------
public function Edit_notify_maestro($id,$prioridad,$texto)
{
$data = array (
'Id' => $id,
'prioridad' => $prioridad,
'descripcion' => $texto
);
$this->db->where('Id', $id);
$this->db->update('Noty_prof_imei', $data);
}
//-------------------------------------------
public function Delete_notify_alum($Matricula)
{
$this->db->where('Id', $Matricula);
$this->db->delete('Noty_alum_imei');
}
//-------------------------------------------
public function Delete_notify_maestro($Matricula)
{
$this->db->where('Id', $Matricula);
$this->db->delete('Noty_prof_imei');
}
//-------------------------------------------
public function Read_notify_alumn($Matricula)
{
$query = $this->db->query('select Id,descripcion,prioridad from Noty_alum_imei where Matricula_Alumno='."'".$Matricula."'");
$result = $query->result_array();
return $result;
}
//-------------------------------------------
public function Read_notify_maestro($Matricula)
{
$query = $this->db->query('select Id,descripcion,prioridad from Noty_prof_imei where Matricula_Profesor='."'".$Matricula."'");
$result = $query->result_array();
return $result;
}
//-------------------------------------------
function New_notify_alum($new_usuario,$new_prioridad,$new_texto) 
{
$data = array (
'Matricula_Alumno' => $new_usuario ,
'descripcion' => $new_texto,
'prioridad' => $new_prioridad 
);
$this->db->insert('Noty_alum_imei', $data);
}
//-------------------------------------------
function New_notify_maestro($new_usuario,$new_prioridad,$new_texto) 
{
$data = array (
'Matricula_Profesor' => $new_usuario ,
'descripcion' => $new_texto,
'prioridad' => $new_prioridad 
);
$this->db->insert('Noty_prof_imei', $data);
}
//-------------------------------------------
public function Horario_of_alumno($Matricula)
{
$query = $this->db->query('select materias.Nombre,concat(profesores.Nombres," ",profesores.Apellido_Paterno," ",profesores.Apellido_Materno)as profesor,Dia,Hora_Inicio,Hora_Fin from calificaciones inner join materias inner join profesores where calificaciones.Id_Materia=materias.Id_Materia and calificaciones.Matricula_Profesor=profesores.Matricula and Matricula_Alumno='."'".$Matricula."'". ' order by calificaciones.Dia,calificaciones.Hora_Inicio');
return $query;
}
//-------------------------------------------
public function Horario_of_profesor($Matricula)
{
$query = $this->db->query('select materias.Id_Materia,concat(profesores.Nombres," ",profesores.Apellido_Paterno," ",profesores.Apellido_Materno)as profe,materias.Nombre,calificaciones.Cuatrimestre,Dia,Turno,Hora_Inicio,Hora_Fin FROM calificaciones INNER JOIN materias inner join profesores WHERE calificaciones.Id_Materia=materias.Id_Materia and calificaciones.Matricula_Profesor=profesores.Matricula and Matricula_Profesor='."'".$Matricula."'". ' GROUP BY Id_Materia,calificaciones.Id_Materia order by calificaciones.Dia,calificaciones.Hora_Inicio');
return $query;
}
//-------------------------------------------
public function Calificaciones_ciclo($Ciclo)
{
$query = $this->db->query('select matricula_alumno,concat(nombres," ",apellido_paterno," ",apellido_materno)as nombre, round(AVG(final),1) as promedio,periodo from historial_calificaciones inner join alumno where historial_calificaciones.Matricula_alumno=alumno.matricula and periodo='."'".$Ciclo."'". ' group by matricula_alumno');
return $query;
}
//-------------------------------------------
public function Historiales_View($Matricula)
{
$query = $this->db->query('select materias.nombre,materias.Creditos,historial_calificaciones.Final,historial_calificaciones.Cuatrimestre,historial_calificaciones.Periodo from historial_calificaciones inner join materias where historial_calificaciones.Id_Materia=materias.Id_Materia and historial_calificaciones.Matricula_Alumno='."'".$Matricula."'". 'ORDER BY historial_calificaciones.Cuatrimestre');
return $query;
}
//-------------------------------------------
public function Horario_maestros()
{
$query = $this->db->query('select calificaciones.Matricula_Profesor,concat(profesores.Nombres," ",profesores.Apellido_Paterno," ",profesores.Apellido_Materno) as nombre from calificaciones inner join profesores where calificaciones.Matricula_Profesor=profesores.Matricula GROUP by calificaciones.Matricula_Profesor');
return $query;
}
//-------------------------------------------
public function Horario_alumnos()
{
$query = $this->db->query('select calificaciones.matricula_alumno,concat(alumno.nombres," ",alumno.Apellido_Paterno," ",alumno.Apellido_Materno) as nombre, calificaciones.Cuatrimestre,alumno.Carrera,calificaciones.turno from calificaciones inner join alumno where calificaciones.Matricula_Alumno=alumno.Matricula GROUP by calificaciones.Matricula_Alumno');
return $query;
}
//-------------------------------------------
public function Ciclos_escolares()
{
$query = $this->db->query('select periodo from historial_calificaciones GROUP by periodo');
return $query;
}
//-------------------------------------------
public function Historiales()
{
$query = $this->db->query('select alumno.Matricula,concat(alumno.Nombres," ",alumno.Apellido_Paterno," ",alumno.Apellido_Materno) as Nombre,alumno.Carrera,round(AVG(Final),1) as Promedio,alumno.escuela from historial_calificaciones inner join alumno WHERE historial_calificaciones.Matricula_Alumno=alumno.Matricula GROUP by historial_calificaciones.Matricula_Alumno');
return $query;
}
//-------------------------------------------

public function Pagos_Alumnos_Todos()
{
$query = $this->db->query('select concat(alumno.Nombres," ",alumno.Apellido_Paterno," ",alumno.Apellido_Materno)as Nombre,alumno.Carrera,mes_1,fecha_1,tipo_dep_1,status_1,mes_2,fecha_2,tipo_dep_2,status_2,mes_3,fecha_3,tipo_dep_3,status_3,mes_4,fecha_4,tipo_dep_4,status_4,observaciones from alumnos_pagos inner join alumno where alumnos_pagos.Matricula_Alumno=alumno.Matricula');
$result = $query->result_array();
return $result;
}
//-------------------------------------------
public function Pagos_Alumnos_Fechas($start_date, $end_date)
{
$query = $this->db->query('select concat(alumno.Nombres," ",alumno.Apellido_Paterno," ",alumno.Apellido_Materno)as Nombre,alumno.Carrera,mes_1,fecha_1,tipo_dep_1,status_1,mes_2,fecha_2,tipo_dep_2,status_2,mes_3,fecha_3,tipo_dep_3,status_3,mes_4,fecha_4,tipo_dep_4,status_4,observaciones from alumnos_pagos inner join alumno where alumnos_pagos.Matricula_Alumno=alumno.Matricula and fecha_1 BETWEEN ' ."'".$start_date."'". ' AND ' ."'".$end_date."'". ' OR fecha_2 BETWEEN ' ."'".$start_date."'". ' AND ' ."'".$end_date."'". ' OR fecha_3 BETWEEN ' ."'".$start_date."'". ' AND ' ."'".$end_date."'". ' OR fecha_4 BETWEEN ' ."'".$start_date."'". ' AND ' ."'".$end_date."'". ' OR fecha_5 BETWEEN ' ."'".$start_date."'". ' AND ' ."'".$end_date."'". ' GROUP BY Nombre,carrera');
$result = $query->result_array();
return $result;
}
//-------------------------------------------


//consulta Ernesto
public function Pagos_Alumnos()
{
$query = $this->db->query('select alumnos_pagos.Matricula_alumno,concat(nombres," ",apellido_paterno," ",apellido_materno) as nombre,round(AVG(final),0) as promedio,alumno.Turno,alumno.carrera,alumno.Conafe,mes_1,mes_2,mes_3,mes_4,reinscripcion from alumnos_pagos inner join calificaciones inner join alumno where alumnos_pagos.Matricula_alumno=calificaciones.matricula_alumno and calificaciones.matricula_alumno=alumno.matricula group by matricula_alumno');
return $query;
}
//consulta Ernesto

public function Calificaciones()
{
$query = $this->db->query('select matricula_alumno,concat(nombres," ",apellido_paterno," ",apellido_materno)as nombre, round(AVG(final),1) as promedio  from calificaciones inner join alumno where calificaciones.Matricula_alumno=alumno.matricula group by matricula_alumno');
return $query;
}


//-------------------------------------------

function getData($id) {
$query = $this->db->query('select materias.nombre, concat(profesores.nombres," ",profesores.apellido_paterno," " ,profesores.apellido_materno) as profesor,id_salon,calificacion_1,calificacion_2,calificacion_3,final from calificaciones inner join materias inner join profesores where matricula_alumno='."'".$id."'".' and calificaciones.id_materia=materias.id_materia and calificaciones.matricula_profesor=profesores.matricula');
return $query;
}

//-------------------------------------------

function getData_2($id,$periodo) {
$query = $this->db->query('select materias.nombre, concat(profesores.nombres," ",profesores.apellido_paterno," " ,profesores.apellido_materno) as profesor,id_salon,calificacion_1,calificacion_2,calificacion_3,final from historial_calificaciones inner join materias inner join profesores where matricula_alumno='."'".$id."'".' and historial_calificaciones.id_materia=materias.id_materia and historial_calificaciones.matricula_profesor=profesores.matricula and periodo='."'".$periodo."'");
return $query;
}

//-------------------------------------------

function getData_ciclo($id,$periodo) {
$query = $this->db->query('select materias.nombre, concat(profesores.nombres," ",profesores.apellido_paterno," " ,profesores.apellido_materno) as profesor,id_salon,calificacion_1,calificacion_2,calificacion_3,final from historial_calificaciones inner join materias inner join profesores where matricula_alumno='."'".$id."'".' and historial_calificaciones.id_materia=materias.id_materia and historial_calificaciones.matricula_profesor=profesores.matricula and periodo='."'".$periodo."'");
return $query;
}

//-------------------------------------------

public function Pago_Alumno($Matricula)
{
$query = $this->db->query('select alumnos_pagos.Matricula_alumno,concat(nombres," ",apellido_paterno," ",apellido_materno) as nombre,alumno.Turno,alumno.carrera,alumno.Conafe,mes_1,mes_2,mes_3,mes_4,reinscripcion,status_1,status_2,status_3,status_4,status_5,fecha_1,fecha_2,fecha_3,fecha_4,fecha_5,tipo_dep_1,tipo_dep_2,tipo_dep_3,tipo_dep_4,tipo_dep_5,observaciones from alumnos_pagos inner join calificaciones inner join alumno where alumnos_pagos.Matricula_alumno=calificaciones.matricula_alumno and calificaciones.matricula_alumno=alumno.matricula and alumnos_pagos.Matricula_Alumno='."'".$Matricula."'");
return $query->row();
}

public function Pago_Alumno_Res($Matricula)
{
$query = $this->db->query('select round(AVG(final),1) as promedio from historial_calificaciones where Matricula_Alumno='."'".$Matricula."'");
return $query->row();
}

function Actualizar_Pagos($Matricula) 
{
$data = array 
(
'mes_1' => $_POST['enero'],  
'status_1'=>$_POST['status_1'], 
'fecha_1'=>$_POST['fecha_1'],    
'tipo_dep_1'=>$_POST['tip_1'],
'mes_2' => $_POST['febrero'],  
'status_2'=>$_POST['status_2'],  
'fecha_2'=>$_POST['fecha_2'],    
'tipo_dep_2'=>$_POST['tip_2'],
'mes_3' => $_POST['marzo'],    
'status_3'=>$_POST['status_3'],  
'fecha_3'=>$_POST['fecha_3'],    
'tipo_dep_3'=>$_POST['tip_3'],
'mes_4' => $_POST['abril'],    
'status_4'=>$_POST['status_4'],  
'fecha_4'=>$_POST['fecha_4'],    
'tipo_dep_4'=>$_POST['tip_4'],
'reinscripcion' => $_POST['reinscripcion'],
'status_5'=>$_POST['status_5'],
'fecha_5'=>$_POST['fecha_5'],     
'tipo_dep_5'=>$_POST['tip_5'],
'observaciones'=>$_POST['Observaciones']
);

$this->db->where('matricula_alumno', $Matricula);
$this->db->update('alumnos_pagos', $data);
}


public function Datos($Matricula)
{
$query = $this->db->query('SELECT * FROM alumno  inner join salones WHERE `Matricula` =' ."'". $Matricula. "'" . ' and alumno.Id_salon=salones.Id_Salon');
return $query->row();
}

//-------------------------------------------
public function Calif_Finales($Matricula)
{
$query = $this->db->query('select round (AVG(calificacion_1))as calif_1, round (AVG(calificacion_2))as calif_2,round (AVG(calificacion_3))as calif_3,round (AVG(final),1)as final  from calificaciones where Matricula_alumno='."'".$Matricula."'");
return $query->row();
}
//-------------------------------------------
public function Calif_Finales_2($Matricula,$Periodo)
{
$query = $this->db->query('select round (AVG(calificacion_1))as calif_1, round (AVG(calificacion_2))as calif_2,round (AVG(calificacion_3))as calif_3,round (AVG(final),1)as final  from historial_calificaciones where Matricula_alumno='."'".$Matricula."'". ' and periodo='."'".$Periodo."'");
return $query->row();
}
//-------------------------------------------

public function Grupos_All()
{
$query = $this->db->query('select matricula_profesor,concat(nombres," ",apellido_paterno," ",apellido_materno)as nombre from calificaciones inner join profesores where calificaciones.matricula_profesor=profesores.matricula group by matricula_profesor');
return $query;
}


public function Horario($Matricula)
{
$query = $this->db->query('select materias.nombre,salones.Nombre_carrera,calificaciones.Cuatrimestre,calificaciones.Turno,Dia,Hora_Inicio,Hora_Fin from calificaciones inner join materias inner join salones where Matricula_Profesor=' ."'".$Matricula."'". ' and calificaciones.Id_materia=materias.Id_materia and calificaciones.Id_salon=salones.Id_salon group by calificaciones.Id_salon,calificaciones.Id_materia order by DAYOFWEEK(Dia)');
return $query;
}

public function Name_maestro($Matricula)
{
$query = $this->db->query('select concat(nombres," ",apellido_paterno," ",apellido_materno) as nombre from profesores where matricula='."'".$Matricula."'");
return $query->row();
}

public function Listas_alumnos($Materia,$Matricula) 
{
$query = $this->db->query('select concat(alumno.Nombres," ",alumno.Apellido_Paterno," ",alumno.Apellido_Materno)as Alumno from calificaciones inner join alumno where calificaciones.Id_Materia=' ."'".$Materia."'" . 'and calificaciones.Matricula_Alumno= alumno.Matricula and calificaciones.Matricula_Profesor='."'".$Matricula."'" . ' ');
return $query;
}

public function Mat_datos($Materia,$Matricula) 
{
$query = $this->db->query('select concat(profesores.Nombres," ",profesores.Apellido_Paterno," ", profesores.Apellido_Materno )as Prof, materias.Nombre,calificaciones.Turno,calificaciones.Cuatrimestre,materias.Carrera, alumno.escuela from calificaciones inner join profesores INNER join materias INNER JOIN alumno WHERE calificaciones.Matricula_Profesor= profesores.Matricula and calificaciones.Matricula_Profesor=' ."'".$Matricula."'". ' and calificaciones.Id_Materia=materias.Id_Materia AND calificaciones.Matricula_Alumno=alumno.Matricula and calificaciones.Id_Materia=' ."'".$Materia."'" . ' LIMIT 1;');
return $query;
}
}