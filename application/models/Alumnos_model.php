<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alumnos_model extends CI_Model 
{

public function __construct() 
{
$this->load->database();
}

//--------------------------------------------------
public function Env_evalu()
{
$data = array (
'Id_Materia' => $this->input->post('materia'),
'Matricula_Profesor' => $this->input->post('profesor'),
'Matricula_Alumno' => $this->input->post('matricula'),

'preg_1' => $this->input->post('radio1'),'preg_2' => $this->input->post('radio2'),
'preg_3' => $this->input->post('radio3'),'preg_4' => $this->input->post('radio4'),
'preg_5' => $this->input->post('radio5'),'preg_6' => $this->input->post('radio6'),
'preg_7' => $this->input->post('radio7'),'preg_8' => $this->input->post('radio8'),
'preg_9' => $this->input->post('radio9'),'preg_10' => $this->input->post('radio10'),
'preg_11' => $this->input->post('radio11'),'preg_12' => $this->input->post('radio12'),
'preg_13' => $this->input->post('radio13'),'preg_14' => $this->input->post('radio14'),
'preg_15' => $this->input->post('radio15'),'preg_16' => $this->input->post('radio16'),
'preg_17' => $this->input->post('radio17'),'preg_18' => $this->input->post('radio18'),
'preg_19' => $this->input->post('radio19'),'preg_20' => $this->input->post('radio20'),
'preg_21' => $this->input->post('radio21'),'preg_22' => $this->input->post('radio22'),
'preg_23' => $this->input->post('radio23'),'preg_24' => $this->input->post('radio24'),
'observaciones' => $this->input->post('observaciones')

);
$this->db->insert('evaluacion', $data);

$data2 = array (
'Id_Materia' => $this->input->post('materia'),
'Matricula_Profesor' => $this->input->post('profesor'),
'Matricula_Alumno' => $this->input->post('matricula'),
);
$this->db->set('evaluacion', 0);
$this->db->where('Id_Materia', $this->input->post('materia'));
$this->db->where('Matricula_Profesor', $this->input->post('profesor'));
$this->db->where('matricula_alumno', $this->input->post('matricula') );
$this->db->update('calificaciones');

}
//--------------------------------------------------

public function Datos($Matricula)
{
$query = $this->db->query('SELECT * FROM alumno  inner join salones WHERE `Matricula` =' ."'". $Matricula. "'" . ' and alumno.Id_salon=salones.Id_Salon');
return $query->row();
}
//--------------------------------------------------
public function Nombres_maestros($Matricula)
{

$query = $this->db->query('select materias.Nombre,concat(profesores.Nombres," ",profesores.Apellido_Paterno," ",profesores.Apellido_Materno)as Maestro, materias.Id_Materia,calificaciones.Matricula_Profesor,evaluacion FROM calificaciones INNER JOIN profesores INNER JOIN materias WHERE calificaciones.Matricula_Profesor=profesores.Matricula AND calificaciones.Id_Materia=materias.Id_Materia AND Matricula_Alumno='."'".$Matricula."'");
return $query;
}
//--------------------------------------------------
public function Mach_Crimi($Matricula)
{
$query = $this->db->query('select count(Matricula_Alumno) AS Resultado from Calif_Dip_Crimi WHERE Matricula_Alumno='."'".$Matricula."'");
return $query->row();
}
//--------------------------------------------------

public function Mach_Criminalistica($Matricula)
{
$query = $this->db->query('select count(Alumno) AS Resultado from Diplomado_criminalistica WHERE Alumno='."'".$Matricula."'");
return $query->row();
}
//--------------------------------------------------
public function Mach_Derecho($Matricula)
{
$query = $this->db->query('select count(Matricula_Alumno) AS Resultado from diplomados_derecho WHERE Matricula_Alumno='."'".$Matricula."'");
return $query->row();
}
//--------------------------------------------------
public function Calificaciones_Crimi($Matricula)
{

$query = $this->db->query('select Calif_Dip_Crimi.Matricula_Alumno,concat(alumno.Nombres," ",alumno.Apellido_Paterno," ",alumno.Apellido_Materno) as Nombre,Nom_mod01,calificacion_1,Nom_mod02,calificacion_2,Nom_mod03,calificacion_3,Nom_mod04,calificacion_4,Nom_mod05,calificacion_5,Nom_mod06,calificacion_6,Nom_mod07,calificacion_7,Nom_mod08,calificacion_8,Nom_mod09,calificacion_9,Nom_mod10,calificacion_10,Nom_mod11,calificacion_11,Nom_mod12,calificacion_12,Final from diplomados_crimi INNER join Calif_Dip_Crimi INNER join alumno where Calif_Dip_Crimi.Matricula_Alumno=alumno.Matricula and Calif_Dip_Crimi.Matricula_Alumno='."'".$Matricula."'");
return $query;
}
//--------------------------------------------------
public function Calificaciones_Criminalistica($Matricula)
{

$query = $this->db->query('select Diplomado_criminalistica_nombres.Materia,Calificacion from Diplomado_criminalistica inner join Diplomado_criminalistica_nombres where Diplomado_criminalistica.Materia=Diplomado_criminalistica_nombres.Id and Alumno='."'".$Matricula."'".' order by Materia ASC');
return $query;
}


//--------------------------------------------------
public function Calificaciones_Derecho($Matricula)
{

$query = $this->db->query('select calificacion_1,calificacion_2,calificacion_3,final from diplomados_derecho where Matricula_Alumno='."'".$Matricula."'");
return $query;
}
//--------------------------------------------------
public function Datos_sidebar($Matricula)
{
$query = $this->db->query('select concat(alumno.Nombres," ",alumno.Apellido_Paterno," ",alumno.Apellido_Materno)as Nombre,Matricula,Carrera,escuela,round(AVG(historial_calificaciones.Final),1)AS Promedio from alumno inner join historial_calificaciones where alumno.Matricula=historial_calificaciones.Matricula_Alumno and alumno.Matricula='."'".$Matricula."'".' LIMIT 1');
return $query->row();
}
//--------------------------------------------------

function Reset_password($Matricula) 
{
$data = array 
(
'Password' => $this->input->post('Password_1')
);
$this->db->where('Matricula', $Matricula);
$this->db->update('alumno', $data);
}

//--------------------------------------------------

public function Calificaciones($Matricula)
{

$query = $this->db->query('select materias.nombre, concat(profesores.nombres," ",profesores.apellido_paterno," " ,profesores.apellido_materno) as profesor,id_salon,calificacion_1,calificacion_2,calificacion_3,final from calificaciones inner join materias inner join profesores where matricula_alumno='."'".$Matricula."'".' and calificaciones.id_materia=materias.id_materia and calificaciones.matricula_profesor=profesores.matricula');
return $query;
}

//--------------------------------------------------
public function Pagos_Alumno($Matricula)
{
$query = $this->db->query('select * from alumnos_pagos WHERE Matricula_Alumno='."'".$Matricula."'");
return $query->row();
}
//--------------------------------------------------
public function Historial($Matricula)
{
$query = $this->db->query('select materias.nombre,materias.Creditos,historial_calificaciones.Final,historial_calificaciones.Cuatrimestre,historial_calificaciones.Periodo from historial_calificaciones inner join materias where historial_calificaciones.Id_Materia=materias.Id_Materia and historial_calificaciones.Matricula_Alumno='."'".$Matricula."'". 'ORDER BY historial_calificaciones.Cuatrimestre;');
return $query;
}
//--------------------------------------------------

public function Horario($Matricula)
{
$query = $this->db->query('select materias.nombre, concat(profesores.nombres," ",profesores.apellido_paterno," " ,profesores.apellido_materno) as profesor,dia,hora_inicio,hora_fin,calificaciones.turno from calificaciones inner join materias inner join profesores where matricula_alumno=' ."'".$Matricula."'". 'and calificaciones.id_materia=materias.id_materia and calificaciones.matricula_profesor=profesores.matricula order by dia,calificaciones.hora_inicio');
return $query;
}

//--------------------------------------------------

function Actualizar($Matricula) 
{
$data = array 
(
'Nombres' => $this->input->post('Nombres'),
'Apellido_Paterno' => $this->input->post('Apellido_Paterno'),
'Apellido_Materno' => $this->input->post('Apellido_Materno'),
'Fecha_Nacimiento' => $this->input->post('Fecha_Nacimiento'),
'Sexo' => $this->input->post('Sexo'),
'Curp' => $this->input->post('Curp'),
'Estado' => $this->input->post('Estado'),
'Municipio' => $this->input->post('Municipio'),
'Localidad' => $this->input->post('Localidad'),
'Codigo_Postal' => $this->input->post('Codigo_Postal'),
'Estado_Actual' => $this->input->post('Estado_Actual'),
'Municipio_Actual' => $this->input->post('Municipio_Actual'),
'Localidad_Actual' => $this->input->post('Localidad_Actual'),
'Calle_Actual' => $this->input->post('Calle_Actual'),
'No_Exterior' => $this->input->post('No_Exterior'),
'No_Interior' => $this->input->post('No_Interior'),
'Correo_Electronico' => $this->input->post('Correo_Electronico'),
'Telefono' => $this->input->post('Telefono'),
'Telefono_Movil' => $this->input->post('Telefono_Movil'),
'Telefono_Emergencia' => $this->input->post('Telefono_Emergencia'),
'Sangre' => $this->input->post('Sangre'),
'Nombre_tutor' => $this->input->post('Nombre_tutor'),
'Telefono_Tutor' => $this->input->post('Telefono_Tutor'),
'Domicilio' => $this->input->post('Domicilio')
);

$this->db->where('Matricula', $Matricula);
$this->db->update('alumno', $data);


}

//--------------------------------------------------
public function Promedio($Matricula)
{
$query = $this->db->query('select round(AVG(Final),1) as promedio from historial_calificaciones WHERE Matricula_Alumno='."'".$Matricula."'");
return $query->row();
}
//--------------------------------------------------
public function Credito($Matricula)
{
$query = $this->db->query('select SUM(materias.Creditos)as Creditos from historial_calificaciones inner join materias where historial_calificaciones.Id_Materia=materias.Id_Materia and historial_calificaciones.Matricula_Alumno=' ."'".$Matricula."'". ' LIMIT 1');
return $query->row();
}
//--------------------------------------------------

public function Calif_Finales($Matricula)
{
$query = $this->db->query('select round (AVG(calificacion_1))as calif_1, round (AVG(calificacion_2))as calif_2,round (AVG(calificacion_3))as calif_3,round (AVG(final),1)as final  from calificaciones where Matricula_alumno='."'".$Matricula."'");
return $query->row();
}


//--------------------------------------------------

public function Main($matricula)
{
$result = $this->db->query("SELECT * FROM alumno WHERE matricula ='" .$matricula ."' LIMIT 1");

if($result->num_rows() > 0){
	return $result->row();
}else{
return null;
}

}

//--------------------------------------------------

public function Escuela($matricula) 
{
$result = $this->db->query("SELECT escuela FROM alumno WHERE matricula ='".$matricula ."' LIMIT 1");
return $result;	
}

//--------------------------------------------------
public function Pagos($matricula)
{
$query = $this->db->query("SELECT mes_1, mes_2,mes_3,mes_4,reinscripcion,Matricula_Alumno,observaciones,periodo FROM alumnos_pagos WHERE Matricula_Alumno='".$matricula ."' LIMIT 1");
return $query;
}
//--------------------------------------------------
//FIN DE CLASE

}
