<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profesores_model extends CI_Model 
{

public function __construct() 
{
 
parent::__construct();
//LOGIN ARRIBA
$this->load->database();

}

//--------------------------------------------------

public function Datos($Matricula)
{
$query = $this->db->query('SELECT * FROM profesores WHERE `Matricula` =' ."'". $Matricula. "'");
return $query->row();
}

//--------------------------------------------------
public function Datos_sidebar($Matricula)
{
$query = $this->db->query('select concat(profesores.Nombres," ",profesores.Apellido_Paterno," ",profesores.Apellido_Materno) as Nombre,calificaciones.Matricula_Profesor, count(DISTINCT Id_Salon) as Salones,count(DISTINCT Id_Materia) as Materias,count(DISTINCT Matricula_Alumno) as Alumnos from calificaciones inner join profesores WHERE calificaciones.Matricula_Profesor=profesores.Matricula and matricula_profesor='."'".$Matricula."'".' group by Matricula_profesor having Alumnos >1');
return $query->row();
}
//--------------------------------------------------


public function Grupos($Matricula)
{
$query = $this->db->query('select materias.nombre,salones.nombre_carrera as carrera,calificaciones.cuatrimestre,calificaciones.turno,calificaciones.id_salon,calificaciones.id_materia from calificaciones inner join materias inner join salones where materias.Id_materia=calificaciones.id_materia and salones.Id_salon=calificaciones.Id_salon and Matricula_Profesor='."'".$Matricula."' group by Id_salon,calificaciones.Id_materia");
return $query;
}

//--------------------------------------------------

function Reset_password($Matricula) 
{
$data = array 
(
'Password' => $this->input->post('Password_1')
);
$this->db->where('Matricula', $Matricula);
$this->db->update('profesores', $data);
}

//--------------------------------------------------

public function Lista($Salon,$Matricula,$Materia)
{

$query = $this->db->query('select calificaciones.Matricula_Alumno,concat(Nombres," ",Apellido_Paterno, " ",Apellido_Materno)as Alumno,calificaciones.matricula_profesor,calificaciones.id_materia,calificacion_1,calificacion_2,calificacion_3,final,calificaciones.id_Salon from calificaciones inner join alumno where calificaciones.Matricula_Alumno=alumno.Matricula and calificaciones.Id_salon=' ."'".$Salon."'". ' and calificaciones.id_materia=' ."'".$Materia."'" . 'and calificaciones.matricula_profesor='."'".$Matricula."' group by calificaciones.Matricula_Alumno order by Apellido_Paterno");
return $query;
}


//--------------------------------------------------
public function Lista_Dip_Crimi()
{

$query = $this->db->query('select Calif_Dip_Crimi.Matricula_Alumno,concat(alumno.Nombres," ",alumno.Apellido_Paterno," ",alumno.Apellido_Materno) as Nombre,Nom_mod01,calificacion_1,Nom_mod02,calificacion_2,Nom_mod03,calificacion_3,Nom_mod04,calificacion_4,Nom_mod05,calificacion_5,Nom_mod06,calificacion_6,Nom_mod07,calificacion_7,Nom_mod08,calificacion_8,Nom_mod09,calificacion_9,Nom_mod10,calificacion_10,Nom_mod11,calificacion_11,Nom_mod12,calificacion_12,Final from diplomados_crimi INNER join Calif_Dip_Crimi INNER join alumno where Calif_Dip_Crimi.Matricula_Alumno=alumno.Matricula ORDER by alumno.Apellido_Paterno');
return $query;
}

//--------------------------------------------------
public function Lista_Diplomado_Criminalistica($Matricula)
{

$query = $this->db->query('select Diplomado_criminalistica.Materia as Materia_Id,Diplomado_criminalistica_nombres.Materia,Profesor,alumno as Alumno_Id,concat(alumno.Nombres," ",alumno.Apellido_Paterno," ",alumno.Apellido_Materno)as Alumno,Calificacion from Diplomado_criminalistica INNER JOIN Diplomado_criminalistica_nombres INNER JOIN alumno where Diplomado_criminalistica.Materia=Diplomado_criminalistica_nombres.Id And Diplomado_criminalistica.Alumno=alumno.Matricula AND Profesor='."'".$Matricula."'");
return $query;
}


//--------------------------------------------------
public function Lista_Dip_Derecho()
{

$query = $this->db->query('select Matricula_Alumno,concat(alumno.Nombres," ",alumno.Apellido_Paterno," ",alumno.Apellido_Materno) as Nombre,calificacion_1,calificacion_2,calificacion_3,final from diplomados_derecho inner join alumno where diplomados_derecho.Matricula_Alumno=alumno.Matricula ORDER by alumno.Apellido_Paterno');
return $query;
}
//--------------------------------------------------

public function Calificaciones($Matricula)
{

$query = $this->db->query('select materias.nombre, concat(profesores.nombres," ",profesores.apellido_paterno," " ,profesores.apellido_materno) as profesor,id_salon,calificacion_1,calificacion_2,calificacion_3,final from calificaciones inner join materias inner join profesores where matricula_alumno='."'".$Matricula."'".' and calificaciones.id_materia=materias.id_materia and calificaciones.matricula_profesor=profesores.matricula');
return $query;
}

//--------------------------------------------------

public function Horario($Matricula)
{
$query = $this->db->query('select materias.nombre,salones.Nombre_carrera,calificaciones.Cuatrimestre,calificaciones.Turno,Dia,Hora_Inicio,Hora_Fin from calificaciones inner join materias inner join salones where Matricula_Profesor=' ."'".$Matricula."'". ' and calificaciones.Id_materia=materias.Id_materia and calificaciones.Id_salon=salones.Id_salon group by calificaciones.Id_salon order by DAYOFWEEK(Dia)');
return $query;
}

//--------------------------------------------------
public function Insert_Calif($calif_1,$calif_2,$calif_3,$final,$alumno,$materia,$profesor)
{


$data = array (
'Calificacion_1' => $calif_1,
'Calificacion_2' => $calif_2,
'Calificacion_3' => $calif_3,
'Final' => $final
);

$this->db->where('Matricula_Alumno', $alumno);
$this->db->where('Id_Materia', $materia);
$this->db->where('Matricula_Profesor', $profesor);
$this->db->update('calificaciones', $data);


}

//--------------------------------------------------
public function Insert_Calif_Dip_Crimi($calif_1,$calif_2,$calif_3,$calif_4,$calif_5,$calif_6,$calif_7,$calif_8,$calif_9,$calif_10,$calif_11,$calif_12,$final,$alumno)
{

$data = array (
'Calificacion_1' => $calif_1,
'Calificacion_2' => $calif_2,
'Calificacion_3' => $calif_3,
'Calificacion_4' => $calif_4,
'Calificacion_5' => $calif_5,
'Calificacion_6' => $calif_6,
'Calificacion_7' => $calif_7,
'Calificacion_8' => $calif_8,
'Calificacion_9' => $calif_9,
'Calificacion_10' => $calif_10,
'Calificacion_11' => $calif_11,
'Calificacion_12' => $calif_12,
'Final' => $final
);

$this->db->where('Matricula_Alumno', $alumno);
$this->db->update('Calif_Dip_Crimi', $data);

}

//--------------------------------------------------
public function Insert_Calif_Diplomado_Criminalistica($Materia_Id,$Profesor,$Alumno_Id,$Calificacion)
{

$data = array (
'Calificacion' => $Calificacion
);

$this->db->where('Materia', $Materia_Id);

$this->db->update('diplomado_criminalistica', $data);

}


//--------------------------------------------------
public function Insert_Calificaciones_Dip_Derecho($calif_1,$calif_2,$calif_3,$final,$alumno)
{

$data = array (
'calificacion_1' => $calif_1,
'calificacion_2' => $calif_2,
'calificacion_3' => $calif_3,
'final' => $final
);

$this->db->where('Matricula_Alumno', $alumno);
$this->db->update('diplomados_derecho', $data);

}
//--------------------------------------------------

public function Insert_Tem($data)
{
	$this->db->insert('tb_temarios', $data);

//return $this-db->insert_id();

}

//--------------------------------------------------

public function Planeacion($Matricula)
{
$query = $this->db->query('select*from tb_temarios where Matricula_profesor='."'".$Matricula."'");
return $query;
}

public function getArchivo($Id)
{
$query = $this->db->query("select * from tb_temarios where Id = ?",array($Id));


return $query->row_array();
}

//--------------------------------------------------

public function deleteArch($Id)
{
$this->db->where('Id', $Id);
$this->db->delete('tb_temarios');
}

//--------------------------------------------------

function Actualizar($Matricula) 
{
$data = array 
(
'Nombres' => $this->input->post('Nombres'),
'Apellido_Paterno' => $this->input->post('Apellido_Paterno'),
'Apellido_Materno' => $this->input->post('Apellido_Materno'),
'Estado' => $this->input->post('Estado'),
'Municipio' => $this->input->post('Municipio'),
'Localidad' => $this->input->post('Localidad'),
'Codigo_Postal' => $this->input->post('Codigo_Postal'),
'Estado_Actual' => $this->input->post('Estado_Actual'),
'Municipio_Actual' => $this->input->post('Municipio_Actual'),
'Localidad_Actual' => $this->input->post('Localidad_Actual'),
'Calle_Actual' => $this->input->post('Calle_Actual'),
'No_Exterior' => $this->input->post('No_Exterior'),
'No_Interior' => $this->input->post('No_Interior'),
'Correo_Electronico' => $this->input->post('Correo_Electronico'),
'Telefono' => $this->input->post('Telefono'),
'Telefono_Movil' => $this->input->post('Telefono_Movil'),
'Telefono_Emergencia' => $this->input->post('Telefono_Emergencia'),
'Sangre' => $this->input->post('Sangre')

);

$this->db->where('Matricula', $Matricula);
$this->db->update('profesores', $data);
}

//--------------------------------------------------

//FIN DE CLASE

//MODELOS DEL LOGIN PROFESORES 

public function Main($matricula)
{
$result = $this->db->query("SELECT * FROM profesores WHERE matricula ='" .$matricula ."' LIMIT 1");

if($result->num_rows() > 0){
	return $result->row();
}else{
return null;
}

}


}
