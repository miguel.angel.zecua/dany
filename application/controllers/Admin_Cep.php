<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_Cep extends CI_Controller 
{

public function __construct() 
{
parent:: __construct();
$this->load->model('Admin_model_cep');
}

//-------------------------------------------

public function index()
{
$this->load->view('Admin_Cep/Inicio');
}

//NOTIFICACIONES PROFESORES
//-------------------------------------------
public function Notificaciones_maestros()
{
$data['lista'] = $this->Admin_model_cep->Horario_maestros();
$this->load->view('Admin_Cep/Notificaciones_maestros',$data);
}
//-------------------------------------------
public function View_notify_maestro() 
{
$this->load->view('Admin_Cep/View_notify_maestro');
}
//-------------------------------------------
public function Read_notify_maestro()
{
$user_id = $this->input->post('Id');
$rows = $this->Admin_model_cep->Read_notify_maestro($user_id);
echo json_encode($rows);
}
//-------------------------------------------
public function New_notify_maestro()
{
$new_usuario = $this->input->post('new_usuario');
$new_prioridad = $this->input->post('new_prioridad');
$new_texto = $this->input->post('new_texto');
$this->Admin_model_cep->New_notify_maestro($new_usuario,$new_prioridad,$new_texto);
}
//-------------------------------------------
public function Edit_notify_maestro() 
{
$id = $this->input->post('id');
$prioridad = $this->input->post('prioridad');
$texto = $this->input->post('texto');
$this->Admin_model_cep->Edit_notify_maestro($id,$prioridad,$texto);
}
//-------------------------------------------
public function Delete_notify_maestro() 
{
$Id_Noty = $this->input->post('Id_Noty');
$this->Admin_model_cep->Delete_notify_maestro($Id_Noty);
}
//-------------------------------------------
//NOTIFICACIONES PROFESORES

//NOTIFICACIONES ALUMNOS
//-------------------------------------------
public function Edit_notify_alum() 
{
$id = $this->input->post('id');
$prioridad = $this->input->post('prioridad');
$texto = $this->input->post('texto');
$this->Admin_model_cep->Edit_notify_alum($id,$prioridad,$texto);
}
//-------------------------------------------
public function Delete_notify_alum() 
{
$Id_Noty = $this->input->post('Id_Noty');
$this->Admin_model_cep->Delete_notify_alum($Id_Noty);
}
//-------------------------------------------
public function View_notify_alum() 
{
$this->load->view('Admin_Cep/View_notify_alum');
}

//-------------------------------------------
public function Read_notify_alumn()
{
$user_id = $this->input->post('Id');
$rows = $this->Admin_model_cep->Read_notify_alumn($user_id);
echo json_encode($rows);
}
//-------------------------------------------

public function New_notify_alumnos()
{
$new_usuario = $this->input->post('new_usuario');
$new_prioridad = $this->input->post('new_prioridad');
$new_texto = $this->input->post('new_texto');
$this->Admin_model_cep->New_notify_alum($new_usuario,$new_prioridad,$new_texto);
}

//-------------------------------------------
public function Notificaciones_alumnos()
{
$data['lista'] = $this->Admin_model_cep->Horario_alumnos();
$this->load->view('Admin_Cep/Notificaciones_alumnos',$data);
}
//-------------------------------------------
//NOTIFICACIONES ALUMNOS

//-------------------------------------------
public function Time_alumno()
{
$user_id = $_POST['id'];
$data['horario'] = $this->Admin_model_cep->Horario_of_alumno($user_id);
$dat='';

foreach ($data['horario']->result() as $usuario){

$dat .= '<tr>
<th class="text-center" style="padding: 0.5rem; font-size: 15px; font-weight: bold;">'.$usuario->Nombre.'</th>
<td class="text-center" style="padding: 0.6rem; font-size: 18px; font-weight: bold;">'.$usuario->profesor.'</td>
<td class="text-center" style="padding: 0.6rem; font-size: 18px; font-weight: bold;">'.$usuario->Dia.'</td>
<td class="text-center" style="padding: 0.6rem; font-size: 18px; font-weight: bold;">'.$usuario->Hora_Inicio.'</td>
<td class="text-center" style="padding: 0.6rem; font-size: 18px; font-weight: bold;">'.$usuario->Hora_Fin.'</td>
</tr>
';

}

echo($dat);
}
//-------------------------------------------
public function Time_profesor()
{
$user_id = $_POST['id'];
$data['horario'] = $this->Admin_model_cep->Horario_of_profesor($user_id);
$dat='';

foreach ($data['horario']->result() as $usuario){

$dat .= '<tr>
<th class="text-center" style="padding: 0.5rem; font-size: 15px; font-weight: bold;">'.$usuario->Nombre.'</th>
<td class="text-center" style="padding: 0.6rem; font-size: 18px; font-weight: bold;">'.$usuario->Cuatrimestre.'</td>
<td class="text-center" style="padding: 0.6rem; font-size: 18px; font-weight: bold;">'.$usuario->Dia.'</td>
<td class="text-center" style="padding: 0.6rem; font-size: 18px; font-weight: bold;">'.$usuario->Turno.'</td>
<td class="text-center" style="padding: 0.6rem; font-size: 18px; font-weight: bold;">'.$usuario->Hora_Inicio.'</td>
<td class="text-center" style="padding: 0.6rem; font-size: 18px; font-weight: bold;">'.$usuario->Hora_Fin.'</td>
</tr>
';

}

echo($dat);
}
//-------------------------------------------
public function Ciclo()
{
$Ciclo =  $this->uri->segment(3);
$data['calificaciones'] = $this->Admin_model_cep->Calificaciones_ciclo($Ciclo);
$this->load->view('Admin_Cep/Calificaciones_ciclo',$data);
}
//-------------------------------------------
public function Ciclos_escolares()
{
$data['ciclos'] = $this->Admin_model_cep->Ciclos_escolares();
$this->load->view('Admin_Cep/Ciclos_escolares',$data);
}
//-------------------------------------------
public function Historiales_View()
{
$Alumno =  $this->uri->segment(3);
$data['historial'] = $this->Admin_model_cep->Historiales_View($Alumno);
$this->load->view('Admin_Cep/Historiales_View',$data);
}
//-------------------------------------------
public function Historiales()
{
$data['Historiales'] = $this->Admin_model_cep->Historiales();
$this->load->view('Admin_Cep/Historiales',$data);
}

//-------------------------------------------
public function Busqueda()
{

if (isset($_POST['start_date']) && isset($_POST['end_date'])) 
{
$start_date = $_POST['start_date'];
$end_date = $_POST['end_date'];

$rows = $this->Admin_model_cep->Pagos_Alumnos_Fechas($start_date, $end_date);
} else {
$rows = $this->Admin_model_cep->Pagos_Alumnos_Todos();
}

echo json_encode($rows);

}
//-------------------------------------------
public function Pagos_Alumnos()
{
$data['pagos'] = $this->Admin_model_cep->Pagos_Alumnos();
$this->load->view('Admin_Cep/Pagos_Alumnos',$data);
}
//-------------------------------------------
public function Pagos_Maestros()
{
$this->load->view('Admin_Cep/Pagos_Maestros');
}
//-------------------------------------------

public function Calificaciones()
{
$data['calificaciones'] = $this->Admin_model_cep->Calificaciones();
$this->load->view('Admin_Cep/Calificaciones',$data);
}

//-------------------------------------------
public function Update_pagos()
{
$matricula = $_POST['matricula'];
$this->Admin_model_cep->Actualizar_Pagos($matricula);

}
//-------------------------------------------

public function GetCalificaciones()
{
$user_id = $_POST['id'];
$data['usuarios'] = $this->Admin_model_cep->getData($user_id);

$dat='';

foreach ($data['usuarios']->result() as $usuario){

$dat .= '<tr>
<th class="text-center" style="padding: 0.5rem; font-size: 15px; font-weight: bold;">'.$usuario->nombre.'</th>
<td class="text-center" style="padding: 0.6rem; font-size: 17px; font-weight: bold;">'.$usuario->profesor.'</td>
<td class="text-center" style="padding: 0.6rem; font-size: 17px; font-weight: bold;">'.$usuario->id_salon.'</td>
<td class="text-center" style="padding: 0.6rem; font-size: 17px; font-weight: bold;">'.$usuario->calificacion_1.'</td>
<td class="text-center" style="padding: 0.6rem; font-size: 17px; font-weight: bold;">'.$usuario->calificacion_2.'</td>
<td class="text-center" style="padding: 0.6rem; font-size: 17px; font-weight: bold;">'.$usuario->calificacion_3.'</td>
<td class="text-center" style="padding: 0.6rem; font-size: 17px; font-weight: bold;">'.$usuario->final.'</td>
</tr>
';

}

echo($dat);
}

//-------------------------------------------


//-------------------------------------------

public function Calificaciones_ciclos()
{
$user_id = $_POST['id'];
$periodo = $_POST['periodo'];
$data['usuarios'] = $this->Admin_model_cep->getData_ciclo($user_id,$periodo);

$dat='';

foreach ($data['usuarios']->result() as $usuario){

$dat .= '<tr>
<th class="text-center" style="padding: 0.5rem; font-size: 15px; font-weight: bold;">'.$usuario->nombre.'</th>
<td class="text-center" style="padding: 0.6rem; font-size: 17px; font-weight: bold;">'.$usuario->profesor.'</td>
<td class="text-center" style="padding: 0.6rem; font-size: 17px; font-weight: bold;">'.$usuario->id_salon.'</td>
<td class="text-center" style="padding: 0.6rem; font-size: 17px; font-weight: bold;">'.$usuario->calificacion_1.'</td>
<td class="text-center" style="padding: 0.6rem; font-size: 17px; font-weight: bold;">'.$usuario->calificacion_2.'</td>
<td class="text-center" style="padding: 0.6rem; font-size: 17px; font-weight: bold;">'.$usuario->calificacion_3.'</td>
<td class="text-center" style="padding: 0.6rem; font-size: 17px; font-weight: bold;">'.$usuario->final.'</td>
</tr>
';

}

echo($dat);



}

//-------------------------------------------

public function GetPagos()
{
$user_id = $_POST['id'];
$data['datos'] = $this->Admin_model_cep->Pago_Alumno($user_id);
$data['promedio'] = $this->Admin_model_cep->Pago_Alumno_Res($user_id);
echo json_encode($data);
}
//-------------------------------------------

public function Boleta()
{
$Alumno =  $this->uri->segment(3);
$data['row'] = $this->Admin_model_cep->Datos($Alumno);
$data['usuarios'] = $this->Admin_model_cep->getData($Alumno);
$data['row2'] = $this->Admin_model_cep->Calif_Finales($Alumno);
$this->load->view('Admin_Cep/Boleta',$data);
}
//-------------------------------------------
public function Boleta_ciclo()
{
$Alumno =  $this->uri->segment(3);
$Periodo =  $this->uri->segment(4);

$data['row'] = $this->Admin_model_cep->Datos($Alumno);
$data['usuarios'] = $this->Admin_model_cep->getData_2($Alumno,$Periodo);
$data['row2'] = $this->Admin_model_cep->Calif_Finales_2($Alumno,$Periodo);
$this->load->view('Admin_Cep/Boleta_ciclo',$data);
}
//-------------------------------------------
public function View_Horario_All()
{
$Matricula = $this->uri->segment(3);
$data['row'] = $this->Admin_model_cep->Horario($Matricula);
$data['name'] = $this->Admin_model_cep->Name_maestro($Matricula);
$this->load->view('Admin_Cep/View_Horario_All',$data);
}
//-------------------------------------------
public function Horario_alumnos()
{
$data['lista'] = $this->Admin_model_cep->Horario_alumnos();
$this->load->view('Admin_Cep/Horario_alumnos',$data);
}
//-------------------------------------------
public function Horario_maestros()
{
$data['lista'] = $this->Admin_model_cep->Horario_maestros();
$this->load->view('Admin_Cep/Horario_maestros',$data);
}
//-------------------------------------------
public function Busqueda_Pay_Alumnos()
{
$this->load->view('Admin_Cep/Busqueda_Pay_Alumnos');
}
//-------------------------------------------

//BAJAS ADMINISTRATIVAS 

//--------------------------------------------------------
public function Bajas_Adm()
{
	$data['datos_alums'] = $this->Admin_model_cep->Bajas();
	$this->load->view('Admin_Cep/Bajas_Adm',$data);
}

//--------------------------------------------------------



}

