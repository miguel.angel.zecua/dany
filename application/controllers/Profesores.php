<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profesores extends CI_Controller 
{

public function __construct() 
{
parent:: __construct();
$this->load->model('Profesores_model');
}

//--------------------------------------

public function index()
{

if($this->session->userdata('Login')==true)
{
$Matricula=$this->session->userdata('Matricula');
$data['side_bar'] = $this->Profesores_model->Datos_sidebar($Matricula);
$this->load->view('Profesores/Inicio',$data);
}

else
{
redirect("Login_secundario");
}

}
//--------------------------------------
public function Diplomados()
{
$Matricula=$this->session->userdata('Matricula');
$data['side_bar'] = $this->Profesores_model->Datos_sidebar($Matricula);

if($Matricula=='ADEROM100')
{
$data['usuarios'] = $this->Profesores_model->Lista_Dip_Crimi();
$this->load->view('Profesores/Diplomados/Criminalistica',$data);
}
elseif ($Matricula=='PATNET126') 
{
$data['usuarios'] = $this->Profesores_model->Lista_Dip_Derecho();
$this->load->view('Profesores/Diplomados/Derecho',$data);
}

else
{
$this->load->view('Profesores/Diplomados/Otro',$data);
}
 
}
//--------------------------------------


public function Diplomado_Criminalistica()
{

if($this->session->userdata('Login')==true)
{
$Matricula=$this->session->userdata('Matricula');
$data['side_bar'] = $this->Profesores_model->Datos_sidebar($Matricula);
$data['usuarios'] = $this->Profesores_model->Lista_Dip_Derecho();


$data['usuarios'] = $this->Profesores_model->Lista_Diplomado_Criminalistica($Matricula);
$this->load->view('Profesores/Diplomados/Diplomado_Criminalistica-V1',$data);
}


else
{
redirect("Login_secundario");
}

}

//--------------------------------------
public function Login_Profesores()
{
$this->session->sess_destroy();
$this->load->view('Profesores/Login_profesores');
}
//--------------------------------------

public function Datos_personales()
{

if($this->session->userdata('Login')==true)
{
$Matricula=$this->session->userdata('Matricula');
$data['side_bar'] = $this->Profesores_model->Datos_sidebar($Matricula);
$data['row'] = $this->Profesores_model->Datos($Matricula);
$this->load->view('Profesores/Datos_personales',$data);
}

else
{
redirect("Login_secundario");
}

}

//--------------------------------------

public function Reset_password()
{
if($this->session->userdata('Login')==true)
{
$Matricula=$this->session->userdata('Matricula');
$this->Profesores_model->Reset_password($Matricula);
redirect("Alumnos");
}
else
{
redirect("Login");
}

}

//--------------------------------------
public function Change_password()
{
if($this->session->userdata('Login')==true)
{
$Matricula=$this->session->userdata('Matricula');
$data['side_bar'] = $this->Profesores_model->Datos_sidebar($Matricula);
$this->load->view('Profesores/Change_password',$data);
}
else
{
redirect("Login");
}

}
//--------------------------------------

public function Calificaciones()
{
if($this->session->userdata('Login')==true)
{
$Matricula=$this->session->userdata('Matricula');
$data['usuario'] = $this->Profesores_model->Grupos($Matricula);
$data['side_bar'] = $this->Profesores_model->Datos_sidebar($Matricula);
$this->load->view('Profesores/Calificaciones',$data);

}
else
{
redirect("Login_secundario");
}

}

//--------------------------------------

public function Tabla_Alumnos()
{


if($this->session->userdata('Login')==true){
$Matricula=$this->session->userdata('Matricula');
$Salon =  $this->uri->segment(3);
$Materia =  $this->uri->segment(4);
$data['usuarios'] = $this->Profesores_model->Lista($Salon,$Matricula,$Materia);
$data['side_bar'] = $this->Profesores_model->Datos_sidebar($Matricula);
$this->load->view('Profesores/Tabla_Alumnos',$data);
}

else
{
redirect("Login_secundario");
}

}

//--------------------------------------


public function Insert_Calificaciones()
{

$datos_recibidos = $_POST;

foreach($datos_recibidos as $key => $value){

$calif_1= $value['Calificacion_1'];
$calif_2= $value['Calificacion_2'];
$calif_3= $value['Calificacion_3'];
$final = $value['final'];
$alumno = $value['matricula'];
$materia= $value['Id_materia'];
$profesor= $value['Matricula_profesor'];
$this->Profesores_model->Insert_Calif($calif_1,$calif_2,$calif_3,$final,$alumno,$materia,$profesor);

}
echo 1;

}

//--------------------------------------
public function Insert_Calificaciones_Dip_Crimi()
{
$datos_recibidos = $_POST;

foreach($datos_recibidos as $key => $value){

$alumno =$value['Matricula'];
$calif_1= $value['Calificacion_1'];
$calif_2= $value['Calificacion_2'];
$calif_3= $value['Calificacion_3'];
$calif_4= $value['Calificacion_4'];
$calif_5= $value['Calificacion_5'];
$calif_6= $value['Calificacion_6'];
$calif_7= $value['Calificacion_7'];
$calif_8= $value['Calificacion_8'];
$calif_9= $value['Calificacion_9'];
$calif_10= $value['Calificacion_10'];
$calif_11= $value['Calificacion_11'];
$calif_12= $value['Calificacion_12'];
$final = $value['Final'];

$this->Profesores_model->Insert_Calif_Dip_Crimi($calif_1,$calif_2,$calif_3,$calif_4,$calif_5,$calif_6,$calif_7,$calif_8,$calif_9,$calif_10,$calif_11,$calif_12,$final,$alumno);

}
echo 1;
}

//--------------------------------------
public function Insert_Calificaciones_Diplomado_Criminalistica()
{
$datos_recibidos = $_POST;


foreach($datos_recibidos as $key => $value){

$Materia_Id =$value['Materia_Id'];
$Profesor= $value['Profesor'];
$Alumno_Id= $value['Alumno_Id'];
$Calificacion= $value['Calif'];

$this->Profesores_model->Insert_Calif_Diplomado_Criminalistica($Materia_Id,$Profesor,$Alumno_Id,$Calificacion);

}
//echo 1;
}


//--------------------------------------



//--------------------------------------
public function Insert_Calificaciones_Dip_Derecho()
{
$datos_recibidos = $_POST;

foreach($datos_recibidos as $key => $value){

$alumno =$value['Matricula'];
$calif_1= $value['Calificacion_1'];
$calif_2= $value['Calificacion_2'];
$calif_3= $value['Calificacion_3'];
$final = $value['Final'];

$this->Profesores_model->Insert_Calificaciones_Dip_Derecho($calif_1,$calif_2,$calif_3,$final,$alumno);

}
echo 1;
}
//--------------------------------------

public function Horario()
{
if($this->session->userdata('Login')==true)
{
$Matricula=$this->session->userdata('Matricula');
$data['row'] = $this->Profesores_model->Horario($Matricula);
$data['side_bar'] = $this->Profesores_model->Datos_sidebar($Matricula);
$this->load->view('Profesores/Horarios',$data);
}

else
{
redirect("Login_secundario");
}

}

//--------------------------------------

public function Temario()
{
if($this->session->userdata('Login')==true)
{
$Matricula=$this->session->userdata('Matricula');
$data['row'] = $this->Profesores_model->Planeacion($Matricula);
$data['side_bar'] = $this->Profesores_model->Datos_sidebar($Matricula);
$this->load->view('Profesores/Planeaciones',$data);
}
else
{
redirect("Login_secundario");
}
}

//--------------------------------------

public function Smt_Temario()
{

$nombre = $this->input->post('nombre');
$Matricula_profesor = $this->session->userdata('Matricula');

$file_name = $_FILES['archivo']['name'];
$file_size = $_FILES['archivo']['size'];
$file_tmp = $_FILES['archivo']['tmp_name'];
$file_type = $_FILES['archivo']['type'];


$fp = fopen($file_tmp,'r+b');
$binario = fread($fp, filesize($file_tmp));
fclose($fp);


$datos = array(
'Archivo' => $binario,
'Nombre' => $nombre,
'Matricula_profesor' => $Matricula_profesor


);

$this->Profesores_model->Insert_Tem($datos);

}

//--------------------------------------

public function verArchivo($Id)
{
$consulta = $this->Profesores_model->getArchivo($Id);
$archivo = $consulta['Archivo'];
$nombre = $consulta['Nombre']; 
header("Content-type: application/pdf");
header("Content-Disposition: inline; filename=$nombre.pdf");
print_r($archivo);
}

//--------------------------------------

public function deleteArchivo($Id)
{
$this->Profesores_model->deleteArch($Id);
redirect("Profesores/Temario");
}

//--------------------------------------

public function Pendiente()
{
$this->load->view('Profesores/Pendiente');
}


//--------------------------------------

public function update($Matricula) 
{
$this->Profesores_model->Actualizar($Matricula);
redirect("Profesores/Datos_personales");
}

//--------------------------------------


//INICIO DE LAS CLASES PARA EL LOGIN DE LOS DOCENTES


public function Enter()
{
$matricula = $this->input->post('Matricula');
$password = $this->input->post('Password');

$fila = $this->Profesores_model->Main($matricula);

if($fila !=null)
{
if($fila->Password==$password){
$data = array(
'Matricula' => $fila->Matricula,
'Nombre'=> $fila->Nombres,
'Login' => true
);
$this->session->set_userdata($data);

redirect("Profesores");
}
else
{
redirect("Profesores/Ses");
}

}

else
{
redirect("Profesores");
}

}

//--------------------------------------

public function End()
{
$this->session->sess_destroy();
redirect("Profesores/Login_profesores");
}

//--------------------------------------

public function Ses()
{
$this->session->sess_destroy();
$this->load->view('Profesores/Login_profesores');
}

}

