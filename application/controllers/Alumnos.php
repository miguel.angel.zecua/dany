<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alumnos extends CI_Controller 
{

public function __construct() 
{
parent:: __construct();
$this->load->library('Pdf');
$this->load->model('Alumnos_model');
}

//--------------------------------------

public function index()
{
if($this->session->userdata('Login')==true)
{
$Matricula=$this->session->userdata('Matricula');
$data['side_bar'] = $this->Alumnos_model->Datos_sidebar($Matricula);
$this->load->view('Alumnos/Inicio',$data);	
}
else
{
redirect("Alumnos/Login_Alumnos");
}

}

//--------------------------------------

public function Datos_personales()
{
if($this->session->userdata('Login')==true)
{
$Matricula=$this->session->userdata('Matricula');
$data['row'] = $this->Alumnos_model->Datos($Matricula);
$data['side_bar'] = $this->Alumnos_model->Datos_sidebar($Matricula);
$this->load->view('Alumnos/Datos_personales',$data);
}
else
{
redirect("Alumnos/Login_Alumnos");
}

}

//--------------------------------------
public function Evaluacion_maestros()
{
if($this->session->userdata('Login')==true)
{
$Matricula=$this->session->userdata('Matricula');
$data['side_bar'] = $this->Alumnos_model->Datos_sidebar($Matricula);
$data['usuarios'] = $this->Alumnos_model->Nombres_maestros($Matricula);
$this->load->view('Alumnos/Evaluacion_maestros',$data);
}
else
{
redirect("Alumnos/Login_Alumnos");
}

}
//--------------------------------------
public function Env_evaluacion()
{
$this->Alumnos_model->Env_evalu();
}
//--------------------------------------
public function Evaluacion_View()
{
if($this->session->userdata('Login')==true)
{
$Matricula=$this->session->userdata('Matricula');
$data['Materia']=$this->uri->segment(3);
$data['Maestro']=$this->uri->segment(4);
$profe = $data['Maestro'];
 //echo "<script>console.log($profe);</script>";

$data['Alumno']=$this->session->userdata('Matricula');  
$data['side_bar'] = $this->Alumnos_model->Datos_sidebar($Matricula);
$this->load->view('Alumnos/Evaluacion_View',$data);
}
else
{
redirect("Alumnos/Login_Alumnos");
}

}
//--------------------------------------
public function Diplomados()
{
if($this->session->userdata('Login')==true)
{
$Matricula=$this->session->userdata('Matricula');
$Deudas = array("ITZSAR010");
if (in_array($Matricula,$Deudas,true)==true){
$data['match']= 1;
}else {
$data['match']= 0;

}


$data['side_bar'] = $this->Alumnos_model->Datos_sidebar($Matricula);
$Mach_1 = $this->Alumnos_model->Mach_Crimi($Matricula);
$Mach_2 = $this->Alumnos_model->Mach_Derecho($Matricula);

if($Mach_1->Resultado>=1)
{
$data['usuarios'] = $this->Alumnos_model->Calificaciones_Crimi($Matricula);
$this->load->view('Alumnos/Diplomados/Criminalistica',$data);
}
elseif ($Mach_2->Resultado>=1) 
{
$data['usuarios'] = $this->Alumnos_model->Calificaciones_Derecho($Matricula);
$this->load->view('Alumnos/Diplomados/Derecho',$data);
}
else
{

}


}

}
//--------------------------------------
public function Diplomado_Criminalistica()
{

if($this->session->userdata('Login')==true)
{
$Matricula=$this->session->userdata('Matricula');
$data['row'] = $this->Alumnos_model->Datos($Matricula);
$data['side_bar'] = $this->Alumnos_model->Datos_sidebar($Matricula);
$Mach_1 = $this->Alumnos_model->Mach_Criminalistica($Matricula);

if($Mach_1->Resultado >=1)
{
$data['usuarios'] = $this->Alumnos_model->Calificaciones_Criminalistica($Matricula);
$this->load->view('Alumnos/Diplomados/Criminalistica-V1',$data);
}

if($Mach_1->Resultado <=0)
{
$this->load->view('Alumnos/Diplomados/None',$data);
}


}
else
{
redirect("Alumnos/Login_Alumnos");
}

}

//--------------------------------------

public function Reset_password()
{
if($this->session->userdata('Login')==true)
{
$Matricula=$this->session->userdata('Matricula');
$this->Alumnos_model->Reset_password($Matricula);
redirect("Alumnos");
}
else
{
redirect("Alumnos/Login_Alumnos");
}

}

//--------------------------------------
public function Change_password()
{
if($this->session->userdata('Login')==true)
{
$Matricula=$this->session->userdata('Matricula');
$data['side_bar'] = $this->Alumnos_model->Datos_sidebar($Matricula);
$this->load->view('Alumnos/Change_password',$data);
}
else
{
redirect("Alumnos/Login_Alumnos");
}

}
//--------------------------------------
public function GetPagos()
{
$user_id = $_POST['id'];

$data['datos'] = $this->Alumnos_model->Pagos_Alumno($user_id);

echo json_encode($data);

}
//--------------------------------------

public function Calificaciones()
{
if($this->session->userdata('Login')==true)
{
$Matricula=$this->session->userdata('Matricula');
$data['row'] = $this->Alumnos_model->Datos($Matricula);
$data['usuarios'] = $this->Alumnos_model->Calificaciones($Matricula);
$data['row2'] = $this->Alumnos_model->Calif_Finales($Matricula);
$data['side_bar'] = $this->Alumnos_model->Datos_sidebar($Matricula);
$this->load->view('Alumnos/Calificaciones',$data);
}
else
{
redirect("Login");
}

}

//--------------------------------------

public function Historial()
{
if($this->session->userdata('Login')==true)
{
$Matricula=$this->session->userdata('Matricula');
$data['row'] = $this->Alumnos_model->Datos($Matricula);
$data['historial'] = $this->Alumnos_model->Historial($Matricula);
$data['promedio'] = $this->Alumnos_model->Promedio($Matricula);
$data['credito'] = $this->Alumnos_model->Credito($Matricula);
$data['side_bar'] = $this->Alumnos_model->Datos_sidebar($Matricula);
$this->load->view('Alumnos/Historial',$data);
}
else
{
redirect("Alumnos/Login_Alumnos");
}

}

//--------------------------------------

public function Horario()
{
if($this->session->userdata('Login')==true)
{
$Matricula=$this->session->userdata('Matricula');
$data['row'] = $this->Alumnos_model->Horario($Matricula);
$data['rows'] = $this->Alumnos_model->Datos($Matricula);
$data['side_bar'] = $this->Alumnos_model->Datos_sidebar($Matricula);
$this->load->view('Alumnos/Horario',$data);
}
else
{
redirect("Alumnos/Login_Alumnos");
}

}

//--------------------------------------

public function Justificante()
{
if($this->session->userdata('Login')==true)
{
$Matricula=$this->session->userdata('Matricula');
$data['rows'] = $this->Alumnos_model->Datos($Matricula);
$this->load->view('Alumnos/Justificante',$data);
}
else
{
redirect("Alumnos/Login_Alumnos");
}

}

//--------------------------------------

public function Pagos()
{
if($this->session->userdata('Login')==true)
{
$Matricula=$this->session->userdata('Matricula');
$data['pagos'] = $this->Alumnos_model->Pagos($Matricula);
$data['side_bar'] = $this->Alumnos_model->Datos_sidebar($Matricula);
$this->load->view('Alumnos/Pagos',$data);
}
else
{
redirect("Alumnos/Login_Alumnos");
}

}

//--------------------------------------

public function Calendario()
{
if($this->session->userdata('Login')==true)
{
$Matricula=$this->session->userdata('Matricula');
$data['rows'] = $this->Alumnos_model->Datos($Matricula);
$data['side_bar'] = $this->Alumnos_model->Datos_sidebar($Matricula);
$this->load->view('Alumnos/Calendario',$data);
}
else
{
redirect("Alumnos/Login_Alumnos");
}

}

//--------------------------------------
public function Ayuda()
{
if($this->session->userdata('Login')==true)
{
$Matricula=$this->session->userdata('Matricula');
$data['side_bar'] = $this->Alumnos_model->Datos_sidebar($Matricula);
$this->load->view('Alumnos/Ayuda',$data);
}
else
{
redirect("Alumnos/Login_Alumnos");
}

}
//--------------------------------------

public function Asesorias()
{
if($this->session->userdata('Login')==true)
{
$this->load->view('Alumnos/Asesorias');
}
else
{
redirect("Alumnos/Login_Alumnos");
}

}

//--------------------------------------

public function update($Matricula) 
{
$this->Alumnos_model->Actualizar($Matricula);
redirect("Alumnos/Datos_personales");
}

//--------------------------------------

public function imprimir()
{


$Matricula=$this->session->userdata('Matricula');

$data['row'] = $this->Alumnos_model->Datos($Matricula);
$data['usuarios'] = $this->Alumnos_model->Calificaciones($Matricula);


$html = $this->load->view('Alumnos/Imp_boleta', $data, true);
 $this->pdf->createPDF($html, 'mypdf', false);



}

//--------------------------------------
public function Enter()
{
$matricula = $this->input->post('Matricula');
$password = $this->input->post('Password');
$jsondata = array();

$fila = $this->Alumnos_model->Main($matricula);

if($fila !=null)
{
if($fila->Password==$password){
$data = array(
'Matricula' => $fila->Matricula,
'Nombre'=> $fila->Nombres,
'Login' => true
);
$this->session->set_userdata($data);

$jsondata['Login'] = 1;
$jsondata['Permisos'] = $fila->Permisos;
echo json_encode($jsondata);

}
else
{
$jsondata['Login'] = 0;
$jsondata['Permisos'] = 0;
echo json_encode($jsondata);
}

}

else
{
$jsondata['Login'] = 2;
$jsondata['Permisos'] = 0 ;
echo json_encode($jsondata);
}

}

//--------------------------------------
public function End()
{
$this->session->sess_destroy();
redirect("Alumnos/Login_Alumnos");
}

//--------------------------------------
public function Login_Alumnos()
{
$this->session->sess_destroy();
$this->load->view('Alumnos/Login');
}
//--------------------------------------

public function Escuela()
{
if($this->session->userdata('Login')==true)
{
$Matricula=$this->session->userdata('Matricula');
$data['escuela'] = $this->Alumnos_model->Escuela($Matricula);

$this->load->view('Alumnos/Pruebas',$data);
}
else
{
redirect("Alumnos/Login_Alumnos");
}

}


//FIN DE CLASE 
}









