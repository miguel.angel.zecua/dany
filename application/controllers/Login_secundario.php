<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_secundario extends CI_Controller {

public function __construct() 
{
parent:: __construct();
$this->load->model('Login_profesores');
}

public function index()
{
$this->session->sess_destroy();
$this->load->view('Profesores/Login_profesores');
}


public function Enter()
{
$matricula = $this->input->post('Matricula');
$password = $this->input->post('Password');

$fila = $this->Login_profesores->Main($matricula);

if($fila !=null)
{
if($fila->Password==$password){
$data = array(
'Matricula' => $fila->Matricula,
'Nombre'=> $fila->Nombres,
'Login' => true
);
$this->session->set_userdata($data);

redirect("Profesores");
}
else
{
redirect("Login_secundario");
}

}

else
{
redirect("Profesores");
}

}



public function End()
{
$this->session->sess_destroy();
redirect("Login_secundario");
}



/*public function Recovery()
{
$this->load->view('Login/Recovery');
}


public function New()
{
$this->Model->New();
echo "<script> swal({
   title: '¡ERROR!',
   text: 'Esto es un mensaje de error',
   type: 'error',
 });</script>";
redirect("Login");

}*/





///FIN DE CLASE
}
