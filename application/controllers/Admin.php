<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller 
{

public function __construct() 
{
parent:: __construct();
$this->load->model('Admin_model');
}
//-------------------------------------------
public function Permisos()
{
$data['lista'] = $this->Admin_model->Horario_alumnos();
$this->load->view('Admin/Permisos',$data);
}

//-------------------------------------------
public function Permisos_View()
{
$usuario = $this->input->post('usuario');
$resultado = $this->Admin_model->Permisos_View($usuario);
echo json_encode($resultado);
}
//-------------------------------------------

public function index()
{
$this->load->view('Admin/Inicio');
}
//-------------------------------------------
public function Datos()
{
$materia =  $_POST['materia'];
$maestro =  $_POST['maestro'];
$rows = $this->Admin_model->Datos_estadistica($materia,$maestro);
echo json_encode($rows);

}
//-------------------------------------------
public function Eval_total() {
$Maestro = 'MELMOR122';	
$data['total'] = $this->Admin_model->Eval_total($maestro);
$this->load->view('Admin/Eval_docentes');
}
//-------------------------------------------
public function Estadisticas()
{
$this->load->view('Admin/Estadisticas');
}
//-------------------------------------------
public function Eva_Maestro()
{
$user_id =  $this->uri->segment(3);
$data['horario'] = $this->Admin_model->Horario_of_profesor($user_id);
$data['maestro'] = $this->uri->segment(3);
$this->load->view('Admin/Eva_Maestro',$data);
}
//-------------------------------------------
public function Evaluacion_maestros()
{
$data['lista'] = $this->Admin_model->Horario_maestros();
$this->load->view('Admin/Evaluacion_maestros',$data);
}
//-------------------------------------------

//NOTIFICACIONES PROFESORES
//-------------------------------------------
public function Notificaciones_maestros()
{
$data['lista'] = $this->Admin_model->Horario_maestros();
$this->load->view('Admin/Notificaciones_maestros',$data);
}
//-------------------------------------------
public function View_notify_maestro() 
{
$this->load->view('Admin/View_notify_maestro');
}
//-------------------------------------------
public function Read_notify_maestro()
{
$user_id = $this->input->post('Id');
$rows = $this->Admin_model->Read_notify_maestro($user_id);
echo json_encode($rows);
}
//-------------------------------------------
public function New_notify_maestro()
{
$new_usuario = $this->input->post('new_usuario');
$new_prioridad = $this->input->post('new_prioridad');
$new_texto = $this->input->post('new_texto');
$this->Admin_model->New_notify_maestro($new_usuario,$new_prioridad,$new_texto);
}
//-------------------------------------------
public function Edit_notify_maestro() 
{
$id = $this->input->post('id');
$prioridad = $this->input->post('prioridad');
$texto = $this->input->post('texto');
$this->Admin_model->Edit_notify_maestro($id,$prioridad,$texto);
}
//-------------------------------------------
public function Delete_notify_maestro() 
{
$Id_Noty = $this->input->post('Id_Noty');
$this->Admin_model->Delete_notify_maestro($Id_Noty);
}
//-------------------------------------------
//NOTIFICACIONES PROFESORES

//NOTIFICACIONES ALUMNOS
//-------------------------------------------
public function Edit_notify_alum() 
{
$id = $this->input->post('id');
$prioridad = $this->input->post('prioridad');
$texto = $this->input->post('texto');
$this->Admin_model->Edit_notify_alum($id,$prioridad,$texto);
}
//-------------------------------------------
public function Delete_notify_alum() 
{
$Id_Noty = $this->input->post('Id_Noty');
$this->Admin_model->Delete_notify_alum($Id_Noty);
}
//-------------------------------------------
public function View_notify_alum() 
{
$this->load->view('Admin/View_notify_alum');
}

//-------------------------------------------
public function Read_notify_alumn()
{
$user_id = $this->input->post('Id');
$rows = $this->Admin_model->Read_notify_alumn($user_id);
echo json_encode($rows);
}
//-------------------------------------------

public function New_notify_alumnos()
{
$new_usuario = $this->input->post('new_usuario');
$new_prioridad = $this->input->post('new_prioridad');
$new_texto = $this->input->post('new_texto');
$this->Admin_model->New_notify_alum($new_usuario,$new_prioridad,$new_texto);
}

//-------------------------------------------
public function Notificaciones_alumnos()
{
$data['lista'] = $this->Admin_model->Horario_alumnos();
$this->load->view('Admin/Notificaciones_alumnos',$data);
}
//-------------------------------------------
//NOTIFICACIONES ALUMNOS

//-------------------------------------------
public function Time_alumno()
{
$user_id = $_POST['id'];
$data['horario'] = $this->Admin_model->Horario_of_alumno($user_id);
$dat='';

foreach ($data['horario']->result() as $usuario){

$dat .= '<tr>
<th class="text-center" style="padding: 0.5rem; font-size: 15px; font-weight: bold;">'.$usuario->Nombre.'</th>
<td class="text-center" style="padding: 0.6rem; font-size: 18px; font-weight: bold;">'.$usuario->profesor.'</td>
<td class="text-center" style="padding: 0.6rem; font-size: 18px; font-weight: bold;">'.$usuario->Dia.'</td>
<td class="text-center" style="padding: 0.6rem; font-size: 18px; font-weight: bold;">'.$usuario->Hora_Inicio.'</td>
<td class="text-center" style="padding: 0.6rem; font-size: 18px; font-weight: bold;">'.$usuario->Hora_Fin.'</td>
</tr>
';

}

echo($dat);
}
//-------------------------------------------
public function Time_profesor()
{
$user_id = $_POST['id'];
$data['horario'] = $this->Admin_model->Horario_of_profesor($user_id);
$dat='';
foreach ($data['horario']->result() as $usuario){

$dat .= '<tr>
<th class=" d-none text-center" style="padding: 0.5rem; font-size: 15px; font-weight: bold;"><strong id="nombre_prof">'.$usuario->profe.'</strong></th>
<th class="text-center" style="padding: 0.5rem; font-size: 15px; font-weight: bold;">'.$usuario->Nombre.'</th>
<td class="text-center" style="padding: 0.6rem; font-size: 18px; font-weight: bold;">'.$usuario->Cuatrimestre.'</td>
<td class="text-center" style="padding: 0.6rem; font-size: 18px; font-weight: bold;">'.$usuario->Dia.'</td>
<td class="text-center" style="padding: 0.6rem; font-size: 18px; font-weight: bold;">'.$usuario->Turno.'</td>
<td class="text-center" style="padding: 0.6rem; font-size: 18px; font-weight: bold;">'.$usuario->Hora_Inicio.'</td>
<td class="text-center" style="padding: 0.6rem; font-size: 18px; font-weight: bold;">'.$usuario->Hora_Fin.'</td>
</tr>
';
}

echo($dat);
}

public function N_p()
{
$user_id = $_POST['id'];
$data['n_profe'] = $this->Admin_model->Name_maestro($user_id);
$dat='';

foreach ($data['n_profe']->result() as $profe){
$dat .= '<tr>
<th class="text-center text-white">Horario Profesor: '.$profe->nombre.' </th>
<th class="text-center text-white">Horario Profesor: '.$profe->nombre.' </th>
<th class="text-center text-white">Horario Profesor: '.$profe->nombre.' </th>
<th class="text-center text-white">Horario Profesor: '.$profe->nombre.' </th>
<th class="text-center text-white">Horario Profesor: '.$profe->nombre.' </th>
<th class="text-center text-white">Horario Profesor: '.$profe->nombre.' </th>
<th class="text-center text-white">Horario Profesor: '.$profe->nombre.' </th>
</tr> ';
}

echo($dat);
}
//-------------------------------------------
public function Ciclo()
{
$Ciclo =  $this->uri->segment(3);
$data['calificaciones'] = $this->Admin_model->Calificaciones_ciclo($Ciclo);
$this->load->view('Admin/Calificaciones_ciclo',$data);
}
//-------------------------------------------
public function Ciclos_escolares()
{
$data['ciclos'] = $this->Admin_model->Ciclos_escolares();
$this->load->view('Admin/Ciclos_escolares',$data);
}
//-------------------------------------------
public function Historiales_View()
{
$Alumno =  $this->uri->segment(3);
$data['historial'] = $this->Admin_model->Historiales_View($Alumno);
$this->load->view('Admin/Historiales_View',$data);
}
//-------------------------------------------
public function Historiales()
{
$data['Historiales'] = $this->Admin_model->Historiales();
$this->load->view('Admin/Historiales',$data);
}

//-------------------------------------------
public function Busqueda()
{

if (isset($_POST['start_date']) && isset($_POST['end_date'])) 
{
$start_date = $_POST['start_date'];
$end_date = $_POST['end_date'];

$rows = $this->Admin_model->Pagos_Alumnos_Fechas($start_date, $end_date);
} else {
$rows = $this->Admin_model->Pagos_Alumnos_Todos();
}

echo json_encode($rows);

}
//-------------------------------------------
public function Pagos_Alumnos()
{
$data['pagos'] = $this->Admin_model->Pagos_Alumnos();
$this->load->view('Admin/Pagos_Alumnos',$data);
}
//-------------------------------------------
public function Pagos_Maestros()
{
$this->load->view('Admin/Pagos_Maestros');
}
//-------------------------------------------

public function Calificaciones()
{
$data['calificaciones'] = $this->Admin_model->Calificaciones();
$this->load->view('Admin/Calificaciones',$data);
}

//-------------------------------------------
public function Update_pagos()
{
$matricula = $_POST['matricula'];
$this->Admin_model->Actualizar_Pagos($matricula);

}
//-------------------------------------------

public function GetCalificaciones()
{
$user_id = $_POST['id'];
$data['usuarios'] = $this->Admin_model->getData($user_id);

$dat='';

foreach ($data['usuarios']->result() as $usuario){

$dat .= '<tr>
<th class="text-center" style="padding: 0.5rem; font-size: 15px; font-weight: bold;">'.$usuario->nombre.'</th>
<td class="text-center" style="padding: 0.6rem; font-size: 17px; font-weight: bold;">'.$usuario->profesor.'</td>
<td class="text-center" style="padding: 0.6rem; font-size: 17px; font-weight: bold;">'.$usuario->id_salon.'</td>
<td class="text-center" style="padding: 0.6rem; font-size: 17px; font-weight: bold;">'.$usuario->calificacion_1.'</td>
<td class="text-center" style="padding: 0.6rem; font-size: 17px; font-weight: bold;">'.$usuario->calificacion_2.'</td>
<td class="text-center" style="padding: 0.6rem; font-size: 17px; font-weight: bold;">'.$usuario->calificacion_3.'</td>
<td class="text-center" style="padding: 0.6rem; font-size: 17px; font-weight: bold;">'.$usuario->final.'</td>
</tr>
';

}

echo($dat);
}

//-------------------------------------------


//-------------------------------------------

public function Calificaciones_ciclos()
{
$user_id = $_POST['id'];
$periodo = $_POST['periodo'];
$data['usuarios'] = $this->Admin_model->getData_ciclo($user_id,$periodo);

$dat='';

foreach ($data['usuarios']->result() as $usuario){

$dat .= '<tr>
<th class="text-center" style="padding: 0.5rem; font-size: 15px; font-weight: bold;">'.$usuario->nombre.'</th>
<td class="text-center" style="padding: 0.6rem; font-size: 17px; font-weight: bold;">'.$usuario->profesor.'</td>
<td class="text-center" style="padding: 0.6rem; font-size: 17px; font-weight: bold;">'.$usuario->id_salon.'</td>
<td class="text-center" style="padding: 0.6rem; font-size: 17px; font-weight: bold;">'.$usuario->calificacion_1.'</td>
<td class="text-center" style="padding: 0.6rem; font-size: 17px; font-weight: bold;">'.$usuario->calificacion_2.'</td>
<td class="text-center" style="padding: 0.6rem; font-size: 17px; font-weight: bold;">'.$usuario->calificacion_3.'</td>
<td class="text-center" style="padding: 0.6rem; font-size: 17px; font-weight: bold;">'.$usuario->final.'</td>
</tr>
';

}

echo($dat);



}

//-------------------------------------------

public function GetPagos()
{
$user_id = $_POST['id'];
$data['datos'] = $this->Admin_model->Pago_Alumno($user_id);
$data['promedio'] = $this->Admin_model->Pago_Alumno_Res($user_id);
echo json_encode($data);
}
//-------------------------------------------

public function Boleta()
{
$Alumno =  $this->uri->segment(3);
$data['row'] = $this->Admin_model->Datos($Alumno);
$data['usuarios'] = $this->Admin_model->getData($Alumno);
$data['row2'] = $this->Admin_model->Calif_Finales($Alumno);
$this->load->view('Admin/Boleta',$data);
}
//-------------------------------------------
public function Boleta_ciclo()
{
$Alumno =  $this->uri->segment(3);
$Periodo =  $this->uri->segment(4);

$data['row'] = $this->Admin_model->Datos($Alumno);
$data['usuarios'] = $this->Admin_model->getData_2($Alumno,$Periodo);
$data['row2'] = $this->Admin_model->Calif_Finales_2($Alumno,$Periodo);
$this->load->view('Admin/Boleta_ciclo',$data);
}
//-------------------------------------------
public function View_Horario_All()
{
$Matricula = $this->uri->segment(3);
$data['row'] = $this->Admin_model->Horario($Matricula);
$data['name'] = $this->Admin_model->Name_maestro($Matricula);
$this->load->view('Admin/View_Horario_All',$data);
}
//-------------------------------------------
public function Horario_alumnos()
{
$data['lista'] = $this->Admin_model->Horario_alumnos();
$this->load->view('Admin/Horario_alumnos',$data);
}
//-------------------------------------------
public function Horario_maestros()
{
$data['lista'] = $this->Admin_model->Horario_maestros();
$this->load->view('Admin/Horario_maestros',$data);
}
//-------------------------------------------
public function Busqueda_Pay_Alumnos()
{
$this->load->view('Admin/Busqueda_Pay_Alumnos');
}
//-------------------------------------------
public function Eval_docentes()
{
$data['lista'] = $this->Admin_model->Horario_maestros(); 
$this->load->view('Admin/Eval_docentes',$data);
}

public function Resultados_eval()
{
$this->load->view('Admin/Resultados_eval');
}

//-------------------------------------------

//Listas de alumnos por materias
public function Listados()
{
$data['lista'] = $this->Admin_model->Horario_maestros();
$this->load->view('Admin/Lista_maestros',$data);
}
//-------------------------------------------
public function Materias_maestros(){
$user_id =  $this->uri->segment(3);
$data['horario'] = $this->Admin_model->Horario_of_profesor($user_id);
$data['maestro'] = $this->uri->segment(3);
$this->load->view('Admin/Materias_maestro',$data);
}

//-------------------------------------------
public function Lista_alumnos(){
$Materia = $this->uri->segment(3);
$Matricula = $this->uri->segment(4);
$data['alumnos'] = $this->Admin_model->Listas_alumnos($Materia,$Matricula);
$data['mat_datos'] = $this->Admin_model->Mat_datos($Materia,$Matricula);
$this->load->view('Admin/Lista_alumnos',$data);

}


}
