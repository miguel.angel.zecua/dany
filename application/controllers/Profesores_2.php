<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profesores_2 extends CI_Controller {

	
	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function Horarios()
	{
		$this->load->view('Profesores/Horarios');
	}

	public function Change_password()
	{
		$this->load->view('Profesores/Change_password');
	}

	public function Datos_personales()
	{
		$this->load->view('Profesores/Datos_personales');
	}

	public function Temarios()
	{
		$this->load->view('Profesores/Temarios');
	}

	public function Planeaciones()
	{
		$this->load->view('Profesores/Planeaciones');
	}

	public function Calificaciones()
	{
		$this->load->view('Profesores/Calificaciones');
	}

}